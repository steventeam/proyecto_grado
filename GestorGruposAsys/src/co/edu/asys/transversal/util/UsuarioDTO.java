package co.edu.asys.transversal.util;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String primerNombre;
	private String segundoNombre;
	private String primerApellido;
	private String segundoApellido;
	private String correoElectronico;
	private String nombreUsuario;

	public UsuarioDTO(Integer codigo, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String correoElectronico, String nombreUsuario) {
		super();
		this.codigo = codigo;
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.correoElectronico = correoElectronico;
		this.nombreUsuario = nombreUsuario;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}
}