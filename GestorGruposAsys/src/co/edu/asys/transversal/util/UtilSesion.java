package co.edu.asys.transversal.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UtilSesion {

	private static UtilSesion INSTANCIA = new UtilSesion();

	private UtilSesion() {
	}

	public static UtilSesion obtenerUtilSesion() {
		return INSTANCIA;
	}

	public void crearSesion(SesionDTO sesion, HttpServletRequest peticion) {

		HttpSession httpSession = peticion.getSession(true);

		// Si la sesi�n no es nueva, se supone que debemos cargarla desde cero.
		// Por tal motivo, si no es nueva, la invalidamos y la volvemos a crear
		if (httpSession.isNew() == false) {
			cerrarSesion(peticion);
			httpSession = peticion.getSession(true);
		}

		// Colocamos los datos del usuario en la sesi�n
		httpSession.setAttribute("datosUsuario", sesion);
	}

	public void cerrarSesion(HttpServletRequest peticion) {
		peticion.getSession(true).invalidate();
	}

	public boolean sesionEsValida(HttpServletRequest peticion) {
		Object sesionUsuario = peticion.getSession(true).getAttribute("datosUsuario");

		boolean retorno = false;

		if (sesionUsuario != null) {
			retorno = true;
		}

		return retorno;
	}

	public boolean estaAutorizado(HttpServletRequest peticion, String ruta) {

		boolean retorno = false;

		if (sesionEsValida(peticion)) {
			SesionDTO sesion = (SesionDTO) peticion.getSession(true).getAttribute("datosUsuario");

			if (sesion != null && sesion.getMapaRecursos() != null && sesion.getMapaRecursos().containsKey(ruta)) {
				retorno = true;
			}
		}

		return retorno;
	}

	public static HttpSession getSession() {

		return (HttpSession)
		          FacesContext.
		          getCurrentInstance().
		          getExternalContext().
		          getSession(false);
	}
	
	public static HttpServletRequest getRequest() {
	       return (HttpServletRequest) FacesContext.
	          getCurrentInstance().
	          getExternalContext().getRequest();
	      }
	
	public static String getNvNombreUsuario()
    {
      HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
      return  session.getAttribute("nvNombreUsuario").toString();
    }
}
