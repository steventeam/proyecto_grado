package co.edu.asys.transversal.util;

import java.io.Serializable;

public class RecursoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private RecursoDTO recursoPadre;
	private TipoRecursoDTO tipoRecurso;
	private String nombre;
	private String url;
	private int orden;

	public RecursoDTO(Integer codigo, RecursoDTO recursoPadre, TipoRecursoDTO tipoRecurso, String nombre, String url, int orden) {
		super();
		this.codigo = codigo;
		this.recursoPadre = recursoPadre;
		this.tipoRecurso = tipoRecurso;
		this.nombre = nombre;
		this.url = url;
		this.orden = orden;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public RecursoDTO getRecursoPadre() {
		return recursoPadre;
	}

	public TipoRecursoDTO getTipoRecurso() {
		return tipoRecurso;
	}

	public String getNombre() {
		return nombre;
	}

	public String getUrl() {
		return url;
	}

	public int getOrden() {
		return orden;
	}
}
