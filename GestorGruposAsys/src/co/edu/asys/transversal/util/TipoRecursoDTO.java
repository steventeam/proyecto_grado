package co.edu.asys.transversal.util;

import java.io.Serializable;

public class TipoRecursoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String nombre;

	public TipoRecursoDTO(Integer codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}
}
