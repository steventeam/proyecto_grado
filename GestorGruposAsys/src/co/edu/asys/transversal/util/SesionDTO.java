package co.edu.asys.transversal.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class SesionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private UsuarioDTO usuario;
	private HashMap<String, RecursoDTO> mapaRecursos;
	private Date fechaInicioSesion;

	public SesionDTO(UsuarioDTO usuario, HashMap<String, RecursoDTO> mapaRecursos) {
		super();
		this.usuario = usuario;
		this.mapaRecursos = mapaRecursos;
		this.fechaInicioSesion = Calendar.getInstance().getTime();
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public HashMap<String, RecursoDTO> getMapaRecursos() {
		return mapaRecursos;
	}

	public Date getFechaInicioSesion() {
		return fechaInicioSesion;
	}
}