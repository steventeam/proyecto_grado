package co.edu.asys.transversal.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblRecuperarClave;
import co.edu.asys.gestorgrupos.negocio.fachada.RecuperarClaveFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="mailBean")
@SessionScoped
public class UtilMail {
	
	private SegUsuario usuario;
	private ArrayList<SegUsuario> listaUsuario;
	
	private TblRecuperarClave recuperarClave;
	
	public TblRecuperarClave getRecuperarClave() {
		return recuperarClave;
	}

	public void setRecuperarClave(TblRecuperarClave recuperarClave) {
		this.recuperarClave = recuperarClave;
	}

	public ArrayList<SegUsuario> getListaUsuario() {
		return listaUsuario;
	}

	public SegUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(SegUsuario usuario) {
		this.usuario = usuario;
	}
	
	public UtilMail(){
		usuario = new SegUsuario();
	}
	
	// Agrega mensaje a faces
		private void gestionarMensaje(Severity severidad, String titulo, String contenido) {

			FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
		}
		
		public static void main(String[] args) throws Exception {
			
			

			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			String tokenReceived = params.get("tk");
			System.out.println("TOKEN RECIBIDO -------------- " + tokenReceived);
			
			
			TblRecuperarClave recuperarClave = new TblRecuperarClave();
			
//			recuperarClave = recuperarClave.consultarPorCodigo(tokenReceived);
//			
//			RecuperarClaveFachada recuperarClaveFachada = LocalizadorBean.ObtenerBean(RecuperarClaveFachada.class);
//			recuperarClaveFachada.consultarPorCodigo(recuperarClave);
//			
////			recuperarClave = TblRecuperarClave.consultarPorCodigo(tokenReceived);
//			
//			if (TblRecuperarClave.consultarPorCodigo(recuperarClave).isEstadoToken()){
//				System.out.println("el token ya fue usado, inicie el proceso nuevamente");
//			}else if (TblRecuperarClave.consultarPorCodigo(recuperarClave).getFechaExpiracion().compareTo(new Date()) > 0){
//				System.out.println("el token expiro");
//			}else{
//				//se puede hacer la modificación
//			}
			
		}

	public void recuperarClave() {
		
		
		try {
			RecuperarClaveFachada recuperarClaveFachada = LocalizadorBean.ObtenerBean(RecuperarClaveFachada.class);
			
			
			if(recuperarClave.getSegUsuario().getNvCorreoElectronico() != null && !recuperarClave.getSegUsuario().getNvCorreoElectronico().trim().equals("")){
				
				String uuid = UUID.randomUUID().toString();
				
				recuperarClave.setToken(uuid);
				recuperarClave.setFechaExpiracion(new Date());
				recuperarClave.setEstadoToken(false);
				recuperarClaveFachada.crear(recuperarClave);
				
				final String username = CatalogoMensajes.obtenerMensaje("NV_CORREO");
				final String password = CatalogoMensajes.obtenerMensaje("NV_CLAVE");
				
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", "smtp.live.com");
				props.put("mail.smtp.port", "25");
				
				Session session = Session.getInstance(props,
						new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
				
								
				try {
					
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(usuario.getNvCorreoElectronico()));
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(usuario.getNvCorreoElectronico()));
					message.setSubject("Recuperar contraseña");
					message.setText("https://www.facebook.com/");
					
					Transport.send(message);
					
					System.out.println("Done");
					
				} catch (MessagingException e) {
					throw new RuntimeException(e);
				}
			
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			}
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}		
	}
}
