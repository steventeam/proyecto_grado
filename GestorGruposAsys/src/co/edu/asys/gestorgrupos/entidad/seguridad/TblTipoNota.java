package co.edu.asys.gestorgrupos.entidad.seguridad;
// Generated 18-dic-2016 11:47:39 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * TblTipoNota generated by hbm2java
 */
@Entity
@Table(name = "tbl_tipo_nota", catalog = "bd_tesis", uniqueConstraints = @UniqueConstraint(columnNames = "NV_NOMBRE") )
public class TblTipoNota implements java.io.Serializable {	private static final long serialVersionUID = 1L;

	private Integer inCodigo;
	private String nvNombre;
	private Set<TblNotaXGrupo> tblNotaXGrupos = new HashSet<TblNotaXGrupo>(0);

	public TblTipoNota() {
	}

	public TblTipoNota(String nvNombre) {
		this.nvNombre = nvNombre;
	}

	public TblTipoNota(String nvNombre, Set<TblNotaXGrupo> tblNotaXGrupos) {
		this.nvNombre = nvNombre;
		this.tblNotaXGrupos = tblNotaXGrupos;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "IN_CODIGO", unique = true, nullable = false)
	public Integer getInCodigo() {
		return this.inCodigo;
	}

	public void setInCodigo(Integer inCodigo) {
		this.inCodigo = inCodigo;
	}

	@Column(name = "NV_NOMBRE", unique = true, nullable = false, length = 100)
	public String getNvNombre() {
		return this.nvNombre;
	}

	public void setNvNombre(String nvNombre) {
		this.nvNombre = nvNombre;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tblTipoNota")
	public Set<TblNotaXGrupo> getTblNotaXGrupos() {
		return this.tblNotaXGrupos;
	}

	public void setTblNotaXGrupos(Set<TblNotaXGrupo> tblNotaXGrupos) {
		this.tblNotaXGrupos = tblNotaXGrupos;
	}

}
