package co.edu.asys.gestorgrupos.entidad.seguridad;
// Generated 23-mar-2017 23:48:01 by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TblRecuperarClave generated by hbm2java
 */
@Entity
@Table(name = "tbl_recuperar_clave", catalog = "bd_tesis")
public class TblRecuperarClave implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String token;
	private SegUsuario segUsuario;
	private boolean estadoToken;
	private Date fechaExpiracion;

	public TblRecuperarClave() {
	}

	public TblRecuperarClave(String token, SegUsuario segUsuario, boolean estadoToken, Date fechaExpiracion) {
		this.token = token;
		this.segUsuario = segUsuario;
		this.estadoToken = estadoToken;
		this.fechaExpiracion = fechaExpiracion;
	}

	@Id

	@Column(name = "TOKEN", unique = true, nullable = false, length = 135)
	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IN_CODIGO_USUARIO", nullable = false)
	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	@Column(name = "ESTADO_TOKEN", nullable = false)
	public boolean isEstadoToken() {
		return this.estadoToken;
	}

	public void setEstadoToken(boolean estadoToken) {
		this.estadoToken = estadoToken;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_EXPIRACION", nullable = false, length = 0)
	public Date getFechaExpiracion() {
		return this.fechaExpiracion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	public static TblRecuperarClave consultarPorCodigo(TblRecuperarClave recuperarClave) {
		// TODO Auto-generated method stub
		recuperarClave.estadoToken = false;
		recuperarClave.fechaExpiracion = new Date();
		recuperarClave.token = UUID.randomUUID().toString();
		
		return recuperarClave;
	}

}
