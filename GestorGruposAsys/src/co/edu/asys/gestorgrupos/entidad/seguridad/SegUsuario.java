package co.edu.asys.gestorgrupos.entidad.seguridad;
//Generated 18-dic-2016 11:47:39 by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
* SegUsuario generated by hbm2java
*/
@Entity
@Table(name = "seg_usuario", catalog = "bd_tesis", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "IN_CODIGO_TIPO_IDENTIFICACION", "NV_NUMERO_IDENTIFICACION" }),
		@UniqueConstraint(columnNames = "NV_NOMBRE_USUARIO"),
		@UniqueConstraint(columnNames = "NV_CORREO_ELECTRONICO") })
public class SegUsuario implements java.io.Serializable {	private static final long serialVersionUID = 1L;

	private Integer inCodigo;
	private SegPerfil segPerfil;
	private SegTipoIdentificacion segTipoIdentificacion;
	private TblGenero tblGenero;
	private String nvNumeroIdentificacion;
	private String nvPrimerNombre;
	private String nvSegundoNombre;
	private String nvPrimerApellido;
	private String nvSegundoApellido;
	private String nvCorreoElectronico;
	private String nvTelefonoMovil;
	private String nvTelefonoFijo;
	private String nvNombreUsuario;
	private String nvClave;
	private Date dtFechaEliminacion;
	private Set<SegPreguntaUsuario> segPreguntaUsuarios = new HashSet<SegPreguntaUsuario>(0);
	private Set<TblEstudianteXInstitucion> tblEstudianteXInstitucions = new HashSet<TblEstudianteXInstitucion>(0);
	private Set<TblProfesorXInstitucion> tblProfesorXInstitucions = new HashSet<TblProfesorXInstitucion>(0);
	private Set<TblRecuperarClave> tblRecuperarClaves = new HashSet<TblRecuperarClave>(0);

	public SegUsuario() {
	}

	public SegUsuario(SegPerfil segPerfil, SegTipoIdentificacion segTipoIdentificacion, TblGenero tblGenero,
			String nvNumeroIdentificacion, String nvPrimerNombre, String nvPrimerApellido, String nvCorreoElectronico,
			String nvTelefonoMovil, String nvNombreUsuario, String nvClave) {
		this.segPerfil = segPerfil;
		this.segTipoIdentificacion = segTipoIdentificacion;
		this.tblGenero = tblGenero;
		this.nvNumeroIdentificacion = nvNumeroIdentificacion;
		this.nvPrimerNombre = nvPrimerNombre;
		this.nvPrimerApellido = nvPrimerApellido;
		this.nvCorreoElectronico = nvCorreoElectronico;
		this.nvTelefonoMovil = nvTelefonoMovil;
		this.nvNombreUsuario = nvNombreUsuario;
		this.nvClave = nvClave;
	}

	public SegUsuario(SegPerfil segPerfil, SegTipoIdentificacion segTipoIdentificacion, TblGenero tblGenero,
			String nvNumeroIdentificacion, String nvPrimerNombre, String nvSegundoNombre, String nvPrimerApellido,
			String nvSegundoApellido, String nvCorreoElectronico, String nvTelefonoMovil, String nvTelefonoFijo,
			String nvNombreUsuario, String nvClave, Date dtFechaEliminacion,
			Set<SegPreguntaUsuario> segPreguntaUsuarios, Set<TblEstudianteXInstitucion> tblEstudianteXInstitucions,
			Set<TblProfesorXInstitucion> tblProfesorXInstitucions, Set<TblRecuperarClave> tblRecuperarClaves) {
		this.segPerfil = segPerfil;
		this.segTipoIdentificacion = segTipoIdentificacion;
		this.tblGenero = tblGenero;
		this.nvNumeroIdentificacion = nvNumeroIdentificacion;
		this.nvPrimerNombre = nvPrimerNombre;
		this.nvSegundoNombre = nvSegundoNombre;
		this.nvPrimerApellido = nvPrimerApellido;
		this.nvSegundoApellido = nvSegundoApellido;
		this.nvCorreoElectronico = nvCorreoElectronico;
		this.nvTelefonoMovil = nvTelefonoMovil;
		this.nvTelefonoFijo = nvTelefonoFijo;
		this.nvNombreUsuario = nvNombreUsuario;
		this.nvClave = nvClave;
		this.dtFechaEliminacion = dtFechaEliminacion;
		this.segPreguntaUsuarios = segPreguntaUsuarios;
		this.tblEstudianteXInstitucions = tblEstudianteXInstitucions;
		this.tblProfesorXInstitucions = tblProfesorXInstitucions;
		this.tblRecuperarClaves = tblRecuperarClaves;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "IN_CODIGO", unique = true, nullable = false)
	public Integer getInCodigo() {
		return this.inCodigo;
	}

	public void setInCodigo(Integer inCodigo) {
		this.inCodigo = inCodigo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IN_CODIGO_PERFIL", nullable = false)
	public SegPerfil getSegPerfil() {
		return this.segPerfil;
	}

	public void setSegPerfil(SegPerfil segPerfil) {
		this.segPerfil = segPerfil;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IN_CODIGO_TIPO_IDENTIFICACION", nullable = false)
	public SegTipoIdentificacion getSegTipoIdentificacion() {
		return this.segTipoIdentificacion;
	}

	public void setSegTipoIdentificacion(SegTipoIdentificacion segTipoIdentificacion) {
		this.segTipoIdentificacion = segTipoIdentificacion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IN_CODIGO_GENERO", nullable = false)
	public TblGenero getTblGenero() {
		return this.tblGenero;
	}

	public void setTblGenero(TblGenero tblGenero) {
		this.tblGenero = tblGenero;
	}

	@Column(name = "NV_NUMERO_IDENTIFICACION", nullable = false, length = 50)
	public String getNvNumeroIdentificacion() {
		return this.nvNumeroIdentificacion;
	}

	public void setNvNumeroIdentificacion(String nvNumeroIdentificacion) {
		this.nvNumeroIdentificacion = nvNumeroIdentificacion;
	}

	@Column(name = "NV_PRIMER_NOMBRE", nullable = false, length = 50)
	public String getNvPrimerNombre() {
		return this.nvPrimerNombre;
	}

	public void setNvPrimerNombre(String nvPrimerNombre) {
		this.nvPrimerNombre = nvPrimerNombre;
	}

	@Column(name = "NV_SEGUNDO_NOMBRE", length = 50)
	public String getNvSegundoNombre() {
		return this.nvSegundoNombre;
	}

	public void setNvSegundoNombre(String nvSegundoNombre) {
		this.nvSegundoNombre = nvSegundoNombre;
	}

	@Column(name = "NV_PRIMER_APELLIDO", nullable = false, length = 50)
	public String getNvPrimerApellido() {
		return this.nvPrimerApellido;
	}

	public void setNvPrimerApellido(String nvPrimerApellido) {
		this.nvPrimerApellido = nvPrimerApellido;
	}

	@Column(name = "NV_SEGUNDO_APELLIDO", length = 50)
	public String getNvSegundoApellido() {
		return this.nvSegundoApellido;
	}

	public void setNvSegundoApellido(String nvSegundoApellido) {
		this.nvSegundoApellido = nvSegundoApellido;
	}

	@Column(name = "NV_CORREO_ELECTRONICO", unique = true, nullable = false, length = 50)
	public String getNvCorreoElectronico() {
		return this.nvCorreoElectronico;
	}

	public void setNvCorreoElectronico(String nvCorreoElectronico) {
		this.nvCorreoElectronico = nvCorreoElectronico;
	}

	@Column(name = "NV_TELEFONO_MOVIL", nullable = false, length = 50)
	public String getNvTelefonoMovil() {
		return this.nvTelefonoMovil;
	}

	public void setNvTelefonoMovil(String nvTelefonoMovil) {
		this.nvTelefonoMovil = nvTelefonoMovil;
	}

	@Column(name = "NV_TELEFONO_FIJO", length = 50)
	public String getNvTelefonoFijo() {
		return this.nvTelefonoFijo;
	}

	public void setNvTelefonoFijo(String nvTelefonoFijo) {
		this.nvTelefonoFijo = nvTelefonoFijo;
	}

	@Column(name = "NV_NOMBRE_USUARIO", unique = true, nullable = false, length = 50)
	public String getNvNombreUsuario() {
		return this.nvNombreUsuario;
	}

	public void setNvNombreUsuario(String nvNombreUsuario) {
		this.nvNombreUsuario = nvNombreUsuario;
	}

	@Column(name = "NV_CLAVE", nullable = false)
	public String getNvClave() {
		return this.nvClave;
	}

	public void setNvClave(String nvClave) {
		this.nvClave = nvClave;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_FECHA_ELIMINACION", length = 0)
	public Date getDtFechaEliminacion() {
		return this.dtFechaEliminacion;
	}

	public void setDtFechaEliminacion(Date dtFechaEliminacion) {
		this.dtFechaEliminacion = dtFechaEliminacion;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "segUsuario")
	public Set<SegPreguntaUsuario> getSegPreguntaUsuarios() {
		return this.segPreguntaUsuarios;
	}

	public void setSegPreguntaUsuarios(Set<SegPreguntaUsuario> segPreguntaUsuarios) {
		this.segPreguntaUsuarios = segPreguntaUsuarios;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "segUsuario")
	public Set<TblEstudianteXInstitucion> getTblEstudianteXInstitucions() {
		return this.tblEstudianteXInstitucions;
	}

	public void setTblEstudianteXInstitucions(Set<TblEstudianteXInstitucion> tblEstudianteXInstitucions) {
		this.tblEstudianteXInstitucions = tblEstudianteXInstitucions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "segUsuario")
	public Set<TblProfesorXInstitucion> getTblProfesorXInstitucions() {
		return this.tblProfesorXInstitucions;
	}

	public void setTblProfesorXInstitucions(Set<TblProfesorXInstitucion> tblProfesorXInstitucions) {
		this.tblProfesorXInstitucions = tblProfesorXInstitucions;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "segUsuario")
	public Set<TblRecuperarClave> getTblRecuperarClaves() {
		return this.tblRecuperarClaves;
	}

	public void setTblRecuperarClaves(Set<TblRecuperarClave> tblRecuperarClaves) {
		this.tblRecuperarClaves = tblRecuperarClaves;
	}

}