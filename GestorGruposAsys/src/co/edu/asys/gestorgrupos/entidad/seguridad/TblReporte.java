package co.edu.asys.gestorgrupos.entidad.seguridad;
// Generated 18-dic-2016 11:47:39 by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TblReporte generated by hbm2java
 */
@Entity
@Table(name = "tbl_reporte", catalog = "bd_tesis")
public class TblReporte implements java.io.Serializable {	private static final long serialVersionUID = 1L;

	private int inCodigo;
	private String nvDescripcion;

	public TblReporte() {
	}

	public TblReporte(int inCodigo, String nvDescripcion) {
		this.inCodigo = inCodigo;
		this.nvDescripcion = nvDescripcion;
	}

	@Id

	@Column(name = "IN_CODIGO", unique = true, nullable = false)
	public int getInCodigo() {
		return this.inCodigo;
	}

	public void setInCodigo(int inCodigo) {
		this.inCodigo = inCodigo;
	}

	@Column(name = "NV_DESCRIPCION", nullable = false)
	public String getNvDescripcion() {
		return this.nvDescripcion;
	}

	public void setNvDescripcion(String nvDescripcion) {
		this.nvDescripcion = nvDescripcion;
	}

}
