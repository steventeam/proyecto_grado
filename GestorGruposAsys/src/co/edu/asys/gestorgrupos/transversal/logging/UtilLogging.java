package co.edu.asys.gestorgrupos.transversal.logging;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

public class UtilLogging {
	
	public static <T> void escribirLog(Class<T> clase, Priority nivel, Throwable excepcion, String codigoMensaje, Object... parametros) {
		
		//Preguntar si el nivel de logging est� habilitado
		if(Logger.getLogger(clase).isEnabledFor(nivel)) {
			Logger.getLogger(clase).log (nivel, CatalogoMensajes.obtenerMensaje(codigoMensaje, parametros), excepcion);
		}
	}
	
	public static <T> void escribirLog(Class<T> clase, Priority nivel, Throwable excepcion) {
		
		//Preguntar si el nivel de logging est� habilitado
		if(Logger.getLogger(clase).isEnabledFor(nivel)) {
			Logger.getLogger(clase).log (nivel, null, excepcion);
		}
	}

}
