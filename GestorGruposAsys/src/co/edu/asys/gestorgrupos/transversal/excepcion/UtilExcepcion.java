package co.edu.asys.gestorgrupos.transversal.excepcion;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;

public class UtilExcepcion {
	
	public static <T> void controlarExcepcion(String mensajeUsuario, String mensajeTecnico, Exception excepcion, LugarExcepcionEnum lugar, boolean hacerLog, Class<T> clase) throws ExcepcionAplicacionGrupos{
		
		ExcepcionAplicacionGrupos excepcionCustomizada = null;
		
		if(excepcion == null) {
			
			if(mensajeTecnico == null){
				excepcionCustomizada = new ExcepcionAplicacionGrupos(lugar, mensajeUsuario);
			}else{
				excepcionCustomizada = new ExcepcionAplicacionGrupos(lugar, mensajeUsuario, mensajeTecnico);
			}
			
		}else if(excepcion instanceof ExcepcionAplicacionGrupos) {
			excepcionCustomizada = (ExcepcionAplicacionGrupos) excepcion;
		}else {
			excepcionCustomizada = new ExcepcionAplicacionGrupos(lugar, mensajeUsuario, mensajeTecnico, excepcion);
		}
		
		if(hacerLog == true) {
			UtilLogging.escribirLog(clase, Level.ERROR, excepcionCustomizada);
		}
		
		throw excepcionCustomizada;
	}

}
