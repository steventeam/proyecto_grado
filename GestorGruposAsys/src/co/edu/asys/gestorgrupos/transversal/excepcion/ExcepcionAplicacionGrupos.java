package co.edu.asys.gestorgrupos.transversal.excepcion;

import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;

public class ExcepcionAplicacionGrupos extends Exception {
	
	public static final long serialVersionUID = 1L;
	
		

		private final String mensajeUsuario;
		private final String mensajeTecnico;
		private final LugarExcepcionEnum lugarExcepcion;
		
		public ExcepcionAplicacionGrupos(LugarExcepcionEnum lugar, String mensajeUsuario, String mensajeTecnico) {
			this.mensajeUsuario = mensajeUsuario;
			this.mensajeTecnico = mensajeTecnico;
			this.lugarExcepcion = lugar;
			
		}
		
		public ExcepcionAplicacionGrupos(LugarExcepcionEnum lugar, String mensajeUsuario) {
			this.mensajeUsuario = mensajeUsuario;
			this.mensajeTecnico = mensajeUsuario;
			this.lugarExcepcion = lugar;
		}
		
		public ExcepcionAplicacionGrupos(LugarExcepcionEnum lugar, String mensajeUsuario, String mensajeTecnico, Exception excepcion) {
			this.mensajeUsuario = mensajeUsuario;
			this.mensajeTecnico = mensajeTecnico;
			this.lugarExcepcion = lugar;
			
		}
		
		public String getMensajeUsuario() {
		return mensajeUsuario;
	}

	public String getMensajeTecnico() {
		return mensajeTecnico;
	}

	public LugarExcepcionEnum getLugarExcepcion() {
		return lugarExcepcion;
	}

}
