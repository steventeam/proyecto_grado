package co.edu.asys.gestorgrupos.transversal.mensajes;

import java.util.Locale;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;


public class CatalogoMensajes {
	
	private static ReloadableResourceBundleMessageSource CATALOGO_MENSAJES = (ReloadableResourceBundleMessageSource) LocalizadorBean.obtenerBean("catalogoMensajes");
	
	public static String obtenerMensaje(String codigoMensaje, Object... parametros) {
		return CATALOGO_MENSAJES.getMessage(codigoMensaje, parametros, Locale.getDefault());
	}

}
