package co.edu.asys.gestorgrupos.transversal.filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.edu.asys.transversal.util.UtilSesion;


	@WebFilter(filterName = "Filtro", urlPatterns = {"*"})
	public class Filtro implements Filter{
			     
	    public Filtro() {
	    }
	 
	    @Override
	    public void init(FilterConfig filterConfig) throws ServletException {
	         
	    }
	 
	    @Override
	    public void doFilter(ServletRequest peticion, ServletResponse respuesta, FilterChain cadena) throws IOException, ServletException {
	         	 
	            // check whether session variable is set
	            HttpServletRequest peticionHttp = (HttpServletRequest) peticion;
	            HttpServletResponse respuestaHttp = (HttpServletResponse) respuesta;
//	            HttpSession sesion = peticionHttp.getSession(false);
	            //  allow user to proccede if url is login.xhtml or user logged in or user is accessing any page in //public folder
	            
	            String contextoAplicacion = peticionHttp.getServletContext().getServletContextName();
	            String peticionActual = peticionHttp.getRequestURI();
	            
	            String rutaRecursosPublicos = "/" + contextoAplicacion + "/faces/paginas/publico";
	    		String rutaRecursosFaces = "/" + contextoAplicacion + "/faces/javax.faces.resource";
	    		String rutaImagenesFaces = "/" + contextoAplicacion + "/imagenes";
	            
	    		if (peticionActual.startsWith(rutaRecursosPublicos) || peticionActual.startsWith(rutaRecursosFaces) || peticionActual.startsWith(rutaImagenesFaces)) {
	    			System.out.println("No debemos autorizar para acceda a: " + peticionActual);
	    			cadena.doFilter(peticion, respuesta);
	    		} else {
	    			System.out.println("Validar que el usuario tenga permisos para: " + peticionActual);
	    			boolean estaAutorizado = UtilSesion.obtenerUtilSesion().estaAutorizado(peticionHttp, peticionActual);

	    			if (estaAutorizado) {
	    				System.out.println("Si est� autorizado");
	    				cadena.doFilter(peticion, respuesta);
	    			} else {
	    				System.out.println("NO est� autorizado");
	    				respuestaHttp.sendRedirect(rutaRecursosPublicos + "/Error.xhtml");
	    			}
	    		}
	    
	    } //doFilter

		@Override
		public void destroy() {
			// TODO Auto-generated method stub
			
		}
	 
//	    public void destroy() {
//	         
//	    }
	
}
