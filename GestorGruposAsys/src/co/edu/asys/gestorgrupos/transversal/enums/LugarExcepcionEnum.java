package co.edu.asys.gestorgrupos.transversal.enums;

public enum LugarExcepcionEnum {
	
	DATOS,
	NEGOCIO,
	FACHADA,
	BEAN,
	TRANSVERSAL

}
