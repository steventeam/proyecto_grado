package co.edu.asys.gestorgrupos.transversal.aop;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

public class LocalizadorBean {
	
	public static <T> T ObtenerBean(Class<T> tipoClase) {
		return (T) ContextSingletonBeanFactoryLocator.getInstance().useBeanFactory("contextoAplicacion").getFactory().getBean(tipoClase);
		
	}
	
	public static Object obtenerBean(String nombreBean){
		return  ContextSingletonBeanFactoryLocator.getInstance().useBeanFactory("contextoAplicacion").getFactory().getBean(nombreBean);
	}

}
