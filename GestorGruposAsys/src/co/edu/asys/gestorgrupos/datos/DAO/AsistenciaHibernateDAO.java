package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblAsistencia;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class AsistenciaHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblAsistencia asistencia) throws Exception {
			
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_CREACION_DATOS");
			
			sesion.save(asistencia);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ASISTENCIA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblAsistencia consultarPorCodigo(TblAsistencia asistencia) {
		TblAsistencia asistenciaConsultado = sesion.load(TblAsistencia.class, asistencia.getInCodigo());
		return asistenciaConsultado;
		
	
	}
	
	public void actualizarConMerge(TblAsistencia asistencia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblAsistencia asistenciaActualizar = consultarPorCodigo(asistencia);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			

			//Pendiente colocar campos asistencia ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(asistenciaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ASISTENCIA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblAsistencia asistencia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ACTUALIZACION_DATOS");
			sesion.update(asistencia);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ASISTENCIA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblAsistencia asistencia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblAsistencia asistenciaInhabilitar = consultarPorCodigo(asistencia);
			
			sesion.merge(asistenciaInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ELIMINACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ASISTENCIA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblAsistencia> consultarPorFiltro(TblAsistencia asistencia)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblAsistencia.class);

		//	
		if (asistencia != null){
			
			//Validamos si consulta por el c�digo
			if (asistencia.getTblEstudianteXGrupo() != null && asistencia.getTblEstudianteXGrupo().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblEstudianteXGrupo.inCodigo", asistencia.getTblEstudianteXGrupo().getInCodigo()));
				
			}
			
			//Validamos si consulta por el c�digo
			if (asistencia.getTblSesion() != null && asistencia.getTblSesion().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblSesion.inCodigo", asistencia.getTblSesion().getInCodigo()));
				
			}

		}
		
		return (ArrayList<TblAsistencia>)consulta.list();
	}
}
