package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblRecuperarClave;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("recuperarClaveHibernateDAO")
@Scope("prototype")
public class RecuperarClaveHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	
	}
	
	public void crear(TblRecuperarClave recuperarClave) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_CREACION_DATOS");
			
			
			
			sesion.save(recuperarClave);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROGRAMA_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblRecuperarClave consultarPorToken(TblRecuperarClave recuperarClave) {
		TblRecuperarClave recuperarClaveConsultado = sesion.load(TblRecuperarClave.class, recuperarClave.getToken());
		return recuperarClaveConsultado;
	
	}
	
	
	public void actualizarConMerge(TblRecuperarClave recuperarClave) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblRecuperarClave recuperarClaveActualizar = consultarPorToken(recuperarClave);
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			
			
			//3. Hacemos un merge de la información
			sesion.merge(recuperarClaveActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_APLICACION_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	
	/*@SuppressWarnings("unchecked")
	public ArrayList<TblRecuperarClave> consultarTodo() throws Exception {
		ArrayList<TblRecuperarClave> listaTotal =
				(ArrayList<TblRecuperarClave>)
				sesion.createQuery("from TblRecuperarClave").list();
		
		return listaTotal;
	} 
	*/
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblRecuperarClave> consultarPorFiltro(TblRecuperarClave recuperarClave)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblRecuperarClave.class);

		//	
		if (recuperarClave != null){
			
			//Validamos si consulta por el token
			if (recuperarClave.getToken() != null && !recuperarClave.getToken().trim().equals("")) {
				consulta.add(Restrictions.eq("token", recuperarClave.getToken()));
				
			}
			
		}
		
		return (ArrayList<TblRecuperarClave>)consulta.list();
	}

	
}
