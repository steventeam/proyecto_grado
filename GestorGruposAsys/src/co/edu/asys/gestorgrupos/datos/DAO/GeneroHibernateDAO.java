package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class GeneroHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblGenero genero) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GENERO_ANTES_CREACION_DATOS", genero.getNvNombre());
			sesion.save(genero);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GENERO_DESPUES_CREACION_DATOS", genero.getNvNombre());

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_GENERO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GENERO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblGenero consultarPorCodigo(TblGenero genero) {
		TblGenero generoConsultado = sesion.load(TblGenero.class, genero.getInCodigo());
		return generoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblGenero genero) throws Exception {
		
		//1. Consultamos el objeto persistente
		TblGenero generoActualizar = consultarPorCodigo(genero);
		
		
		//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
		
		generoActualizar.setNvNombre(genero.getNvNombre());
		

		/**generoActualizar.setNvCorreoElectronico(genero.getNvCorreoElectronico());
		generoActualizar.setNvTelefonoMovil(genero.getNvTelefonoMovil());
		generoActualizar.setNvTelefonoFijo(genero.getNvTelefonoFijo());
		generoActualizar.setNvNombreUsuario(genero.getNvNombreUsuario());
		generoActualizar.setNvClave(genero.getNvClave());*/
		
		//Pendiente colocar campos genero ======================================
		
		
		//Pendiente colocar genero creaci�n
		
		//3. Hacemos un merge de la informaci�n
		sesion.merge(generoActualizar);
		
	}
	
	public void actualizarConUpdate(TblGenero genero) throws Exception {
		
		//Pendiente colocar genero creaci�n
		
		sesion.update(genero);
		
	}
	
	public void inhabilitarConMerge(TblGenero genero) throws Exception {
		
		//1. Consultamos el objeto persistente
		TblGenero generoInhabilitar = consultarPorCodigo(genero);
		
		
		//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
		
		//Pendiente colocar genero eliminaci�n
		
		//3. Hacemos un merge de la informaci�n
		sesion.merge(generoInhabilitar);
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblGenero> consultarPorFiltro(TblGenero genero)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblGenero.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (genero != null){
			
			//Validamos si consulta por el c�digo
			if (genero.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("inCodigo", genero.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (genero.getNvNombre() != null && !genero.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", genero.getNvNombre().trim()));
			}
			
			
		}
		
		return (ArrayList<TblGenero>)consulta.list();
	}
}
