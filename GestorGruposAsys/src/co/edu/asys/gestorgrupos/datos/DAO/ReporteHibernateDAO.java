package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblReporte;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class ReporteHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblReporte reporte) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_CREACION_DATOS", reporte.getNvDescripcion());
			sesion.save(reporte);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_CREACION_DATOS", reporte.getNvDescripcion());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblReporte consultarPorCodigo(TblReporte reporte) {
		TblReporte reporteConsultado = sesion.load(TblReporte.class, reporte.getInCodigo());
		return reporteConsultado;
		
	
	}
	
	public void actualizarConMerge(TblReporte reporte) throws Exception {
		
		//1. Consultamos el objeto persistente
		TblReporte reporteActualizar = consultarPorCodigo(reporte);
		
		
		//2. Vaciamos al objeto persistente la información que vamos a actualizar
		
		reporteActualizar.setNvDescripcion(reporte.getNvDescripcion());
		
		

		
		//Pendiente colocar campos reporte ======================================
	
		
		//3. Hacemos un merge de la información
		sesion.merge(reporteActualizar);
		
	}
	
	public void actualizarConUpdate(TblReporte reporte) throws Exception {
		
		//Pendiente colocar reporte creación
		
		sesion.update(reporte);
		
	}
	
	public void inhabilitarConMerge(TblReporte reporte) throws Exception {
		
		//1. Consultamos el objeto persistente
		TblReporte reporteInhabilitar = consultarPorCodigo(reporte);
		
		
		//2. Vaciamos al objeto persistente la información que vamos a actualizar
		
		//Pendiente colocar reporte eliminación
		
		//3. Hacemos un merge de la información
		sesion.merge(reporteInhabilitar);
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblReporte> consultarPorFiltro(TblReporte reporte)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblReporte.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (reporte != null){
			
			//Validamos si consulta por el código
			if (reporte.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", reporte.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (reporte.getNvDescripcion() != null && !reporte.getNvDescripcion().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", reporte.getNvDescripcion().trim()));
			}
			
		}
		
		return (ArrayList<TblReporte>)consulta.list();
	}
}
