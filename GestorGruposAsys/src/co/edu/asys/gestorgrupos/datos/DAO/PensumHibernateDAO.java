package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensum;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class PensumHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblPensum pensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_CREACION_DATOS", pensum.getNvNombre());
			sesion.save(pensum);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_CREACION_DATOS", pensum.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblPensum consultarPorCodigo(TblPensum pensum) {
		TblPensum pensumConsultado = sesion.load(TblPensum.class, pensum.getInCodigo());
		return pensumConsultado;
		
	
	}
	
	public void actualizarConMerge(TblPensum pensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_ACTUALIZACION_DATOS", pensum.getNvNombre());

			//1. Consultamos el objeto persistente
			TblPensum pensumActualizar = consultarPorCodigo(pensum);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			pensumActualizar.setNvNombre(pensum.getNvNombre());
			
			//Pendiente colocar campos pensum ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(pensumActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_ACTUALIZACION_DATOS", pensum.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblPensum pensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_ACTUALIZACION_DATOS", pensum.getNvNombre());
			sesion.update(pensum);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_ACTUALIZACION_DATOS", pensum.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			}	
		}
	
	public void inhabilitarConMerge(TblPensum pensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_ELIMINACION_DATOS", pensum.getNvNombre());

			//1. Consultamos el objeto persistente
			TblPensum pensumInhabilitar = consultarPorCodigo(pensum);
			
			sesion.merge(pensumInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_ELIMINACION_DATOS", pensum.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblPensum> consultarPorFiltro(TblPensum pensum)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblPensum.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (pensum != null){
			
			//Validamos si consulta por el c�digo
			if (pensum.getInCodigo() != null && pensum.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", pensum.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (pensum.getNvNombre() != null && !pensum.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", pensum.getNvNombre().trim()));
			}
			
		}
		
		return (ArrayList<TblPensum>)consulta.list();
	}
}
