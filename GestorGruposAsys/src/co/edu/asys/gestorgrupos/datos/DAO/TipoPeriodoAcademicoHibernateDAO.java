package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPeriodoAcademico;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class TipoPeriodoAcademicoHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_CREACION_DATOS", tipoPeriodoAcademico.getNvNombre());
			sesion.save(tipoPeriodoAcademico);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CREACION_DATOS", tipoPeriodoAcademico.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblTipoPeriodoAcademico consultarPorCodigo(TblTipoPeriodoAcademico tipoPeriodoAcademico) {
		TblTipoPeriodoAcademico tipoPeriodoAcademicoConsultado = sesion.load(TblTipoPeriodoAcademico.class, tipoPeriodoAcademico.getInCodigo());
		return tipoPeriodoAcademicoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_DATOS", tipoPeriodoAcademico.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoPeriodoAcademico tipoPeriodoAcademicoActualizar = consultarPorCodigo(tipoPeriodoAcademico);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			tipoPeriodoAcademicoActualizar.setNvNombre(tipoPeriodoAcademico.getNvNombre());
			tipoPeriodoAcademicoActualizar.setNvDescripcion(tipoPeriodoAcademico.getNvDescripcion());
			
			
			//Pendiente colocar campos tipoPeriodoAcademico ======================================
			
			
			//Pendiente colocar tipoPeriodoAcademico creaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoPeriodoAcademicoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_DATOS", tipoPeriodoAcademico.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_DATOS", tipoPeriodoAcademico.getNvNombre());
			//Pendiente colocar tipoPeriodoAcademico creaci�n
			
			sesion.update(tipoPeriodoAcademico);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_DATOS", tipoPeriodoAcademico.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ELIMINACION_DATOS", tipoPeriodoAcademico.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoPeriodoAcademico tipoPeriodoAcademicoInhabilitar = consultarPorCodigo(tipoPeriodoAcademico);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar tipoPeriodoAcademico eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoPeriodoAcademicoInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ELIMINACION_DATOS", tipoPeriodoAcademico.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblTipoPeriodoAcademico> consultarPorFiltro(TblTipoPeriodoAcademico tipoPeriodoAcademico)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblTipoPeriodoAcademico.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (tipoPeriodoAcademico != null){
			
			//Validamos si consulta por el c�digo
			if (tipoPeriodoAcademico.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("inCodigo", tipoPeriodoAcademico.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (tipoPeriodoAcademico.getNvNombre() != null && !tipoPeriodoAcademico.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", tipoPeriodoAcademico.getNvNombre().trim()));
			}
			 
			//Validamos si consulta por la descripci�n
			 if (tipoPeriodoAcademico.getNvDescripcion() != null && !tipoPeriodoAcademico.getNvDescripcion().trim().equals("")) {
				consulta.add(Restrictions.eq("nvDescripcion", tipoPeriodoAcademico.getNvDescripcion().trim()));
			}
			
			
		}
		
		return (ArrayList<TblTipoPeriodoAcademico>)consulta.list();
	}
}
