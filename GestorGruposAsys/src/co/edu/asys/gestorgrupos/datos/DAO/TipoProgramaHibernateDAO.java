package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPrograma;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class TipoProgramaHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblTipoPrograma tipoPrograma) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_CREACION_DATOS", tipoPrograma.getNvNombre());
			sesion.save(tipoPrograma);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_CREACION_DATOS", tipoPrograma.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PROGRAMA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblTipoPrograma consultarPorCodigo(TblTipoPrograma tipoPrograma) {
		TblTipoPrograma tipoProgramaConsultado = sesion.load(TblTipoPrograma.class, tipoPrograma.getInCodigo());
		return tipoProgramaConsultado;
		
	
	}
	
	public void actualizarConMerge(TblTipoPrograma tipoPrograma) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_ACTUALIZACION_DATOS", tipoPrograma.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoPrograma tipoProgramaActualizar = consultarPorCodigo(tipoPrograma);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			tipoProgramaActualizar.setNvNombre(tipoPrograma.getNvNombre());
			tipoProgramaActualizar.setTblInstitucion(tipoPrograma.getTblInstitucion());
			
			//Pendiente colocar campos tipoPrograma ======================================
		
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoProgramaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_ACTUALIZACION_DATOS", tipoPrograma.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PROGRAMA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}		
	}
	
	public void actualizarConUpdate(TblTipoPrograma tipoPrograma) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_ACTUALIZACION_DATOS", tipoPrograma.getNvNombre());
			//Pendiente colocar tipoPrograma creaci�n
			
			sesion.update(tipoPrograma);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_ACTUALIZACION_DATOS", tipoPrograma.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PROGRAMA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblTipoPrograma tipoPrograma) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_ELIMINACION_DATOS", tipoPrograma.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoPrograma tipoProgramaInhabilitar = consultarPorCodigo(tipoPrograma);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar tipoPrograma eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoProgramaInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_ELIMINACION_DATOS", tipoPrograma.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_PROGRAMA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblTipoPrograma> consultarPorFiltro(TblTipoPrograma tipoPrograma)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblTipoPrograma.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (tipoPrograma != null){
			
			//Validamos si consulta por el c�digo
			if (tipoPrograma.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("inCodigo", tipoPrograma.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (tipoPrograma.getNvNombre() != null && !tipoPrograma.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", tipoPrograma.getNvNombre().trim()));
			}
			
		}
		
		return (ArrayList<TblTipoPrograma>)consulta.list();
	}
}
