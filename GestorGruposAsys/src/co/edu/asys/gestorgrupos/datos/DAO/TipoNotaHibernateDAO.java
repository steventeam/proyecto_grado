package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoNota;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class TipoNotaHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblTipoNota tipoNota) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_CREACION_DATOS", tipoNota.getNvNombre());
			sesion.save(tipoNota);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_CREACION_DATOS", tipoNota.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_NOTA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblTipoNota consultarPorCodigo(TblTipoNota tipoNota) {
		TblTipoNota tipoNotaConsultado = sesion.load(TblTipoNota.class, tipoNota.getInCodigo());
		return tipoNotaConsultado;
		
	
	}
	
	public void actualizarConMerge(TblTipoNota tipoNota) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_ACTUALIZACION_DATOS", tipoNota.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoNota tipoNotaActualizar = consultarPorCodigo(tipoNota);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			tipoNotaActualizar.setNvNombre(tipoNota.getNvNombre());
			
			//Pendiente colocar campos tipoNota ======================================
		
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoNotaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_ACTUALIZACION_DATOS", tipoNota.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_NOTA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblTipoNota tipoNota) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_ACTUALIZACION_DATOS", tipoNota.getNvNombre());
			//Pendiente colocar tipoNota creaci�n
			
			sesion.update(tipoNota);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_ACTUALIZACION_DATOS", tipoNota.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_NOTA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblTipoNota tipoNota) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_ELIMINACION_DATOS", tipoNota.getNvNombre());

			//1. Consultamos el objeto persistente
			TblTipoNota tipoNotaInhabilitar = consultarPorCodigo(tipoNota);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar tipoNota eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoNotaInhabilitar);		
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_ELIMINACION_DATOS", tipoNota.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_NOTA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblTipoNota> consultarPorFiltro(TblTipoNota tipoNota)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblTipoNota.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (tipoNota != null){
			
			//Validamos si consulta por el c�digo
			if (tipoNota.getInCodigo() != null && tipoNota.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", tipoNota.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (tipoNota.getNvNombre() != null && !tipoNota.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", tipoNota.getNvNombre().trim()));
			}
			
		}
		
		return (ArrayList<TblTipoNota>)consulta.list();
	}
}
