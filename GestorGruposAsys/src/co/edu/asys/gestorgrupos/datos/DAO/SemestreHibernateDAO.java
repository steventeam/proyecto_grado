package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblSemestre;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class SemestreHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblSemestre semestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_CREACION_DATOS", semestre.getNvNombre());
			sesion.save(semestre);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_CREACION_DATOS", semestre.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SEMESTRE_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblSemestre consultarPorCodigo(TblSemestre semestre) {
		TblSemestre semestreConsultado = sesion.load(TblSemestre.class, semestre.getInCodigo());
		return semestreConsultado;
		
	
	}
	
	public void actualizarConMerge(TblSemestre semestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_ACTUALIZACION_DATOS", semestre.getNvNombre());
			
			//1. Consultamos el objeto persistente
			TblSemestre semestreActualizar = consultarPorCodigo(semestre);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			semestreActualizar.setNvNombre(semestre.getNvNombre());
			semestreActualizar.setInA�o(semestre.getInA�o());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(semestreActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_ACTUALIZACION_DATOS", semestre.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SEMESTRE_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public void actualizarConUpdate(TblSemestre semestre) throws Exception {
		
		//Pendiente colocar semestre creaci�n
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_ACTUALIZACION_DATOS", semestre.getNvNombre());
			sesion.update(semestre);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_ACTUALIZACION_DATOS", semestre.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SEMESTRE_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblSemestre semestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_ELIMINACION_DATOS", semestre.getNvNombre());

			//1. Consultamos el objeto persistente
			TblSemestre semestreInhabilitar = consultarPorCodigo(semestre);
			
			//2. Hacemos un merge de la informaci�n
			sesion.merge(semestreInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_ELIMINACION_DATOS", semestre.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SEMESTRE_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblSemestre> consultarPorFiltro(TblSemestre semestre)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblSemestre.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (semestre != null){
			
			//Validamos si consulta por el c�digo
			if (semestre.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("inCodigo", semestre.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (semestre.getNvNombre() != null && !semestre.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", semestre.getNvNombre().trim()));
			}
			 
			//Validamos si consulta por el a�o
			 if (semestre.getInA�o() > 0) {
				consulta.add(Restrictions.eq("inA�o", semestre.getInA�o()));
			}
			 
						
		}
		
		return (ArrayList<TblSemestre>)consulta.list();
	}
}
