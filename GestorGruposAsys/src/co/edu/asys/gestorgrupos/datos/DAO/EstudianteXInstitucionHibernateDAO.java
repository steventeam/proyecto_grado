package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXInstitucion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class EstudianteXInstitucionHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		
		
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_CREACION_DATOS");
			
			sesion.save(estudianteXInstitucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_CREACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_CREACION");

			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
		
	}
	
	public TblEstudianteXInstitucion consultarPorCodigo(TblEstudianteXInstitucion estudianteXInstitucion) {
		TblEstudianteXInstitucion estudianteXInstitucionConsultado = sesion.load(TblEstudianteXInstitucion.class, estudianteXInstitucion.getInCodigo());
		return estudianteXInstitucionConsultado;
		
	
	}
	
	public void actualizarConMerge(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblEstudianteXInstitucion estudianteXInstitucionActualizar = consultarPorCodigo(estudianteXInstitucion);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			estudianteXInstitucionActualizar.setTblInstitucion(estudianteXInstitucion.getTblInstitucion());
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(estudianteXInstitucionActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_ACTUALIZACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_ACTUALIZACION");

			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	public void actualizarConUpdate(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_ACTUALIZACION_DATOS");
			sesion.update(estudianteXInstitucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_ACTUALIZACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_ACTUALIZACION");

			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	public void inhabilitarConMerge(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblEstudianteXInstitucion estudianteXInstitucionInhabilitar = consultarPorCodigo(estudianteXInstitucion);
			
			sesion.merge(estudianteXInstitucionInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_ELIMINACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_ELIMINACION");

			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblEstudianteXInstitucion> consultarPorFiltro(TblEstudianteXInstitucion estudianteXInstitucion)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblEstudianteXInstitucion.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (estudianteXInstitucion != null){
			
			//Validamos si consulta por el c�digo
//			if (estudianteXInstitucion.getSegUsuario().getInCodigo() != null && estudianteXInstitucion.getSegUsuario().getInCodigo() != 0) {
//				
//				consulta.add(Restrictions.eq("inCodigo", estudianteXInstitucion.getInCodigo()));
//				
//			}
			
			if (estudianteXInstitucion.getSegUsuario() != null && estudianteXInstitucion.getSegUsuario().getInCodigo() != 0){
				
				consulta.add(Restrictions.eq("segUsuario", estudianteXInstitucion.getSegUsuario()));
			}
			
		}
		
		return (ArrayList<TblEstudianteXInstitucion>)consulta.list();
	}
}
