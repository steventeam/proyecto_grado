package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateriaXPensum;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class MateriaXPensumHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblMateriaXPensum materiaXPensum) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_CREACION_DATOS");
			sesion.save(materiaXPensum);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CREACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_X_PENSUM_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblMateriaXPensum consultarPorCodigo(TblMateriaXPensum materiaXPensum) {
		TblMateriaXPensum materiaXPensumConsultado = sesion.load(TblMateriaXPensum.class, materiaXPensum.getInCodigo());
		return materiaXPensumConsultado;
		
	
	}
	
	public void actualizarConMerge(TblMateriaXPensum materiaXPensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblMateriaXPensum materiaXPensumActualizar = consultarPorCodigo(materiaXPensum);
			
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			materiaXPensumActualizar.setInNumeroCreditos(materiaXPensum.getInNumeroCreditos());

			
			//Pendiente colocar campos materiaXPensum ======================================
		
			
			//3. Hacemos un merge de la información
			sesion.merge(materiaXPensumActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ACTUALIZACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblMateriaXPensum materiaXPensum) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ACTUALIZACION_DATOS");
			sesion.update(materiaXPensum);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ACTUALIZACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblMateriaXPensum materiaXPensum) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblMateriaXPensum materiaXPensumInhabilitar = consultarPorCodigo(materiaXPensum);
			
			sesion.merge(materiaXPensumInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ELIMINACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_X_PENSUM_ELIMINACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblMateriaXPensum> consultarPorFiltro(TblMateriaXPensum materiaXPensum)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblMateriaXPensum.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (materiaXPensum != null){
			
			//Validamos si consulta por la materia
			if (materiaXPensum.getTblMateria() != null && materiaXPensum.getTblMateria().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblMateria", materiaXPensum.getTblMateria()));
				
			}
						
			//Validamos si consulta por la materia
			if (materiaXPensum.getTblPensumXSemestre() != null && materiaXPensum.getTblPensumXSemestre().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblPensumXSemestre", materiaXPensum.getTblPensumXSemestre()));
				
			}
			
			//Validamos si consulta por la materia
			if (materiaXPensum.getInNumeroCreditos() != 0) {
				
				consulta.add(Restrictions.eq("inNumeroCreditos", materiaXPensum.getInNumeroCreditos()));
				
			}
			
		}
		
		return (ArrayList<TblMateriaXPensum>)consulta.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblMateriaXPensum> consultarPorGrupo(TblGrupo grupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblMateriaXPensum.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (grupo != null){
			
			//Validamos si consulta por la materia
			if (grupo.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("tblGrupo.inCodigo", grupo.getInCodigo()));
				
			}
						
		}
		
		return (ArrayList<TblMateriaXPensum>)consulta.list();
	}
}
