package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblInstitucion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class InstitucionHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblInstitucion institucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_CREACION_DATOS", institucion.getNvNombre());
			sesion.save(institucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_CREACION_DATOS", institucion.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_INSTITUCION_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblInstitucion consultarPorCodigo(TblInstitucion institucion) {
		TblInstitucion institucionConsultado = sesion.load(TblInstitucion.class, institucion.getInCodigo());
		return institucionConsultado;
		
	
	}
	
	public void actualizarConMerge(TblInstitucion institucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_ACTUALIZACION_DATOS", institucion.getNvNombre());
			//1. Consultamos el objeto persistente
			TblInstitucion institucionActualizar = consultarPorCodigo(institucion);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			institucionActualizar.setNvNombre(institucion.getNvNombre());
			
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(institucionActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_ACTUALIZACION_DATOS", institucion.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_INSTITUCION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblInstitucion institucion) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_ACTUALIZACION_DATOS", institucion.getNvNombre());
			sesion.update(institucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_ACTUALIZACION_DATOS", institucion.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_INSTITUCION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblInstitucion institucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_ELIMINACION_DATOS", institucion.getNvNombre());

			//1. Consultamos el objeto persistente
			TblInstitucion institucionInhabilitar = consultarPorCodigo(institucion);
			
			sesion.merge(institucionInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_ELIMINACION_DATOS", institucion.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_INSTITUCION_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblInstitucion> consultarPorFiltro(TblInstitucion institucion)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblInstitucion.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (institucion != null){
			
			//Validamos si consulta por el c�digo
			if (institucion.getInCodigo() != null && institucion.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", institucion.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (institucion.getNvNombre() != null && !institucion.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", institucion.getNvNombre().trim()));
			}
			
			
		}
		
		return (ArrayList<TblInstitucion>)consulta.list();
	}
}
