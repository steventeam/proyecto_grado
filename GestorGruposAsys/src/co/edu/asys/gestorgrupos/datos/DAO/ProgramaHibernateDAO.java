package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class ProgramaHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblPrograma programa) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_CREACION_DATOS", programa.getNvNombre());
			sesion.save(programa);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CREACION_DATOS", programa.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROGRAMA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblPrograma consultarPorCodigo(TblPrograma programa) {
		TblPrograma programaConsultado = sesion.load(TblPrograma.class, programa.getInCodigo());
		return programaConsultado;
		
	
	}
	
	public void actualizarConMerge(TblPrograma programa) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ACTUALIZACION_DATOS", programa.getNvNombre());

			//1. Consultamos el objeto persistente
			TblPrograma programaActualizar = consultarPorCodigo(programa);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			programaActualizar.setNvNombre(programa.getNvNombre());
			programaActualizar.setTblTipoPrograma(programa.getTblTipoPrograma());		
			

			//Pendiente colocar campos programa ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(programaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ACTUALIZACION_DATOS", programa.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROGRAMA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblPrograma programa) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ACTUALIZACION_DATOS", programa.getNvNombre());
			//Pendiente colocar programa creaci�n
			
			sesion.update(programa);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ACTUALIZACION_DATOS", programa.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROGRAMA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblPrograma programa) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ELIMINACION_DATOS", programa.getNvNombre());

			//1. Consultamos el objeto persistente
			TblPrograma programaInhabilitar = consultarPorCodigo(programa);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar programa eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(programaInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ELIMINACION_DATOS", programa.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROGRAMA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblPrograma> consultarPorFiltro(TblPrograma programa)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblPrograma.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (programa != null){
			
			//Validamos si consulta por el c�digo
			if (programa.getInCodigo() != null && programa.getInCodigo() !=0) {
				
				consulta.add(Restrictions.eq("inCodigo", programa.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (programa.getNvNombre() != null && !programa.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", programa.getNvNombre().trim()));
			}
			
		}
		
		return (ArrayList<TblPrograma>)consulta.list();
	}
}
