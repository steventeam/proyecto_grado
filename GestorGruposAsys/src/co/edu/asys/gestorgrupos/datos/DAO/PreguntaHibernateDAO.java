package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPregunta;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("preguntaDAO")
@Scope("prototype")
public class PreguntaHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegPregunta pregunta) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_CREACION_DATOS", pregunta.getNvDescripcion());

			sesion.save(pregunta);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_CREACION_DATOS", pregunta.getNvDescripcion());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegPregunta consultarPorCodigo(SegPregunta pregunta) {
		SegPregunta preguntaConsultado = sesion.load(SegPregunta.class, pregunta.getInCodigo());
		return preguntaConsultado;
	
	}
	
	public void actualizarConMerge(SegPregunta pregunta) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_ACTUALIZACION_DATOS", pregunta.getNvDescripcion());

			//1. Consultamos el objeto persistente
			SegPregunta preguntaActualizar = consultarPorCodigo(pregunta);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			preguntaActualizar.setNvDescripcion(pregunta.getNvDescripcion());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(preguntaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_ACTUALIZACION_DATOS", pregunta.getNvDescripcion());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(SegPregunta pregunta) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_ACTUALIZACION_DATOS", pregunta.getNvDescripcion());
			
			sesion.update(pregunta);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_ACTUALIZACION_DATOS", pregunta.getNvDescripcion());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(SegPregunta pregunta) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_ELIMINACION_DATOS", pregunta.getNvDescripcion());

			//1. Consultamos el objeto persistente
			SegPregunta preguntaInhabilitar = consultarPorCodigo(pregunta);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			preguntaInhabilitar.setDtFechaEliminacion(new Date());
			//Pendiente colocar usuario eliminaci�n ===================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(preguntaInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_ELIMINACION_DATOS", pregunta.getNvDescripcion());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegPregunta> consultarPorFiltro(SegPregunta pregunta)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegPregunta.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (pregunta != null){
			
			//Validamos si consulta por el c�digo
			if (pregunta.getInCodigo() != null && pregunta.getInCodigo() !=0) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", pregunta.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", pregunta.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (pregunta.getNvDescripcion() != null && !pregunta.getNvDescripcion().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", pregunta.getNvNombre()));
				consulta.add(Restrictions.eq("nvDescripcion", pregunta.getNvDescripcion().trim()));
			}
		}
		
		return (ArrayList<SegPregunta>)consulta.list();
	}


	
}
