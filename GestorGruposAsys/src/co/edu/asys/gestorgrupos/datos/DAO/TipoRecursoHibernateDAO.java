package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoRecurso;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("tipoRecursoHibernateDAO")
@Scope("prototype")
public class TipoRecursoHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegTipoRecurso tipoRecurso) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_CREACION_DATOS", tipoRecurso.getNvNombre());
			
			sesion.save(tipoRecurso);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_CREACION_DATOS", tipoRecurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_RECURSO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegTipoRecurso consultarPorCodigo(SegTipoRecurso tipoRecurso) {
		SegTipoRecurso tipoRecursoConsultado = sesion.load(SegTipoRecurso.class, tipoRecurso.getInCodigo());
		return tipoRecursoConsultado;
	
	}
	
	public void actualizarConMerge(SegTipoRecurso tipoRecurso) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_ACTUALIZACION_DATOS", tipoRecurso.getNvNombre());

			//1. Consultamos el objeto persistente
			SegTipoRecurso tipoRecursoActualizar = consultarPorCodigo(tipoRecurso);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			tipoRecursoActualizar.setNvNombre(tipoRecurso.getNvNombre());
			
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoRecursoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_ACTUALIZACION_DATOS", tipoRecurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_RECURSO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(SegTipoRecurso tipoRecurso) throws Exception {

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_ACTUALIZACION_DATOS", tipoRecurso.getNvNombre());
			
			sesion.update(tipoRecurso);

			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_ACTUALIZACION_DATOS", tipoRecurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_RECURSO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(SegTipoRecurso tipoRecurso) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_ELIMINACION_DATOS", tipoRecurso.getNvNombre());

			//1. Consultamos el objeto persistente
			SegTipoRecurso tipoRecursoInhabilitar = consultarPorCodigo(tipoRecurso);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			tipoRecursoInhabilitar.setDtFechaEliminacion(new Date());
			//Pendiente colocar usuario eliminaci�n ===================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoRecursoInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_ELIMINACION_DATOS", tipoRecurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_RECURSO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	/*@SuppressWarnings("unchecked")
	public ArrayList<SegTipoRecurso> consultarTodo() throws Exception {
		ArrayList<SegTipoRecurso> listaTotal =
				(ArrayList<SegTipoRecurso>)
				sesion.createQuery("from SegTipoRecurso").list();
		
		return listaTotal;
	} 
	*/
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegTipoRecurso> consultarPorFiltro(SegTipoRecurso tipoRecurso)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegTipoRecurso.class);
		//String consulta = "from SegTipoRecurso ";
			//	
		if (tipoRecurso != null){
			
			//Validamos si consulta por el c�digo
			if (tipoRecurso.getInCodigo() != null) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", tipoRecurso.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", tipoRecurso.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (tipoRecurso.getNvNombre() != null && !tipoRecurso.getNvNombre().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", tipoRecurso.getNvNombre()));
				consulta.add(Restrictions.eq("nvNombre", tipoRecurso.getNvNombre().trim()));
			}
		}
		
		return (ArrayList<SegTipoRecurso>)consulta.list();
	}

	
}
