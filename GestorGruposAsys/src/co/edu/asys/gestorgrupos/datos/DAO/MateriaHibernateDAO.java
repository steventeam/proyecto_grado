package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateria;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class MateriaHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblMateria materia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_CREACION_DATOS", materia.getNvNombre());
			sesion.save(materia);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_CREACION_DATOS", materia.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblMateria consultarPorCodigo(TblMateria materia) {
		TblMateria materiaConsultado = sesion.load(TblMateria.class, materia.getInCodigo());
		return materiaConsultado;
		
	
	}
	
	public void actualizarConMerge(TblMateria materia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_ACTUALIZACION_DATOS", materia.getNvNombre());

			//1. Consultamos el objeto persistente
			TblMateria materiaActualizar = consultarPorCodigo(materia);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			materiaActualizar.setNvNombre(materia.getNvNombre());
			
			//Pendiente colocar campos materia ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(materiaActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_ACTUALIZACION_DATOS", materia.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}		
	}
	
	public void actualizarConUpdate(TblMateria materia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_ACTUALIZACION_DATOS", materia.getNvNombre());

			sesion.update(materia);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_ACTUALIZACION_DATOS", materia.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblMateria materia) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_ELIMINACION_DATOS", materia.getNvNombre());

			//1. Consultamos el objeto persistente
			TblMateria materiaInhabilitar = consultarPorCodigo(materia);

			sesion.merge(materiaInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_ELIMINACION_DATOS", materia.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_MATERIA_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblMateria> consultarPorFiltro(TblMateria materia)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblMateria.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (materia != null){
			
			//Validamos si consulta por el c�digo
			if (materia.getInCodigo() != null && materia.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", materia.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			 if (materia.getNvNombre() != null && !materia.getNvNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombre", materia.getNvNombre().trim()));
			}
			
		}
		
		return (ArrayList<TblMateria>)consulta.list();
	}
}
