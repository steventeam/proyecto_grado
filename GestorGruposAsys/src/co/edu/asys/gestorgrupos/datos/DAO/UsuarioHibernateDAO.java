package co.edu.asys.gestorgrupos.datos.DAO;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;


import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class UsuarioHibernateDAO {

	private Session sesion;

	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}

	public void crear(SegUsuario usuario) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_CREACION_DATOS", usuario.getNvNombreUsuario());
			
			sesion.save(usuario);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CREACION_DATOS", usuario.getNvNombreUsuario());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_USUARIO_CREACION");
		
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}

	}

	public SegUsuario consultarPorNombreUsuario(SegUsuario usuario) {
		SegUsuario usuarioConsultado = sesion.load(SegUsuario.class, usuario.getNvNombreUsuario());
		return usuarioConsultado;
	}
		
	public SegUsuario consultarPorCodigo(SegUsuario usuario) {
		SegUsuario usuarioConsultado = sesion.load(SegUsuario.class, usuario.getInCodigo());
		return usuarioConsultado;
	}

	public void actualizarConMerge(SegUsuario usuario) throws Exception {

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ACTUALIZACION_DATOS", usuario.getNvNombreUsuario());
			
			// 1. Consultamos el objeto persistente
			SegUsuario usuarioActualizar = consultarPorCodigo(usuario);

			// 2. Vaciamos al objeto persistente la informaci�n que vamos a
			// actualizar

			usuarioActualizar.setSegTipoIdentificacion(usuario.getSegTipoIdentificacion());
			usuarioActualizar.setNvNumeroIdentificacion(usuario.getNvNumeroIdentificacion());
			usuarioActualizar.setNvPrimerNombre(usuario.getNvPrimerNombre());
			usuarioActualizar.setNvSegundoNombre(usuario.getNvSegundoNombre());
			usuarioActualizar.setNvPrimerApellido(usuario.getNvPrimerApellido());
			usuarioActualizar.setNvSegundoApellido(usuario.getNvSegundoApellido());
			usuarioActualizar.setNvCorreoElectronico(usuario.getNvCorreoElectronico());
			usuarioActualizar.setNvTelefonoMovil(usuario.getNvTelefonoMovil());
			usuarioActualizar.setNvTelefonoFijo(usuario.getNvTelefonoFijo());
			usuarioActualizar.setNvNombreUsuario(usuario.getNvNombreUsuario());
			usuarioActualizar.setNvClave(usuario.getNvClave());
		
			// Hacemos un merge de la informaci�n
			sesion.merge(usuarioActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ACTUALIZACION_DATOS", usuario.getNvNombreUsuario());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_USUARIO_ACTUALIZACION");
		
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}

	public void actualizarConUpdate(SegUsuario usuario) throws Exception {
		
		// Pendiente colocar usuario creaci�n

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ACTUALIZACION_DATOS", usuario.getNvNombreUsuario());
			
			sesion.update(usuario);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ACTUALIZACION_DATOS", usuario.getNvNombreUsuario());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_USUARIO_ACTUALIZACION");
		
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}

	}
	
	//////
	public void actualizar2(SegUsuario usuario) throws Exception {

		java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd_tesis", "root", "1234"); 
		java.sql.PreparedStatement sentencia =conexion.prepareStatement("UPDATE seg_usuario SET NV_CLAVE =? WHERE NV_NOMBRE_USUARIO= ?");
		sentencia.setString(1, usuario.getNvClave());
		sentencia.setString(2, usuario.getNvNombreUsuario());
		@SuppressWarnings("unused")
		int n = sentencia.executeUpdate();
		

	}	
	
	public void habilitar(SegUsuario usuario) throws Exception {

		java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd_tesis", "root", "1234"); 
		java.sql.PreparedStatement sentencia =conexion.prepareStatement("UPDATE seg_usuario SET DT_FECHA_ELIMINACION=NULL WHERE IN_CODIGO= ?");
		sentencia.setInt(1, usuario.getInCodigo());
		@SuppressWarnings("unused")
		int n = sentencia.executeUpdate();

	}
	
	public void enviarClave(SegUsuario usuario) throws Exception{
		
		java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd_tesis", "root", "1234");
		java.sql.PreparedStatement sentencia = conexion.prepareStatement("SELECT NV_CLAVE from seg_usuario WHERE NV_CORREO_ELECTRONICO = ?");
		sentencia.setString(1, usuario.getNvCorreoElectronico());
		@SuppressWarnings("unused")
		int n = sentencia.executeUpdate();
	}

	public void inhabilitarConMerge(SegUsuario usuario) throws Exception {

		// Pendiente colocar usuario eliminaci�n

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ELIMINACION_DATOS", usuario.getNvNombreUsuario());
			// 1. Consultamos el objeto persistente
			SegUsuario usuarioInhabilitar = consultarPorCodigo(usuario);

			// 2. Vaciamos al objeto persistente la informaci�n que vamos a
			// actualizar
			usuarioInhabilitar.setDtFechaEliminacion(new Date());
			
			//Pendiente colocar usuario creaci�n
			// 3. Hacemos un merge de la informaci�n
			sesion.merge(usuarioInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ELIMINACION_DATOS", usuario.getNvNombreUsuario());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_USUARIO_ELIMINACION");
		
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}

	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegUsuario> consultarPorFiltro(SegUsuario usuario) throws Exception {
		Criteria consulta = sesion.createCriteria(SegUsuario.class);
		//
		if (usuario != null) {

			// Validamos si consulta por el c�digo
			if (usuario.getInCodigo() != null && usuario.getInCodigo() != 0) {
				consulta.add(Restrictions.eq("inCodigo", usuario.getInCodigo()));

			}
//			
			// Validamos si consulta por el tipo de identificaci�n
//			if (usuario.getSegTipoIdentificacion() != null && usuario.getSegTipoIdentificacion().getInCodigo() != 0) {
//				consulta.add(Restrictions.eqOrIsNull("segTipoIdentificacion", usuario.getSegTipoIdentificacion()));
//			}
//			
			// Validamos si consulta por el tipo de identificaci�n
//			if (usuario.getSegPerfil() != null && usuario.getSegPerfil().getInCodigo() != 0) {
//				consulta.add(Restrictions.eqOrIsNull("segPerfil", usuario.getSegPerfil()));
//			}
//			
			// Validamos si consulta por el tipo de identificaci�n
//			if (usuario.getTblGenero() != null && usuario.getTblGenero().getInCodigo() != 0) {
//				consulta.add(Restrictions.eqOrIsNull("tblGenero", usuario.getTblGenero()));
//			}

			// Validamos si consulta por el n�mero de identificaci�n
			if (usuario.getNvNumeroIdentificacion() != null && !usuario.getNvNumeroIdentificacion().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNumeroIdentificacion", usuario.getNvNumeroIdentificacion().trim()));
			}

			// Validamos si consulta por el primer nombre
			if (usuario.getNvPrimerNombre() != null && !usuario.getNvPrimerNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvPrimerNombre", usuario.getNvPrimerNombre().trim()));
			}

			// Validamos si consulta por el segundo nombre
			if (usuario.getNvSegundoNombre() != null && !usuario.getNvSegundoNombre().trim().equals("")) {
				consulta.add(Restrictions.eq("nvSegundoNombre", usuario.getNvSegundoNombre().trim()));
			}

			// Validamos si consulta por el primer apellido
			if (usuario.getNvPrimerApellido() != null && !usuario.getNvPrimerApellido().trim().equals("")) {
				consulta.add(Restrictions.eq("nvPrimerApellido", usuario.getNvPrimerApellido().trim()));
			}

			// Validamos si consulta por el segundo apellido
			if (usuario.getNvSegundoApellido() != null && !usuario.getNvSegundoApellido().trim().equals("")) {
				consulta.add(Restrictions.eq("nvSegundoApellido", usuario.getNvSegundoApellido().trim()));
			}

			// Validamos si consulta por el correo
			if (usuario.getNvCorreoElectronico() != null && !usuario.getNvCorreoElectronico().trim().equals("")) {
				consulta.add(Restrictions.eq("nvCorreoElectronico", usuario.getNvCorreoElectronico()));
			}

			// Validamos si consulta por el celular
			if (usuario.getNvTelefonoMovil() != null && !usuario.getNvTelefonoMovil().trim().equals("")) {
				consulta.add(Restrictions.eq("nvTelefonoMovil", usuario.getNvTelefonoMovil()));
			}

			// Validamos si consulta por el n�mero de tel�fono
			if (usuario.getNvTelefonoFijo() != null && !usuario.getNvTelefonoFijo().trim().equals("")) {// Para
																										// validar
				consulta.add(Restrictions.eq("nvTelefonoFijo", usuario.getNvTelefonoFijo()));
			}

			// Validamos si consulta por el nombre de usuario
			if (usuario.getNvNombreUsuario() != null && !usuario.getNvNombreUsuario().trim().equals("")) {
				consulta.add(Restrictions.eq("nvNombreUsuario", usuario.getNvNombreUsuario().trim()));
			}

			// validamos si vamos a consultar por contrase�a
			if (usuario.getNvClave() != null && !usuario.getNvClave().trim().equals("")) {
				
				String encript = DigestUtils.sha1Hex(usuario.getNvClave());
				usuario.setNvClave(encript);
				consulta.add(Restrictions.eq("nvClave", usuario.getNvClave()));
				
			}
					
			if (usuario.getDtFechaEliminacion() != null) {
				consulta.add(Restrictions.eq("dtFechaEliminacion", usuario.getDtFechaEliminacion()));
			}
		}

		return (ArrayList<SegUsuario>) consulta.list();
	}	
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegUsuario> consultarPorNumeroId(SegUsuario usuario) throws Exception {
		Criteria consulta = sesion.createCriteria(SegUsuario.class);
		// String consulta = "from SegTipoIdentificacion ";
		//
		if (usuario != null) {

			// Validamos si consulta por el n�mero de identificaci�n
			if (usuario.getNvNumeroIdentificacion() != null && !usuario.getNvNumeroIdentificacion().trim().equals("")) {
				consulta.add(Restrictions.like("nvNumeroIdentificacion", usuario.getNvNumeroIdentificacion().trim()));
			}

			// validamos si vamos a consultar por contrase�a
			if (usuario.getNvClave() != null && !usuario.getNvClave().trim().equals("")) {
				
				String encript = DigestUtils.sha1Hex(usuario.getNvClave());
				usuario.setNvClave(encript);
				consulta.add(Restrictions.like("nvClave", usuario.getNvClave()));
				
			}
			
		}

		return (ArrayList<SegUsuario>) consulta.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegUsuario> consultarPorCorreo(SegUsuario usuario) throws Exception {
		Criteria consulta = sesion.createCriteria(SegUsuario.class);
		// String consulta = "from SegTipoIdentificacion ";
		//
		if (usuario != null) {

			// Validamos si consulta por el correo electr�nico
			if (usuario.getNvCorreoElectronico() != null && !usuario.getNvCorreoElectronico().trim().equals("")) {
				consulta.add(Restrictions.eq("nvCorreoElectronico", usuario.getNvCorreoElectronico().trim()));
			}
								
			
		}

		return (ArrayList<SegUsuario>) consulta.list();
	}
}
