package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPeriodoAcademico;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class PeriodoAcademicoHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblPeriodoAcademico periodoAcademico) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_CREACION_DATOS", periodoAcademico.getInCodigo());
			sesion.save(periodoAcademico);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CREACION_DATOS", periodoAcademico.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERIODO_ACADEMICO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblPeriodoAcademico consultarPorCodigo(TblPeriodoAcademico periodoAcademico) {
		TblPeriodoAcademico periodoAcademicoConsultado = sesion.load(TblPeriodoAcademico.class, periodoAcademico.getInCodigo());
		return periodoAcademicoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblPeriodoAcademico periodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_DATOS", periodoAcademico.getInCodigo());
			//1. Consultamos el objeto persistente
			TblPeriodoAcademico periodoAcademicoActualizar = consultarPorCodigo(periodoAcademico);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			periodoAcademicoActualizar.setDaFechaInicio(periodoAcademico.getDaFechaInicio());
			periodoAcademicoActualizar.setDaFechaFin(periodoAcademico.getDaFechaFin());
			periodoAcademicoActualizar.setTblTipoPeriodoAcademico(periodoAcademico.getTblTipoPeriodoAcademico());
			

			//Pendiente colocar campos periodoAcademico ======================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(periodoAcademicoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_DATOS", periodoAcademico.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblPeriodoAcademico periodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_DATOS", periodoAcademico.getInCodigo());

			sesion.update(periodoAcademico);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_DATOS", periodoAcademico.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblPeriodoAcademico periodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ELIMINACION_DATOS", periodoAcademico.getInCodigo());

			TblPeriodoAcademico periodoAcademicoInhabilitar = consultarPorCodigo(periodoAcademico);
			
			sesion.merge(periodoAcademicoInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ELIMINACION_DATOS", periodoAcademico.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERIODO_ACADEMICO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblPeriodoAcademico> consultarPorFiltro(TblPeriodoAcademico periodoAcademico)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblPeriodoAcademico.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (periodoAcademico != null){
			
			//Validamos si consulta por el c�digo
			if (periodoAcademico.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("inCodigo", periodoAcademico.getInCodigo()));
				
			}
			
		}
		
		return (ArrayList<TblPeriodoAcademico>)consulta.list();
	}
}
