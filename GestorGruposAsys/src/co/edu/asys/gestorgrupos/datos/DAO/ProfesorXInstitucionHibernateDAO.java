package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblProfesorXInstitucion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class ProfesorXInstitucionHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_CREACION_DATOS");
			
			sesion.save(profesorXInstitucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CREACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROFESOR_X_INSTITUCION_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblProfesorXInstitucion consultarPorCodigo(TblProfesorXInstitucion profesorXInstitucion) {
		TblProfesorXInstitucion profesorXInstitucionConsultado = sesion.load(TblProfesorXInstitucion.class, profesorXInstitucion.getInCodigo());
		return profesorXInstitucionConsultado;
		
	
	}
	
	public void actualizarConMerge(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ACTUALIZACION_DATOS");
			//1. Consultamos el objeto persistente
			TblProfesorXInstitucion profesorXInstitucionActualizar = consultarPorCodigo(profesorXInstitucion);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			profesorXInstitucionActualizar.setTblInstitucion(profesorXInstitucion.getTblInstitucion());
			profesorXInstitucionActualizar.setSegUsuario(new SegUsuario());
			
			//Pendiente colocar campos profesorXInstitucion ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(profesorXInstitucionActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ACTUALIZACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ACTUALIZACION_DATOS");
			//Pendiente colocar profesorXInstitucion creaci�n
			
			sesion.update(profesorXInstitucion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ACTUALIZACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ELIMINACION_DATOS");
			

			//1. Consultamos el objeto persistente
			TblProfesorXInstitucion profesorXInstitucionInhabilitar = consultarPorCodigo(profesorXInstitucion);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar profesorXInstitucion eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(profesorXInstitucionInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ELIMINACION_DATOS");
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblProfesorXInstitucion> consultarPorFiltro(TblProfesorXInstitucion profesorXInstitucion)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblProfesorXInstitucion.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (profesorXInstitucion != null){
			
			//Validamos si consulta por el c�digo
			if (profesorXInstitucion.getInCodigo() != null && profesorXInstitucion.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", profesorXInstitucion.getInCodigo()));
				
			}
						
		}
		
		return (ArrayList<TblProfesorXInstitucion>)consulta.list();
	}
}
