package co.edu.asys.gestorgrupos.datos.DAO;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecurso;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("recursoDAO")
@Scope("prototype")
public class RecursoHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegRecurso recurso) throws Exception {

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_CREACION_DATOS", recurso.getNvNombre());

			sesion.save(recurso);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_CREACION_DATOS", recurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegRecurso consultarPorCodigo(SegRecurso recurso) {
		SegRecurso recursoConsultado = sesion.load(SegRecurso.class, recurso.getInCodigo());
		return recursoConsultado;
	
	}
	
	public void actualizarConMerge(SegRecurso recurso) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_ACTUALIZACION_DATOS", recurso.getNvNombre());

			//1. Consultamos el objeto persistente
			SegRecurso recursoActualizar = consultarPorCodigo(recurso);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			recursoActualizar.setNvNombre(recurso.getNvNombre());
			recursoActualizar.setNvUrl(recurso.getNvUrl());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(recursoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_ACTUALIZACION_DATOS", recurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public void actualizarConUpdate(SegRecurso recurso) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_ACTUALIZACION_DATOS", recurso.getNvNombre());
			
			sesion.update(recurso);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_ACTUALIZACION_DATOS", recurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public void habilitar(SegRecurso recurso) throws Exception {

		java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd_tesis", "root", "1234"); 
		java.sql.PreparedStatement sentencia =conexion.prepareStatement("UPDATE seg_recurso SET DT_FECHA_ELIMINACION=NULL WHERE IN_CODIGO= ?");
		sentencia.setInt(1, recurso.getInCodigo());
		@SuppressWarnings("unused")
		int n = sentencia.executeUpdate();

	}
	
	public void inhabilitarConMerge(SegRecurso recurso) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_ELIMINACION_DATOS", recurso.getNvNombre());
			
			//1. Consultamos el objeto persistente
			SegRecurso recursoInhabilitar = consultarPorCodigo(recurso);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			recursoInhabilitar.setDtFechaEliminacion(new Date());
			//Pendiente colocar usuario eliminaci�n ===================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(recursoInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_ELIMINACION_DATOS", recurso.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegRecurso> consultarPorFiltro(SegRecurso recurso)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegRecurso.class);
		//String consulta = "from SegRecurso ";
			//	
		if (recurso != null){
			
			//Validamos si consulta por el c�digo
			if (recurso.getInCodigo() != null && recurso.getInCodigo() !=0) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", recurso.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", recurso.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (recurso.getNvNombre() != null && !recurso.getNvNombre().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", recurso.getNvNombre()));
				consulta.add(Restrictions.eq("nvNombre", recurso.getNvNombre().trim()));
			}
			
			//Validamos si consulta por el nombre
			if (recurso.getNvUrl() != null && !recurso.getNvUrl().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", recurso.getNvNombre()));
				consulta.add(Restrictions.eq("nvUrl", recurso.getNvUrl().trim()));
			}
			
		}
		
		return (ArrayList<SegRecurso>)consulta.list();
	}

	
}
