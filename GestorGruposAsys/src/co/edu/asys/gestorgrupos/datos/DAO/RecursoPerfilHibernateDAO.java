package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecursoPerfil;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("recursoPerfilDAO")
@Scope("prototype")
public class RecursoPerfilHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegRecursoPerfil recursoPerfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_CREACION_DATOS", recursoPerfil.getInCodigo());

			sesion.save(recursoPerfil);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_CREACION_DATOS", recursoPerfil.getInCodigo());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_PERFIL_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegRecursoPerfil consultarPorCodigo(SegRecursoPerfil recursoPerfil) {
		SegRecursoPerfil recursoPerfilConsultado = sesion.load(SegRecursoPerfil.class, recursoPerfil.getInCodigo());
		return recursoPerfilConsultado;
	
	}
	
	public void actualizarConMerge(SegRecursoPerfil recursoPerfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_ACTUALIZACION_DATOS", recursoPerfil.getInCodigo());
			//1. Consultamos el objeto persistente
			SegRecursoPerfil recursoPerfilActualizar = consultarPorCodigo(recursoPerfil);
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			recursoPerfilActualizar.setSegPerfil(recursoPerfil.getSegPerfil());
			recursoPerfilActualizar.setSegRecurso(recursoPerfil.getSegRecurso());

			
			//3. Hacemos un merge de la información
			sesion.merge(recursoPerfilActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_ACTUALIZACION_DATOS", recursoPerfil.getInCodigo());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_PERFIL_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public void actualizarConUpdate(SegRecursoPerfil recursoPerfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_ACTUALIZACION_DATOS", recursoPerfil.getInCodigo());
			
			sesion.update(recursoPerfil);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_ACTUALIZACION_DATOS", recursoPerfil.getInCodigo());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_PERFIL_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(SegRecursoPerfil recursoPerfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_ELIMINACION_DATOS", recursoPerfil.getInCodigo());
			
			//1. Consultamos el objeto persistente
			SegRecursoPerfil recursoPerfilInhabilitar = consultarPorCodigo(recursoPerfil);
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			recursoPerfilInhabilitar.setDtFechaEliminacion(new Date());
			
			//3. Hacemos un merge de la información
			sesion.merge(recursoPerfilInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_ELIMINACION_DATOS", recursoPerfil.getInCodigo());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_RECURSO_PERFIL_ELIMINACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegRecursoPerfil> consultarPorFiltro(SegRecursoPerfil recursoPerfil)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegRecursoPerfil.class);
		//String consulta = "from SegRecursoPerfil ";
			//	
		if (recursoPerfil != null){
			
			//Validamos si consulta por el perfil
			if (recursoPerfil.getSegPerfil() != null && recursoPerfil.getSegPerfil().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("segPerfil", recursoPerfil.getSegPerfil()));
			}

			//Validamos si consulta por el recurso
			if (recursoPerfil.getSegRecurso() != null && recursoPerfil.getSegRecurso().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("segRecurso", recursoPerfil.getSegRecurso()));
			}
		}
		
		return (ArrayList<SegRecursoPerfil>)consulta.list();
	}

	
}
