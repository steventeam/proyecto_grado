package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("grupoHibernateDAO")
@Scope("prototype")
public class GrupoHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblGrupo grupo) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_CREACION_DATOS", grupo.getInCodigo());
			sesion.save(grupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_CREACION_DATOS", grupo.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_GRUPO_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}

		
	}
	
	public TblGrupo consultarPorCodigo(TblGrupo grupo) {
		TblGrupo grupoConsultado = sesion.load(TblGrupo.class, grupo.getInCodigo());
		return grupoConsultado;
	
	}
	
	public void actualizarConMerge(TblGrupo grupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_ACTUALIZACION_DATOS", grupo.getInCodigo());

			//1. Consultamos el objeto persistente
			TblGrupo grupoActualizar = consultarPorCodigo(grupo);
			
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			grupoActualizar.setInCantidadMaxima(grupo.getInCantidadMaxima());
			grupoActualizar.setInCantidadMinima(grupo.getInCantidadMinima());
			grupoActualizar.setNvDia(grupo.getNvDia());
			grupoActualizar.setTblMateriaXPensum(grupo.getTblMateriaXPensum());
			grupoActualizar.setTblPeriodoAcademico(grupo.getTblPeriodoAcademico());
			grupoActualizar.setTblProfesorXInstitucion(grupo.getTblProfesorXInstitucion());
			grupoActualizar.setTiHoraFin(grupo.getTiHoraFin());
			grupoActualizar.setTiHoraInicio(grupo.getTiHoraInicio());
			
			//3. Hacemos un merge de la información
			sesion.merge(grupoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_ACTUALIZACION_DATOS", grupo.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblGrupo grupo) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_ACTUALIZACION_DATOS", grupo.getInCodigo());
			sesion.update(grupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_ACTUALIZACION_DATOS", grupo.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblGrupo grupo) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_ELIMINACION_DATOS", grupo.getInCodigo());
			sesion.save(grupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_ELIMINACION_DATOS", grupo.getInCodigo());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_GRUPO_ELIMINACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			//1. Consultamos el objeto persistente
			TblGrupo grupoInhabilitar = consultarPorCodigo(grupo);
			
			sesion.merge(grupoInhabilitar);
			
		}
	}
	
	/*@SuppressWarnings("unchecked")
	public ArrayList<TblGrupo> consultarTodo() throws Exception {
		ArrayList<TblGrupo> listaTotal =
				(ArrayList<TblGrupo>)
				sesion.createQuery("from TblGrupo").list();
		
		return listaTotal;
	} 
	*/
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblGrupo> consultarPorFiltro(TblGrupo grupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblGrupo.class);
		//String consulta = "from TblGrupo ";
			//	
		if (grupo != null){
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblMateriaXPensum() != null && grupo.getTblMateriaXPensum().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblMateriaXPensum", grupo.getTblMateriaXPensum()));
				
			}
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblPeriodoAcademico() != null && grupo.getTblPeriodoAcademico().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblPeriodoAcademico", grupo.getTblPeriodoAcademico()));
				
			}
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblProfesorXInstitucion() != null && grupo.getTblProfesorXInstitucion().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblProfesorXInstitucion", grupo.getTblProfesorXInstitucion()));
				
			}
			
					
		}
		
		return (ArrayList<TblGrupo>)consulta.list();
	}

	
	@SuppressWarnings("unchecked")
	public ArrayList<TblGrupo> consultarPorFiltroCrear(TblGrupo grupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblGrupo.class);
		//String consulta = "from TblGrupo ";
			//	
		if (grupo != null){
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblMateriaXPensum() != null && grupo.getTblMateriaXPensum().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblMateriaXPensum", grupo.getTblMateriaXPensum()));
				
			}
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblPeriodoAcademico() != null && grupo.getTblPeriodoAcademico().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblPeriodoAcademico", grupo.getTblPeriodoAcademico()));
				
			}
			
			//Validamos si consulta por la materia por pensum
			if (grupo.getTblProfesorXInstitucion() != null && grupo.getTblProfesorXInstitucion().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblProfesorXInstitucion", grupo.getTblProfesorXInstitucion()));
				
			}
			
					
		}
		
		return (ArrayList<TblGrupo>)consulta.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblGrupo> consultarPorEstudianteGrupo(TblEstudianteXGrupo estudianteXGrupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblGrupo.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (estudianteXGrupo != null){
			
			//Validamos si consulta por la materia
			if (estudianteXGrupo.getInCodigo() != null) {
				
				consulta.add(Restrictions.eq("tblEstudianteXGrupo.inCodigo", estudianteXGrupo.getInCodigo()));
				
			}
						
		}
		
		return (ArrayList<TblGrupo>)consulta.list();
	}
	
}
