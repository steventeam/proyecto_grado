package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensumXSemestre;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class PensumXSemestreHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblPensumXSemestre pensumXSemestre) throws Exception {
		
		
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_CREACION_DATOS");
			sesion.save(pensumXSemestre);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_X_SEMESTRE_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblPensumXSemestre consultarPorCodigo(TblPensumXSemestre pensumXSemestre) {
		TblPensumXSemestre pensumXSemestreConsultado = sesion.load(TblPensumXSemestre.class, pensumXSemestre.getInCodigo());
		return pensumXSemestreConsultado;
		
	
	}
	
	public void actualizarConMerge(TblPensumXSemestre pensumXSemestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ACTUALIZACION_DATOS");
			//1. Consultamos el objeto persistente
			TblPensumXSemestre pensumXSemestreActualizar = consultarPorCodigo(pensumXSemestre);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar

			
			//Pendiente colocar campos pensumXSemestre ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(pensumXSemestreActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblPensumXSemestre pensumXSemestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ACTUALIZACION_DATOS");
			sesion.update(pensumXSemestre);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblPensumXSemestre pensumXSemestre) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblPensumXSemestre pensumXSemestreInhabilitar = consultarPorCodigo(pensumXSemestre);
			
			sesion.merge(pensumXSemestreInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ELIMINACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PENSUM_X_SEMESTRE_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblPensumXSemestre> consultarPorFiltro(TblPensumXSemestre pensumXSemestre)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblPensumXSemestre.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (pensumXSemestre != null){
			
			//Validamos si consulta por el c�digo
			if (pensumXSemestre.getInCodigo() != null && pensumXSemestre.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", pensumXSemestre.getInCodigo()));
				
			}
			
			
			
		}
		
		return (ArrayList<TblPensumXSemestre>)consulta.list();
	}
}
