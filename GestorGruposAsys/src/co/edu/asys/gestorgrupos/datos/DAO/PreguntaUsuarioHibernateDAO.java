package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPreguntaUsuario;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("preguntaUsuarioUsuarioDAO")
@Scope("prototype")
public class PreguntaUsuarioHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegPreguntaUsuario preguntaUsuario) throws Exception {

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_CREACION_DATOS", preguntaUsuario.getNvRespuesta());

			sesion.save(preguntaUsuario);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CREACION_DATOS", preguntaUsuario.getNvRespuesta());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_USUARIO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegPreguntaUsuario consultarPorCodigo(SegPreguntaUsuario preguntaUsuario) {
		SegPreguntaUsuario preguntaUsuarioConsultado = sesion.load(SegPreguntaUsuario.class, preguntaUsuario.getInCodigo());
		return preguntaUsuarioConsultado;
	
	}
	
	public void actualizarConMerge(SegPreguntaUsuario preguntaUsuario) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ACTUALIZACION_DATOS", preguntaUsuario.getNvRespuesta());
			//1. Consultamos el objeto persistente
			SegPreguntaUsuario preguntaUsuarioActualizar = consultarPorCodigo(preguntaUsuario);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			preguntaUsuarioActualizar.setNvRespuesta(preguntaUsuario.getNvRespuesta());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(preguntaUsuarioActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ACTUALIZACION_DATOS", preguntaUsuario.getNvRespuesta());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(SegPreguntaUsuario preguntaUsuario) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ACTUALIZACION_DATOS", preguntaUsuario.getNvRespuesta());
			
			sesion.update(preguntaUsuario);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ACTUALIZACION_DATOS", preguntaUsuario.getNvRespuesta());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(SegPreguntaUsuario preguntaUsuario) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ELIMINACION_DATOS", preguntaUsuario.getNvRespuesta());

			//1. Consultamos el objeto persistente
			SegPreguntaUsuario preguntaUsuarioInhabilitar = consultarPorCodigo(preguntaUsuario);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			preguntaUsuarioInhabilitar.setDtFechaEliminacion(new Date());
			//Pendiente colocar usuario eliminaci�n ===================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(preguntaUsuarioInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ELIMINACION_DATOS", preguntaUsuario.getNvRespuesta());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PREGUNTA_USUARIO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void recuperar(SegPreguntaUsuario preguntaUsuario) throws Exception {


	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegPreguntaUsuario> consultarPorFiltro(SegPreguntaUsuario pregunta)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegPreguntaUsuario.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (pregunta != null){
			
			//Validamos si consulta por el c�digo
			if (pregunta.getInCodigo() != null && pregunta.getInCodigo() !=0) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", pregunta.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", pregunta.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (pregunta.getNvRespuesta() != null && !pregunta.getNvRespuesta().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", pregunta.getNvNombre()));
				consulta.add(Restrictions.eq("nvRespuesta", pregunta.getNvRespuesta().trim()));
			}
		}
		
		return (ArrayList<SegPreguntaUsuario>)consulta.list();
	}

	
}
