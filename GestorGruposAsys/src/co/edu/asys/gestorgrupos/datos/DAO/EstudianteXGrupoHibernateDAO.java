package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class EstudianteXGrupoHibernateDAO {

private Session sesion;

	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		
//		Criteria consulta = sesion.createCriteria(TblEstudianteXGrupo.class).add(Restrictions.eq("tblGrupo.tblMateriaXPensum.tblMateriaXPensum.inCodigo", 0));
				
		try {
			
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_CREACION_DATOS");
//			if(consulta != null){
				sesion.save(estudianteXGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CREACION_DATOS");
//			}

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_X_GRUPO_CREACION");

			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
		
	}
	
	public TblEstudianteXGrupo consultarPorCodigo(TblEstudianteXGrupo estudianteXGrupo) {
		TblEstudianteXGrupo estudianteXGrupoConsultado = sesion.load(TblEstudianteXGrupo.class, estudianteXGrupo.getInCodigo());
		return estudianteXGrupoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblEstudianteXGrupo estudianteXGrupoActualizar = consultarPorCodigo(estudianteXGrupo);
			
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			
			//Pendiente colocar campos estudianteXGrupo ======================================
		
			
			//3. Hacemos un merge de la información
			sesion.merge(estudianteXGrupoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ACTUALIZACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");

			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	public void actualizarConUpdate(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ACTUALIZACION_DATOS");
			sesion.update(estudianteXGrupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ACTUALIZACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");

			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	public void inhabilitarConMerge(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblEstudianteXGrupo estudianteXGrupoInhabilitar = consultarPorCodigo(estudianteXGrupo);
			
			sesion.merge(estudianteXGrupoInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ELIMINACION_DATOS");

		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");

			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblEstudianteXGrupo> consultarPorFiltro(TblEstudianteXGrupo estudianteXGrupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblEstudianteXGrupo.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (estudianteXGrupo != null){
			
			//Validamos si consulta por el estudiante por institución
			if (estudianteXGrupo.getTblEstudianteXInstitucion() != null && estudianteXGrupo.getTblEstudianteXInstitucion().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblEstudianteXInstitucion", estudianteXGrupo.getTblEstudianteXInstitucion()));
				
			}
			
			//Validamos si consulta por el 
			if (estudianteXGrupo.getTblGrupo() != null && estudianteXGrupo.getTblGrupo().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblGrupo", estudianteXGrupo.getTblGrupo()));
				
			}
			
//			consulta.add(Restrictions.or(
//				        Restrictions.and(Property.forName("class").eq(TblGrupo.class))
//				        , Restrictions.and(Property.forName("class").eq(TblMateriaXPensum.class)
//				        		, Restrictions.eq("inCodigo", 0))));
		}
		
		return (ArrayList<TblEstudianteXGrupo>)consulta.list();
	}
	
	
	
	
	public int consultarPorFiltroCrear(TblEstudianteXGrupo estudianteXGrupo)
			throws Exception {
//		Criteria consulta = sesion.createCriteria(TblEstudianteXGrupo.class, "estudianteGrupo");
		//String consulta = "from SegTipoIdentificacion ";
			//	
//		if (estudianteXGrupo != null){
//				    
//				    Criteria grupoCriteria =  consulta.createCriteria("tblGrupo","grupo");
//				    Criteria estudianteInstitucionCriteria =  consulta.createCriteria("tblEstudianteXInstitucion","estudianteXInstitucion");
//				    Criteria materiaPensumCriteria =  grupoCriteria.createCriteria("tblMateriaXPensum","materiaPensum");
//				    Criteria materiaCriteria = materiaPensumCriteria.createCriteria("tblMateria", "materia");
//				    
//				   if(materiaCriteria.add(Restrictions.like("materia.nvNombre", "Herramientas ofimáticas y nuevas tecnologías avanzadas II")) != null){
//					   
//					   Conjunction and = Restrictions.conjunction();
//					   
//					   and.add(Restrictions.eq("estudianteXInstitucion.inCodigo", estudianteXGrupo.getTblEstudianteXInstitucion().getInCodigo()));
//					   and.add(Restrictions.like("materia.nvNombre", "Procesamiento de información y nuevas tecnologías I"));
//				   
//				    ProjectionList properties = Projections.projectionList();
//				    properties.add(Projections.property("materia.nvNombre"));
//				    properties.add(Projections.property("estudianteXInstitucion.inCodigo"));
//
//				    consulta.setProjection(properties);
//				   }
//		}
		
			
			Query query = sesion.createQuery("SELECT d.NV_NOMBRE, f.NV_PRIMER_NOMBRE FROM tbl_estudiante_x_institucion as e inner join fetch"
			+ "(tbl_estudiante_x_grupo as a, tbl_grupo as b, tbl_materia_x_pensum as c, tbl_materia as d, seg_usuario as f) "
			+ "on a.IN_CODIGO_GRUPO = b.IN_CODIGO and b.IN_CODIGO_MATERIA_X_PENSUM = c.IN_CODIGO and "
			+ "c.IN_CODIGO_MATERIA = d.IN_CODIGO and a.IN_CODIGO_ESTUDIANTE_INSTITUCION = e.IN_CODIGO and "
			+ "e.IN_CODIGO_ESTUDIANTE_USUARIO = f.IN_CODIGO where "
			+ "e.IN_CODIGO = 1 and d.NV_NOMBRE like '%Procesamiento de información y nuevas tecnologías I' limit 1");
//			query.setParameter("in_codigo", estudianteXGrupo.getTblEstudianteXInstitucion().getInCodigo());
			
			int result = query.executeUpdate();
			
				
		return result;
	}
	

}
