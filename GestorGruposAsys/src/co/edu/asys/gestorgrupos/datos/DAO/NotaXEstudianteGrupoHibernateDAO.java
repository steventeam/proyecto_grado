package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXEstudianteGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class NotaXEstudianteGrupoHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_CREACION_DATOS");
			notaXEstudianteGrupo.setDaFechaRegistro(new Date());
			sesion.save(notaXEstudianteGrupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblNotaXEstudianteGrupo consultarPorCodigo(TblNotaXEstudianteGrupo notaXEstudianteGrupo) {
		TblNotaXEstudianteGrupo notaXEstudianteGrupoConsultado = sesion.load(TblNotaXEstudianteGrupo.class, notaXEstudianteGrupo.getInCodigo());
		return notaXEstudianteGrupoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ACTUALIZACION_DATOS");
			//1. Consultamos el objeto persistente
			TblNotaXEstudianteGrupo notaXEstudianteGrupoActualizar = consultarPorCodigo(notaXEstudianteGrupo);
			
			//2. Vaciamos al objeto persistente la información que vamos a actualizar
			
			notaXEstudianteGrupoActualizar.setNuNota(notaXEstudianteGrupo.getNuNota());
			
			//3. Hacemos un merge de la información
			sesion.merge(notaXEstudianteGrupoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ACTUALIZACION_DATOS");
			sesion.update(notaXEstudianteGrupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ELIMINACION_DATOS");

			TblNotaXEstudianteGrupo notaXEstudianteGrupoInhabilitar = consultarPorCodigo(notaXEstudianteGrupo);
			
			sesion.merge(notaXEstudianteGrupoInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ELIMINACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
			
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorFiltro(TblNotaXEstudianteGrupo notaXEstudianteGrupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblNotaXEstudianteGrupo.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (notaXEstudianteGrupo != null){
						
			//Validamos si consulta por el estudiante grupo
			 if (notaXEstudianteGrupo.getTblEstudianteXGrupo() != null && notaXEstudianteGrupo.getTblEstudianteXGrupo().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblEstudianteXGrupo", notaXEstudianteGrupo.getTblEstudianteXGrupo()));
			}
			 
			//Validamos si consulta por el nombre
			 if (notaXEstudianteGrupo.getTblNotaXGrupo() != null && notaXEstudianteGrupo.getTblNotaXGrupo().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblNotaXGrupo", notaXEstudianteGrupo.getTblNotaXGrupo()));
			}
			 
			//Validamos si consulta por el nombre
			 if (notaXEstudianteGrupo.getTblSesion() != null && notaXEstudianteGrupo.getTblSesion().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblSesion", notaXEstudianteGrupo.getTblSesion()));
			}
			
			//Validamos si consulta por el nombre
			 if (notaXEstudianteGrupo.getDaFechaRegistro() != null && !notaXEstudianteGrupo.getDaFechaRegistro().equals("")) {
				consulta.add(Restrictions.le("daFechaRegistro", notaXEstudianteGrupo.getDaFechaRegistro()));
			}
			
			notaXEstudianteGrupo.setNuNotaPorcentual(notaXEstudianteGrupo.getNuNota() * notaXEstudianteGrupo.getTblNotaXGrupo().getInPorcentaje());
		}
		
		return (ArrayList<TblNotaXEstudianteGrupo>)consulta.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorNumeroId(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		Criteria consulta = sesion.createCriteria(TblNotaXEstudianteGrupo.class);
		//
		if (notaXEstudianteGrupo != null) {

			//Validamos si consulta por el estudiante grupo
			 if (notaXEstudianteGrupo.getTblEstudianteXGrupo() != null && notaXEstudianteGrupo.getTblEstudianteXGrupo().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblEstudianteXGrupo.inCodigo", notaXEstudianteGrupo.getTblEstudianteXGrupo().getInCodigo()));
			}
			 
			 Query query = sesion.createQuery("update ignore tbl_nota_x_estudiante_grupo as nota set NU_NOTA_PORCENTUAL = " + 
					" (Select (porc.IN_PORCENTAJE*nota.NU_NOTA/100) as nu_nota from tbl_nota_x_grupo porc where porc.IN_CODIGO = 1)");
			 
			@SuppressWarnings("unused")
			int result = query.executeUpdate();
			
			
		}

		return (ArrayList<TblNotaXEstudianteGrupo>) consulta.list();
	}
	
	
	
}
