package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPerfil;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("perfilHibernateDAO")
@Scope("prototype")
public class PerfilHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegPerfil perfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_CREACION_DATOS", perfil.getNvNombre());
			sesion.save(perfil);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_CREACION_DATOS", perfil.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERFIL_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public SegPerfil consultarPorCodigo(SegPerfil perfil) {
		SegPerfil perfilConsultado = sesion.load(SegPerfil.class, perfil.getInCodigo());
		return perfilConsultado;
	
	}
	
	public void actualizarConMerge(SegPerfil perfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_ACTUALIZACION_DATOS", perfil.getNvNombre());

			//1. Consultamos el objeto persistente
			SegPerfil perfilActualizar = consultarPorCodigo(perfil);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			perfilActualizar.setNvNombre(perfil.getNvNombre());
		
			//3. Hacemos un merge de la informaci�n
			sesion.merge(perfilActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_ACTUALIZACION_DATOS", perfil.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERFIL_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void actualizarConUpdate(SegPerfil perfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_ACTUALIZACION_DATOS", perfil.getNvNombre());
					
			sesion.update(perfil);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_ACTUALIZACION_DATOS", perfil.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERFIL_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(SegPerfil perfil) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_ELIMINACION_DATOS", perfil.getNvNombre());

			//1. Consultamos el objeto persistente
			SegPerfil perfilInhabilitar = consultarPorCodigo(perfil);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			perfilInhabilitar.setDtFechaEliminacion(new Date());
			//Pendiente colocar usuario eliminaci�n ===================================
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(perfilInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_ELIMINACION_DATOS", perfil.getNvNombre());
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_PERFIL_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegPerfil> consultarPorFiltro(SegPerfil perfil)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegPerfil.class);
		//String consulta = "from SegPerfil ";
			//	
		if (perfil != null){
			
			//Validamos si consulta por el c�digo
			if (perfil.getInCodigo() != null && perfil.getInCodigo() != 0 ) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", perfil.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", perfil.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (perfil.getNvNombre() != null && !perfil.getNvNombre().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", perfil.getNvNombre()));
				consulta.add(Restrictions.eq("nvNombre", perfil.getNvNombre().trim()));
			}
		}
		
		return (ArrayList<SegPerfil>)consulta.list();
	}

	
}
