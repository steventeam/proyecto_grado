package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("tipoIdentificacionHibernateDAO")
@Scope("prototype")
public class TipoIdentificacionHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		
		try{
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_CREACION_DATOS", tipoIdentificacion.getNvNombre());
			
			
			sesion.save(tipoIdentificacion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CREACION_DATOS", tipoIdentificacion.getNvNombre());
			
			}catch(Exception excepcion){
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_IDENTIFICACION_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		
		}
	}
	
	public SegTipoIdentificacion consultarPorCodigo(SegTipoIdentificacion tipoIdentificacion) {
		SegTipoIdentificacion tipoIdentificacionConsultado = sesion.load(SegTipoIdentificacion.class, tipoIdentificacion.getInCodigo());
		return tipoIdentificacionConsultado;
	
	}
	
	public void actualizarConMerge(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		
		try{
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ACTUALIZACION_DATOS", tipoIdentificacion.getNvNombre());

			//1. Consultamos el objeto persistente
			SegTipoIdentificacion tipoIdentificacionActualizar = consultarPorCodigo(tipoIdentificacion);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			tipoIdentificacionActualizar.setNvNombre(tipoIdentificacion.getNvNombre());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoIdentificacionActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ACTUALIZACION_DATOS", tipoIdentificacion.getNvNombre());
			
			}catch(Exception excepcion){
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
			}
		
	}
	
	public void actualizarConUpdate(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		
		try{
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ACTUALIZACION_DATOS", tipoIdentificacion.getNvNombre());
			
			sesion.update(tipoIdentificacion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ACTUALIZACION_DATOS", tipoIdentificacion.getNvNombre());
			
			}catch(Exception excepcion){
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		
		}
		
	}
	
	public void inhabilitarConMerge(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		
		
		try{
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ELIMINACION_DATOS", tipoIdentificacion.getNvNombre());
			
			//1. Consultamos el objeto persistente
			SegTipoIdentificacion tipoIdentificacionInhabilitar = consultarPorCodigo(tipoIdentificacion);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			tipoIdentificacionInhabilitar.setDtFechaEliminacion(new Date());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(tipoIdentificacionInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ELIMINACION_DATOS", tipoIdentificacion.getNvNombre());
			
			}catch(Exception excepcion){
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_TIPO_IDENTIFICACION_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
		
		
		}
		
	}
	
	/*@SuppressWarnings("unchecked")
	public ArrayList<SegTipoIdentificacion> consultarTodo() throws Exception {
		ArrayList<SegTipoIdentificacion> listaTotal =
				(ArrayList<SegTipoIdentificacion>)
				sesion.createQuery("from SegTipoIdentificacion").list();
		
		return listaTotal;
	} 
	*/
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegTipoIdentificacion> consultarPorFiltro(SegTipoIdentificacion tipoIdentificacion)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegTipoIdentificacion.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (tipoIdentificacion != null){
			
			//Validamos si consulta por el c�digo
			if (tipoIdentificacion.getInCodigo() != null) {
				//consulta = consulta + "where inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", tipoIdentificacion.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", tipoIdentificacion.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (tipoIdentificacion.getNvNombre() != null && !tipoIdentificacion.getNvNombre().trim().equals("")) {
				//consulta = consulta + "where inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", tipoIdentificacion.getNvNombre()));
				consulta.add(Restrictions.eq("nvNombre", tipoIdentificacion.getNvNombre().trim()));
			}
			
			if (tipoIdentificacion.getDtFechaEliminacion() != null){
				consulta.add(Restrictions.le("dtFechaEliminacion", tipoIdentificacion.getDtFechaEliminacion()));
			}
		}
		
		return (ArrayList<SegTipoIdentificacion>)consulta.list();
	}

	
}
