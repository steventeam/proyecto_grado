package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblSesion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class SesionHibernateDAO {

private Session sesion1;
	
	public void setSession(Session sesionActiva) {
		this.sesion1 = sesionActiva;
	}
	
	public void crear(TblSesion session1) throws Exception {
		
		
		int dias = 7;
		Date fecha = session1.getDtFechaSesion();
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_CREACION_DATOS");
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(fecha); // Configuramos la fecha que se recibe
		    calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de d�as a a�adir, o restar en caso de d�as<0
		    session1.setDtFechaLimite(calendar.getTime());
		    
		     sesion1.save(session1);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SESION_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public TblSesion consultarPorCodigo(TblSesion session1) {
		TblSesion session1Consultado = sesion1.load(TblSesion.class, session1.getInCodigo());
		return session1Consultado;
		
	
	}
	
	public void actualizarConMerge(TblSesion session1) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ACTUALIZACION_DATOS");
			//1. Consultamos el objeto persistente
			TblSesion session1Actualizar = consultarPorCodigo(session1);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			session1Actualizar.setDtFechaLimite(session1.getDtFechaLimite());
			
			//Pendiente colocar campos session1 ======================================
			
			//3. Hacemos un merge de la informaci�n
			sesion1.merge(session1Actualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SESION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
		
	}
	
	public void actualizarConUpdate(TblSesion session1) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ACTUALIZACION_DATOS");

			sesion1.update(session1);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SESION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	public void inhabilitarConMerge(TblSesion session1) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ELIMINACION_DATOS");
			//1. Consultamos el objeto persistente
			TblSesion session1Inhabilitar = consultarPorCodigo(session1);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//Pendiente colocar session1 eliminaci�n
			
			//3. Hacemos un merge de la informaci�n
			sesion1.merge(session1Inhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ELIMINACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_SESION_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblSesion> consultarPorFiltro(TblSesion session1)
			throws Exception {
		Criteria consulta = sesion1.createCriteria(TblSesion.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (session1 != null){
			
			//Validamos si consulta por el c�digo
			if (session1.getInCodigo() != null && session1.getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("inCodigo", session1.getInCodigo()));
				
			}
			
			if (session1.getInSesion() != 0) {
				
				consulta.add(Restrictions.eq("inSesion", session1.getInSesion()));
				
			}
			
			if (session1.getTblGrupo() != null && session1.getTblGrupo().getInCodigo() != 0) {
				
				consulta.add(Restrictions.eq("tblGrupo", session1.getTblGrupo()));
				
			}
			
		}
		
		return (ArrayList<TblSesion>)consulta.list();
	}
}
