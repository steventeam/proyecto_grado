package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository("aplicacionHibernateDAO")
@Scope("prototype")
public class AplicacionHibernateDAO {
	
	private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	public void crear(SegAplicacion aplicacion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_CREACION_DATOS", aplicacion.getNvNombre());

			sesion.save(aplicacion);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CREACION_DATOS", aplicacion.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_APLICACION_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public SegAplicacion consultarPorCodigo(SegAplicacion aplicacion) {
		SegAplicacion aplicacionConsultado = sesion.load(SegAplicacion.class, aplicacion.getInCodigo());
		return aplicacionConsultado;
	
	}
	
	public void actualizarConMerge(SegAplicacion aplicacion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_DATOS", aplicacion.getNvNombre());

			//1. Consultamos el objeto persistente
			SegAplicacion aplicacionActualizar = consultarPorCodigo(aplicacion);
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			aplicacionActualizar.setNvNombre(aplicacion.getNvNombre());
			
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(aplicacionActualizar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_DATOS", aplicacion.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_APLICACION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public void actualizarConUpdate(SegAplicacion aplicacion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_DATOS", aplicacion.getNvNombre());
			
			sesion.update(aplicacion);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_DATOS", aplicacion.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_APLICACION_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(SegAplicacion aplicacion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ELIMINACION_DATOS", aplicacion.getNvNombre());

			//1. Consultamos el objeto persistente
			SegAplicacion aplicacionInhabilitar = consultarPorCodigo(aplicacion);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			aplicacionInhabilitar.setDtFechaEliminacion(new Date());
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(aplicacionInhabilitar);
			
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ELIMINACION_DATOS", aplicacion.getNvNombre());
			
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_APLICACION_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	/*@SuppressWarnings("unchecked")
	public ArrayList<SegAplicacion> consultarTodo() throws Exception {
		ArrayList<SegAplicacion> listaTotal =
				(ArrayList<SegAplicacion>)
				sesion.createQuery("from SegAplicacion").list();
		
		return listaTotal;
	} 
	*/
	
	@SuppressWarnings("unchecked")
	public ArrayList<SegAplicacion> consultarPorFiltro(SegAplicacion aplicacion)
			throws Exception {
		Criteria consulta = sesion.createCriteria(SegAplicacion.class);
		//String consulta = "from SegAplicacion ";
			//	
		if (aplicacion != null){
			
			//Validamos si consulta por el c�digo
			if (aplicacion.getInCodigo() != null && aplicacion.getInCodigo() != 0) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("inCodigo", aplicacion.getInCodigo()));
				consulta.add(Restrictions.eq("inCodigo", aplicacion.getInCodigo()));
				
			}
			
			//Validamos si consulta por el nombre
			if (aplicacion.getNvNombre() != null && !aplicacion.getNvNombre().trim().equals("")) {
				//consulta = consulta + "wheere inCodigo = :inCodigo"
				//consulta.add(Expression.eq("nvNombre", aplicacion.getNvNombre()));
				consulta.add(Restrictions.eq("nvNombre", aplicacion.getNvNombre()));
			}
		}
		
		return (ArrayList<SegAplicacion>)consulta.list();
	}

	
}
