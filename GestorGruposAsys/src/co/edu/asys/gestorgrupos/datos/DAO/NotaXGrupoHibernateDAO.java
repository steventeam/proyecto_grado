package co.edu.asys.gestorgrupos.datos.DAO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Repository
@Scope("prototype")
public class NotaXGrupoHibernateDAO {

private Session sesion;
	
	public void setSession(Session sesionActiva) {
		this.sesion = sesionActiva;
	}
	
	
	public void crear(TblNotaXGrupo notaXGrupo) throws Exception {
		
		int dias = 7;
		Date fecha= notaXGrupo.getDaFechaRegistro();
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_CREACION_DATOS");
			Calendar calendar = Calendar.getInstance();
		      calendar.setTime(fecha); // Configuramos la fecha que se recibe
		      calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de d�as a a�adir, o restar en caso de d�as<0
		      notaXGrupo.setDaFechaLimiteRegistro(calendar.getTime());
		      
		      sesion.save(notaXGrupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_CREACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_GRUPO_CREACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
		
	}
	
	public TblNotaXGrupo consultarPorCodigo(TblNotaXGrupo notaXGrupo) {
		TblNotaXGrupo notaXGrupoConsultado = sesion.load(TblNotaXGrupo.class, notaXGrupo.getInCodigo());
		return notaXGrupoConsultado;
		
	
	}
	
	public void actualizarConMerge(TblNotaXGrupo notaXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_ACTUALIZACION_DATOS");

			//1. Consultamos el objeto persistente
			TblNotaXGrupo notaXGrupoActualizar = consultarPorCodigo(notaXGrupo);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			notaXGrupoActualizar.setNvDescripcion(notaXGrupo.getNvDescripcion());
			notaXGrupoActualizar.setDaFechaLimiteRegistro(notaXGrupo.getDaFechaLimiteRegistro());		

			
			//Pendiente colocar campos notaXGrupo ======================================
		
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(notaXGrupoActualizar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void actualizarConUpdate(TblNotaXGrupo notaXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_ACTUALIZACION_DATOS");
			sesion.update(notaXGrupo);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_ACTUALIZACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	public void inhabilitarConMerge(TblNotaXGrupo notaXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_ELIMINACION_DATOS");

			//1. Consultamos el objeto persistente
			TblNotaXGrupo notaXGrupoInhabilitar = consultarPorCodigo(notaXGrupo);
			
			
			//2. Vaciamos al objeto persistente la informaci�n que vamos a actualizar
			
			//3. Hacemos un merge de la informaci�n
			sesion.merge(notaXGrupoInhabilitar);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_ELIMINACION_DATOS");
		} catch (Exception excepcion) {
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "ERROR_NOTA_X_GRUPO_ELIMINACION");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.DATOS, false, this.getClass());
			
			
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<TblNotaXGrupo> consultarPorFiltro(TblNotaXGrupo notaXGrupo)
			throws Exception {
		Criteria consulta = sesion.createCriteria(TblNotaXGrupo.class);
		//String consulta = "from SegTipoIdentificacion ";
			//	
		if (notaXGrupo != null){
		
			//Validamos si consulta por el nombre
			 if (notaXGrupo.getNvDescripcion() != null && !notaXGrupo.getNvDescripcion().trim().equals("")) {
				consulta.add(Restrictions.eq("nvDescripcion", notaXGrupo.getNvDescripcion().trim()));
			}
						 
			//Validamos si consulta por el grupo
			if (notaXGrupo.getTblGrupo() != null && notaXGrupo.getTblGrupo().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblGrupo", notaXGrupo.getTblGrupo()));
			}
			
			//Validamos si consulta por el tipo de nota 
			if (notaXGrupo.getTblTipoNota() != null && notaXGrupo.getTblTipoNota().getInCodigo() != 0) {
				consulta.add(Restrictions.eq("tblTipoNota", notaXGrupo.getTblTipoNota()));
			}

			 //Validamos si consulta por el porcentaje
			 if (notaXGrupo.getInPorcentaje() != 0) {
				 consulta.add(Restrictions.le("inPorcentaje", notaXGrupo.getInPorcentaje()));
				 
			 }
			
			 //Validamos si consulta por la fecha l�mite de registro
//			 if (notaXGrupo.getDaFechaLimiteRegistro().before(notaXGrupo.getDaFechaRegistro()) ) {
//				 consulta.add(Restrictions.gt("daFechaLimiteRegistro", notaXGrupo.getDaFechaLimiteRegistro()));
//			 }
			
		}
		
		return (ArrayList<TblNotaXGrupo>)consulta.list();
	}
}
