package co.edu.asys.gestorgrupos.datos.daofactory;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.edu.asys.gestorgrupos.datos.DAO.*;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;

@Repository
@Scope("prototype")
public class HibernateDAOFactory {
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	private Session sesionActiva;
	
	private Transaction transaccion; 
	
	@PostConstruct
	private void configurar() {
		abrirConexion();
		iniciarTransaccion();
		
	}
	
	private void abrirConexion() {
		sesionActiva = sessionFactory.openSession();
	}
	
	private void iniciarTransaccion() {
		transaccion = sesionActiva.beginTransaction();
	}
	
	public void confirmarTransaccion() {
		transaccion.commit();
	}
	
	public void cancelarTransaccion() {
		transaccion.rollback();
	}
	
	public void cerrarConexion() {
		sesionActiva.disconnect();
	}
	
	public TipoIdentificacionHibernateDAO obtenerTipoIdentificacionDAO() {
		TipoIdentificacionHibernateDAO	 tipoIdentificacionDAO = LocalizadorBean.ObtenerBean(TipoIdentificacionHibernateDAO.class);
		tipoIdentificacionDAO.setSession(sesionActiva);
		return tipoIdentificacionDAO;
	}
	
	public UsuarioHibernateDAO obtenerUsuarioDAO() {
		UsuarioHibernateDAO	 usuarioDAO = LocalizadorBean.ObtenerBean(UsuarioHibernateDAO.class);
		usuarioDAO.setSession(sesionActiva);
		return usuarioDAO;
		
	}
	
	public TipoRecursoHibernateDAO obtenerTipoRecursoDAO() {
		TipoRecursoHibernateDAO	 tipoRecursoDAO = LocalizadorBean.ObtenerBean(TipoRecursoHibernateDAO.class);
		tipoRecursoDAO.setSession(sesionActiva);
		return tipoRecursoDAO;
	}
	
	public PreguntaHibernateDAO obtenerPreguntaDAO() {
		PreguntaHibernateDAO	 preguntaDAO = LocalizadorBean.ObtenerBean(PreguntaHibernateDAO.class);
		preguntaDAO.setSession(sesionActiva);
		return preguntaDAO;
	}
	
	public PreguntaUsuarioHibernateDAO obtenerPreguntaUsuarioDAO() {
		PreguntaUsuarioHibernateDAO	 preguntaUsuarioDAO = LocalizadorBean.ObtenerBean(PreguntaUsuarioHibernateDAO.class);
		preguntaUsuarioDAO.setSession(sesionActiva);
		return preguntaUsuarioDAO;
	}
	
	public RecuperarClaveHibernateDAO obtenerRecuperarClaveDAO() {
		RecuperarClaveHibernateDAO	 recuperarClaveDAO = LocalizadorBean.ObtenerBean(RecuperarClaveHibernateDAO.class);
		recuperarClaveDAO.setSession(sesionActiva);
		return recuperarClaveDAO;
		
	}
	
	public RecursoHibernateDAO obtenerRecursoDAO() {
		RecursoHibernateDAO	 recursoDAO = LocalizadorBean.ObtenerBean(RecursoHibernateDAO.class);
		recursoDAO.setSession(sesionActiva);
		return recursoDAO;
	}
	
	public RecursoPerfilHibernateDAO obtenerRecursoPerfilDAO() {
		RecursoPerfilHibernateDAO	 recursoPerfilDAO = LocalizadorBean.ObtenerBean(RecursoPerfilHibernateDAO.class);
		recursoPerfilDAO.setSession(sesionActiva);
		return recursoPerfilDAO;
		
	}
	
	public AplicacionHibernateDAO obtenerAplicacionDAO() {
		AplicacionHibernateDAO	 aplicacionDAO = LocalizadorBean.ObtenerBean(AplicacionHibernateDAO.class);
		aplicacionDAO.setSession(sesionActiva);
		return aplicacionDAO;
	}
	
	public PerfilHibernateDAO obtenerPerfilDAO() {
		PerfilHibernateDAO	 perfilDAO = LocalizadorBean.ObtenerBean(PerfilHibernateDAO.class);
		perfilDAO.setSession(sesionActiva);
		return perfilDAO;
		
	}
	
//	public EstudianteUsuarioHibernateDAO obtenerEstudianteUsuarioDAO() {
//		EstudianteUsuarioHibernateDAO	 estudianteHibernateDAO = LocalizadorBean.ObtenerBean(EstudianteUsuarioHibernateDAO.class);
//		estudianteHibernateDAO.setSession(sesionActiva);
//		return estudianteHibernateDAO;
//		
//	}
	
	public InstitucionHibernateDAO obtenerInstitucionDAO() {
		InstitucionHibernateDAO	 institucionDAO = LocalizadorBean.ObtenerBean(InstitucionHibernateDAO.class);
		institucionDAO.setSession(sesionActiva);
		return institucionDAO;
		
	}
	
	public MateriaHibernateDAO obtenerMateriaDAO() {
		MateriaHibernateDAO	 materiaDAO = LocalizadorBean.ObtenerBean(MateriaHibernateDAO.class);
		materiaDAO.setSession(sesionActiva);
		return materiaDAO;
		
	}
	
	public PensumHibernateDAO obtenerPensumDAO() {
		PensumHibernateDAO	 pensumDAO = LocalizadorBean.ObtenerBean(PensumHibernateDAO.class);
		pensumDAO.setSession(sesionActiva);
		return pensumDAO;
		
	}
	
	public PensumXSemestreHibernateDAO obtenerPensumXSemestreDAO() {
		PensumXSemestreHibernateDAO	 pensumXSemestreDAO = LocalizadorBean.ObtenerBean(PensumXSemestreHibernateDAO.class);
		pensumXSemestreDAO.setSession(sesionActiva);
		return pensumXSemestreDAO;
		
	}
	
	public ProgramaHibernateDAO obtenerProgramaDAO() {
		ProgramaHibernateDAO	 programaDAO = LocalizadorBean.ObtenerBean(ProgramaHibernateDAO.class);
		programaDAO.setSession(sesionActiva);
		return programaDAO;
		
	}
	
	public SemestreHibernateDAO obtenerSemestreDAO() {
		SemestreHibernateDAO	 semestreDAO = LocalizadorBean.ObtenerBean(SemestreHibernateDAO.class);
		semestreDAO.setSession(sesionActiva);
		return semestreDAO;
		
	}
	
	public TipoProgramaHibernateDAO obtenerTipoProgramaDAO() {
		TipoProgramaHibernateDAO	tipoProgramaDAO = LocalizadorBean.ObtenerBean(TipoProgramaHibernateDAO.class);
		tipoProgramaDAO.setSession(sesionActiva);
		return tipoProgramaDAO;
		
	}
	
	public GeneroHibernateDAO obtenerGeneroDAO() {
		GeneroHibernateDAO	generoDAO = LocalizadorBean.ObtenerBean(GeneroHibernateDAO.class);
		generoDAO.setSession(sesionActiva);
		return generoDAO;
		
	}
		
	public ProfesorXInstitucionHibernateDAO obtenerProfesorXInstitucionDAO() {
		ProfesorXInstitucionHibernateDAO	profesorXInstitucionDAO = LocalizadorBean.ObtenerBean(ProfesorXInstitucionHibernateDAO.class);
		profesorXInstitucionDAO.setSession(sesionActiva);
		return profesorXInstitucionDAO;
		
	}
	
	public GrupoHibernateDAO obtenerGrupoDAO() {
		GrupoHibernateDAO	grupoDAO = LocalizadorBean.ObtenerBean(GrupoHibernateDAO.class);
		grupoDAO.setSession(sesionActiva);
		return grupoDAO;
		
	}
	
	public EstudianteXGrupoHibernateDAO obtenerEstudianteXGrupoDAO() {
		EstudianteXGrupoHibernateDAO	estudianteXGrupoDAO = LocalizadorBean.ObtenerBean(EstudianteXGrupoHibernateDAO.class);
		estudianteXGrupoDAO.setSession(sesionActiva);
		return estudianteXGrupoDAO;
		
	}
	
	public EstudianteXInstitucionHibernateDAO obtenerEstudianteXInstitucionDAO() {
		EstudianteXInstitucionHibernateDAO	estudianteXInstitucionDAO = LocalizadorBean.ObtenerBean(EstudianteXInstitucionHibernateDAO.class);
		estudianteXInstitucionDAO.setSession(sesionActiva);
		return estudianteXInstitucionDAO;
		
	}
	
	public MateriaXPensumHibernateDAO obtenerMateriaXPensumDAO() {
		MateriaXPensumHibernateDAO	materiaXPensumDAO = LocalizadorBean.ObtenerBean(MateriaXPensumHibernateDAO.class);
		materiaXPensumDAO.setSession(sesionActiva);
		return materiaXPensumDAO;
		
	}
	
	public PeriodoAcademicoHibernateDAO obtenerPeriodoAcademicoDAO() {
		PeriodoAcademicoHibernateDAO	periodoAcademicoDAO = LocalizadorBean.ObtenerBean(PeriodoAcademicoHibernateDAO.class);
		periodoAcademicoDAO.setSession(sesionActiva);
		return periodoAcademicoDAO;
		
	}
	
	public TipoPeriodoAcademicoHibernateDAO obtenerTipoPeriodoAcademicoDAO() {
		TipoPeriodoAcademicoHibernateDAO	tipoPeriodoAcademicoDAO = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoHibernateDAO.class);
		tipoPeriodoAcademicoDAO.setSession(sesionActiva);
		return tipoPeriodoAcademicoDAO;
		
	}
	
	public AsistenciaHibernateDAO obtenerAsistenciaDAO() {
		AsistenciaHibernateDAO	asistenciaDAO = LocalizadorBean.ObtenerBean(AsistenciaHibernateDAO.class);
		asistenciaDAO.setSession(sesionActiva);
		return asistenciaDAO;
		
	}
		
	public NotaXEstudianteGrupoHibernateDAO obtenerNotaXEstudianteGrupoDAO() {
		NotaXEstudianteGrupoHibernateDAO	notaXEstudianteGrupoDAO = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoHibernateDAO.class);
		notaXEstudianteGrupoDAO.setSession(sesionActiva);
		return notaXEstudianteGrupoDAO;
		
	}
	
	public TipoNotaHibernateDAO obtenerTipoNotaDAO() {
		TipoNotaHibernateDAO	tipoNotaDAO = LocalizadorBean.ObtenerBean(TipoNotaHibernateDAO.class);
		tipoNotaDAO.setSession(sesionActiva);
		return tipoNotaDAO;
		
	}
	
	public NotaXGrupoHibernateDAO obtenerNotaXGrupoDAO() {
		NotaXGrupoHibernateDAO	notaXGrupoDAO = LocalizadorBean.ObtenerBean(NotaXGrupoHibernateDAO.class);
		notaXGrupoDAO.setSession(sesionActiva);
		return notaXGrupoDAO;
		
	}
	
	public SesionHibernateDAO obtenerSesionDAO() {
		SesionHibernateDAO	sesionDAO = LocalizadorBean.ObtenerBean(SesionHibernateDAO.class);
		sesionDAO.setSession(sesionActiva);
		return sesionDAO;
		
	}
	
	public ReporteHibernateDAO obtenerReporteDAO() {
		ReporteHibernateDAO	reporteDAO = LocalizadorBean.ObtenerBean(ReporteHibernateDAO.class);
		reporteDAO.setSession(sesionActiva);
		return reporteDAO;
		
	}

}
