package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.ProgramaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.DAO.RecuperarClaveHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblRecuperarClave;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class RecuperarClaveNegocio {
	
	public void crear(TblRecuperarClave recuperarClave, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecuperarClaveHibernateDAO recuperarClaveDAO = daoFactory.obtenerRecuperarClaveDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_CREACION_NEGOCIO");
				
				if(recuperarClaveDAO.consultarPorFiltro(recuperarClave).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerRecuperarClaveDAO().crear(recuperarClave);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROGRAMA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
	public void actualizar(TblRecuperarClave recuperarClave, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecuperarClaveHibernateDAO recuperarClaveDAO = daoFactory.obtenerRecuperarClaveDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(recuperarClaveDAO.consultarPorToken(recuperarClave) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerRecuperarClaveDAO().actualizarConMerge(recuperarClave);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_APLICACION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
		
	public TblRecuperarClave consultarPorCodigo(TblRecuperarClave recuperarClave, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerRecuperarClaveDAO().consultarPorToken(recuperarClave);
		
	}

	
	public ArrayList<TblRecuperarClave> consultarPorFiltro(TblRecuperarClave recuperarClave, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerRecuperarClaveDAO().consultarPorFiltro(recuperarClave);
		
	}

}
