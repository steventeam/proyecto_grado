package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.SesionHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSesion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class SesionNegocio {
	
	public void crear(TblSesion session1, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SesionHibernateDAO sesionDAO = daoFactory.obtenerSesionDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_CREACION_NEGOCIO");
				
				if(sesionDAO.consultarPorFiltro(session1).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerSesionDAO().crear(session1);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SESION_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblSesion session1, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SesionHibernateDAO sesionDAO = daoFactory.obtenerSesionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(sesionDAO.consultarPorCodigo(session1) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerSesionDAO().actualizarConMerge(session1);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SESION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblSesion session1, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SesionHibernateDAO sesionDAO = daoFactory.obtenerSesionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ELIMINACION_NEGOCIO");
			
			if(sesionDAO.consultarPorFiltro(session1).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerSesionDAO().inhabilitarConMerge(session1);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SESION_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblSesion consultarPorCodigo(TblSesion session1, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerSesionDAO().consultarPorCodigo(session1);
		
	}
	
	/*public ArrayList<TblSesion> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerSesionDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblSesion> consultarPorFiltro(TblSesion session1, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerSesionDAO().consultarPorFiltro(session1);
		
	}

}
