package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.NotaXEstudianteGrupoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXEstudianteGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class NotaXEstudianteGrupoNegocio {
	
	public void crear(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXEstudianteGrupoHibernateDAO notaXEstudianteGrupoDAO = daoFactory.obtenerNotaXEstudianteGrupoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_CREACION_NEGOCIO");
				
				if(notaXEstudianteGrupoDAO.consultarPorFiltro(notaXEstudianteGrupo).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerNotaXEstudianteGrupoDAO().crear(notaXEstudianteGrupo);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXEstudianteGrupoHibernateDAO notaXEstudianteGrupoDAO = daoFactory.obtenerNotaXEstudianteGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(notaXEstudianteGrupoDAO.consultarPorCodigo(notaXEstudianteGrupo) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerNotaXEstudianteGrupoDAO().actualizarConMerge(notaXEstudianteGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXEstudianteGrupoHibernateDAO notaXEstudianteGrupoDAO = daoFactory.obtenerNotaXEstudianteGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ELIMINACION_NEGOCIO");
			
			if(notaXEstudianteGrupoDAO.consultarPorFiltro(notaXEstudianteGrupo).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerNotaXEstudianteGrupoDAO().inhabilitarConMerge(notaXEstudianteGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblNotaXEstudianteGrupo consultarPorCodigo(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerNotaXEstudianteGrupoDAO().consultarPorCodigo(notaXEstudianteGrupo);
		
	}
	
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorFiltro(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerNotaXEstudianteGrupoDAO().consultarPorFiltro(notaXEstudianteGrupo);
		
	}
	
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorNumeroId(TblNotaXEstudianteGrupo notaXEstudianteGrupo, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerNotaXEstudianteGrupoDAO().consultarPorNumeroId(notaXEstudianteGrupo);
		
	}

}
