package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.TipoPeriodoAcademicoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPeriodoAcademico;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoPeriodoAcademicoNegocio {
	
	public void crear(TblTipoPeriodoAcademico tipoPeriodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoPeriodoAcademicoHibernateDAO tipoPeriodoAcademicoDAO = daoFactory.obtenerTipoPeriodoAcademicoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_CREACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
				
				if(tipoPeriodoAcademicoDAO.consultarPorFiltro(tipoPeriodoAcademico).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION_YA_EXISTE", tipoPeriodoAcademico.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerTipoPeriodoAcademicoDAO().crear(tipoPeriodoAcademico);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CREACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblTipoPeriodoAcademico tipoPeriodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoPeriodoAcademicoHibernateDAO tipoPeriodoAcademicoDAO = daoFactory.obtenerTipoPeriodoAcademicoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
			
			if(tipoPeriodoAcademicoDAO.consultarPorCodigo(tipoPeriodoAcademico) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION_NO_EXISTE", tipoPeriodoAcademico.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoPeriodoAcademicoDAO().actualizarConMerge(tipoPeriodoAcademico);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblTipoPeriodoAcademico tipoPeriodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoPeriodoAcademicoHibernateDAO tipoPeriodoAcademicoDAO = daoFactory.obtenerTipoPeriodoAcademicoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ELIMINACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
			
			if(tipoPeriodoAcademicoDAO.consultarPorFiltro(tipoPeriodoAcademico).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION_NO_EXISTE", tipoPeriodoAcademico.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoPeriodoAcademicoDAO().inhabilitarConMerge(tipoPeriodoAcademico);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ELIMINACION_NEGOCIO", tipoPeriodoAcademico.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblTipoPeriodoAcademico consultarPorCodigo(TblTipoPeriodoAcademico tipoPeriodoAcademico, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerTipoPeriodoAcademicoDAO().consultarPorCodigo(tipoPeriodoAcademico);
		
	}
	
	/*public ArrayList<TblTipoPeriodoAcademico> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoPeriodoAcademicoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblTipoPeriodoAcademico> consultarPorFiltro(TblTipoPeriodoAcademico tipoPeriodoAcademico, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoPeriodoAcademicoDAO().consultarPorFiltro(tipoPeriodoAcademico);
		
	}

}
