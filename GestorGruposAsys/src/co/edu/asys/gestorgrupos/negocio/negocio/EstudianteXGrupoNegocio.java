package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.EstudianteXGrupoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class EstudianteXGrupoNegocio {
	
	public void crear(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		EstudianteXGrupoHibernateDAO estudianteXGrupoDAO = daoFactory.obtenerEstudianteXGrupoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_CREACION_NEGOCIO");
				
				if(estudianteXGrupoDAO.consultarPorFiltro(estudianteXGrupo).size() > 0) {
					if(estudianteXGrupoDAO.consultarPorFiltroCrear(estudianteXGrupo) == 0){
						String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION_YA_EXISTE");
						UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					}
				}
					daoFactory.obtenerEstudianteXGrupoDAO().crear(estudianteXGrupo);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ESTUDIANTE_X_GRUPO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		EstudianteXGrupoHibernateDAO estudianteXGrupoDAO = daoFactory.obtenerEstudianteXGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(estudianteXGrupoDAO.consultarPorCodigo(estudianteXGrupo) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerEstudianteXGrupoDAO().actualizarConMerge(estudianteXGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		EstudianteXGrupoHibernateDAO estudianteXGrupoDAO = daoFactory.obtenerEstudianteXGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ELIMINACION_NEGOCIO");
			
			if(estudianteXGrupoDAO.consultarPorFiltro(estudianteXGrupo).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerEstudianteXGrupoDAO().inhabilitarConMerge(estudianteXGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblEstudianteXGrupo consultarPorCodigo(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerEstudianteXGrupoDAO().consultarPorCodigo(estudianteXGrupo);
		
	}
	
	/*public ArrayList<TblEstudianteXGrupo> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerEstudianteXGrupoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblEstudianteXGrupo> consultarPorFiltro(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerEstudianteXGrupoDAO().consultarPorFiltro(estudianteXGrupo);
		
	}


}
