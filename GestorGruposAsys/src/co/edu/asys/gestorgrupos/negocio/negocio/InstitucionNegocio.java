package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.InstitucionHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblInstitucion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class InstitucionNegocio {
	
	public void crear(TblInstitucion institucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		InstitucionHibernateDAO institucionDAO = daoFactory.obtenerInstitucionDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_CREACION_NEGOCIO", institucion.getNvNombre());
				
				if(institucionDAO.consultarPorFiltro(institucion).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION_YA_EXISTE", institucion.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerInstitucionDAO().crear(institucion);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_CREACION_NEGOCIO", institucion.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_INSTITUCION_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblInstitucion institucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		InstitucionHibernateDAO institucionDAO = daoFactory.obtenerInstitucionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_ACTUALIZACION_NEGOCIO", institucion.getNvNombre());
			
			if(institucionDAO.consultarPorCodigo(institucion) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION_NO_EXISTE", institucion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerInstitucionDAO().actualizarConMerge(institucion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_ACTUALIZACION_NEGOCIO", institucion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_INSTITUCION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblInstitucion institucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		InstitucionHibernateDAO institucionDAO = daoFactory.obtenerInstitucionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_ANTES_ELIMINACION_NEGOCIO", institucion.getNvNombre());
			
			if(institucionDAO.consultarPorFiltro(institucion).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION_NO_EXISTE", institucion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerInstitucionDAO().inhabilitarConMerge(institucion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_INSTITUCION_DESPUES_ELIMINACION_NEGOCIO", institucion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_INSTITUCION_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblInstitucion consultarPorCodigo(TblInstitucion institucion, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerInstitucionDAO().consultarPorCodigo(institucion);
		
	}
	
	/*public ArrayList<TblInstitucion> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerInstitucionDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblInstitucion> consultarPorFiltro(TblInstitucion institucion, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerInstitucionDAO().consultarPorFiltro(institucion);
		
	}

}
