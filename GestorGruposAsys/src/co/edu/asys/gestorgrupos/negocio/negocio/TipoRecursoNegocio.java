package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.TipoRecursoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoRecurso;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoRecursoNegocio {
	
	public void crear(SegTipoRecurso tipoRecurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoRecursoHibernateDAO tipoRecursoDAO = daoFactory.obtenerTipoRecursoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_CREACION_NEGOCIO", tipoRecurso.getNvNombre());
				
				if(tipoRecursoDAO.consultarPorFiltro(tipoRecurso).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION_YA_EXISTE", tipoRecurso.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerTipoRecursoDAO().crear(tipoRecurso);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_CREACION_NEGOCIO", tipoRecurso.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_RECURSO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegTipoRecurso tipoRecurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoRecursoHibernateDAO tipoRecursoDAO = daoFactory.obtenerTipoRecursoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_ACTUALIZACION_NEGOCIO", tipoRecurso.getNvNombre());
			
			if(tipoRecursoDAO.consultarPorCodigo(tipoRecurso) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION_NO_EXISTE", tipoRecurso.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoRecursoDAO().actualizarConMerge(tipoRecurso);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_ACTUALIZACION_NEGOCIO", tipoRecurso.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_RECURSO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegTipoRecurso tipoRecurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoRecursoHibernateDAO tipoRecursoDAO = daoFactory.obtenerTipoRecursoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_ANTES_ELIMINACION_NEGOCIO", tipoRecurso.getNvNombre());
			
			if(tipoRecursoDAO.consultarPorFiltro(tipoRecurso).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION_NO_EXISTE", tipoRecurso.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoRecursoDAO().inhabilitarConMerge(tipoRecurso);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_RECURSO_DESPUES_ELIMINACION_NEGOCIO", tipoRecurso.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_RECURSO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegTipoRecurso consultarPorCodigo(SegTipoRecurso tipoRecurso, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerTipoRecursoDAO().consultarPorCodigo(tipoRecurso);
		
	}
	
	/*public ArrayList<SegTipoRecurso> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoRecursoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegTipoRecurso> consultarPorFiltro(SegTipoRecurso tipoRecurso, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoRecursoDAO().consultarPorFiltro(tipoRecurso);
		
	}

}
