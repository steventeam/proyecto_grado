package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.TipoIdentificacionHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoIdentificacionNegocio {
	
	public void crear(SegTipoIdentificacion tipoIdentificacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoIdentificacionHibernateDAO tipoIdentificacionDAO = daoFactory.obtenerTipoIdentificacionDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_CREACION_NEGOCIO", tipoIdentificacion.getNvNombre());
				
				if(tipoIdentificacionDAO.consultarPorFiltro(tipoIdentificacion).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION_YA_EXISTE", tipoIdentificacion.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerTipoIdentificacionDAO().crear(tipoIdentificacion);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CREACION_NEGOCIO", tipoIdentificacion.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_IDENTIFICACION_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegTipoIdentificacion tipoIdentificacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoIdentificacionHibernateDAO tipoIdentificacionDAO = daoFactory.obtenerTipoIdentificacionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ACTUALIZACION_NEGOCIO", tipoIdentificacion.getNvNombre());
			
			if(tipoIdentificacionDAO.consultarPorCodigo(tipoIdentificacion) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION_NO_EXISTE", tipoIdentificacion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoIdentificacionDAO().actualizarConMerge(tipoIdentificacion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ACTUALIZACION_NEGOCIO", tipoIdentificacion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegTipoIdentificacion tipoIdentificacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoIdentificacionHibernateDAO tipoIdentificacionDAO = daoFactory.obtenerTipoIdentificacionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ELIMINACION_NEGOCIO", tipoIdentificacion.getNvNombre());
			
			if(tipoIdentificacionDAO.consultarPorFiltro(tipoIdentificacion).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION_NO_EXISTE", tipoIdentificacion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoIdentificacionDAO().inhabilitarConMerge(tipoIdentificacion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ELIMINACION_NEGOCIO", tipoIdentificacion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_IDENTIFICACION_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegTipoIdentificacion consultarPorCodigo(SegTipoIdentificacion tipoIdentificacion, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerTipoIdentificacionDAO().consultarPorCodigo(tipoIdentificacion);
		
	}
	
	/*public ArrayList<SegTipoIdentificacion> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoIdentificacionDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegTipoIdentificacion> consultarPorFiltro(SegTipoIdentificacion tipoIdentificacion, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoIdentificacionDAO().consultarPorFiltro(tipoIdentificacion);
		
	}

}
