package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PreguntaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegPregunta;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PreguntaNegocio {
	
	public void crear(SegPregunta pregunta, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaHibernateDAO preguntaDAO = daoFactory.obtenerPreguntaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_CREACION_NEGOCIO");
				
				if(preguntaDAO.consultarPorFiltro(pregunta).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPreguntaDAO().crear(pregunta);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegPregunta pregunta, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaHibernateDAO preguntaDAO = daoFactory.obtenerPreguntaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(preguntaDAO.consultarPorCodigo(pregunta) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPreguntaDAO().actualizarConMerge(pregunta);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegPregunta pregunta, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaHibernateDAO preguntaDAO = daoFactory.obtenerPreguntaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_ANTES_ELIMINACION_NEGOCIO");
			
			if(preguntaDAO.consultarPorFiltro(pregunta).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPreguntaDAO().inhabilitarConMerge(pregunta);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegPregunta consultarPorCodigo(SegPregunta pregunta, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPreguntaDAO().consultarPorCodigo(pregunta);
		
	}
	
	/*public ArrayList<SegPregunta> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPreguntaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegPregunta> consultarPorFiltro(SegPregunta pregunta, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPreguntaDAO().consultarPorFiltro(pregunta);
		
	}

}
