package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.RecursoPerfilHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecursoPerfil;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class RecursoPerfilNegocio {
	
	public void crear(SegRecursoPerfil recursoPerfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoPerfilHibernateDAO recursoPerfilDAO = daoFactory.obtenerRecursoPerfilDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_CREACION_NEGOCIO");
				
				if(recursoPerfilDAO.consultarPorFiltro(recursoPerfil).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerRecursoPerfilDAO().crear(recursoPerfil);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_PERFIL_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegRecursoPerfil recursoPerfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoPerfilHibernateDAO recursoPerfilDAO = daoFactory.obtenerRecursoPerfilDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(recursoPerfilDAO.consultarPorCodigo(recursoPerfil) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerRecursoPerfilDAO().actualizarConMerge(recursoPerfil);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_PERFIL_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegRecursoPerfil recursoPerfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoPerfilHibernateDAO recursoPerfilDAO = daoFactory.obtenerRecursoPerfilDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_ANTES_ELIMINACION_NEGOCIO");
			
			if(recursoPerfilDAO.consultarPorFiltro(recursoPerfil).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerRecursoPerfilDAO().inhabilitarConMerge(recursoPerfil);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_PERFIL_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_PERFIL_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegRecursoPerfil consultarPorCodigo(SegRecursoPerfil recursoPerfil, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerRecursoPerfilDAO().consultarPorCodigo(recursoPerfil);
		
	}
	
	/*public ArrayList<SegRecursoPerfil> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerRecursoPerfilDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegRecursoPerfil> consultarPorFiltro(SegRecursoPerfil recursoPerfil, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerRecursoPerfilDAO().consultarPorFiltro(recursoPerfil);
		
	}

}
