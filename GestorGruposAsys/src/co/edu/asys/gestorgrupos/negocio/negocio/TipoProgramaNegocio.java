package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.TipoProgramaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPrograma;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoProgramaNegocio {
	
	public void crear(TblTipoPrograma tipoPrograma, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoProgramaHibernateDAO tipoProgramaDAO = daoFactory.obtenerTipoProgramaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_CREACION_NEGOCIO", tipoPrograma.getNvNombre());
				
				if(tipoProgramaDAO.consultarPorFiltro(tipoPrograma).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION_YA_EXISTE", tipoPrograma.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerTipoProgramaDAO().crear(tipoPrograma);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_CREACION_NEGOCIO", tipoPrograma.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PROGRAMA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblTipoPrograma tipoPrograma, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoProgramaHibernateDAO tipoProgramaDAO = daoFactory.obtenerTipoProgramaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_ACTUALIZACION_NEGOCIO", tipoPrograma.getNvNombre());
			
			if(tipoProgramaDAO.consultarPorCodigo(tipoPrograma) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION_NO_EXISTE", tipoPrograma.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoProgramaDAO().actualizarConMerge(tipoPrograma);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_ACTUALIZACION_NEGOCIO", tipoPrograma.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PROGRAMA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblTipoPrograma tipoPrograma, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoProgramaHibernateDAO tipoProgramaDAO = daoFactory.obtenerTipoProgramaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_ANTES_ELIMINACION_NEGOCIO", tipoPrograma.getNvNombre());
			
			if(tipoProgramaDAO.consultarPorFiltro(tipoPrograma).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION_NO_EXISTE", tipoPrograma.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoProgramaDAO().inhabilitarConMerge(tipoPrograma);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PROGRAMA_DESPUES_ELIMINACION_NEGOCIO", tipoPrograma.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_PROGRAMA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblTipoPrograma consultarPorCodigo(TblTipoPrograma tipoPrograma, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerTipoProgramaDAO().consultarPorCodigo(tipoPrograma);
		
	}
	
	/*public ArrayList<TblTipoPrograma> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoProgramaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblTipoPrograma> consultarPorFiltro(TblTipoPrograma tipoPrograma, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoProgramaDAO().consultarPorFiltro(tipoPrograma);
		
	}

}
