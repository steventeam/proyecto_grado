package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.GrupoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class GrupoNegocio {
	
	public void crear(TblGrupo grupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		GrupoHibernateDAO grupoDAO = daoFactory.obtenerGrupoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_CREACION_NEGOCIO");
				
				if(grupoDAO.consultarPorFiltro(grupo).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerGrupoDAO().crear(grupo);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_GRUPO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblGrupo grupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		GrupoHibernateDAO grupoDAO = daoFactory.obtenerGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(grupoDAO.consultarPorCodigo(grupo) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerGrupoDAO().actualizarConMerge(grupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_GRUPO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblGrupo grupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		GrupoHibernateDAO grupoDAO = daoFactory.obtenerGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_ANTES_ELIMINACION_NEGOCIO");
			
			if(grupoDAO.consultarPorFiltro(grupo).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerGrupoDAO().inhabilitarConMerge(grupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_GRUPO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_GRUPO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblGrupo consultarPorCodigo(TblGrupo grupo, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerGrupoDAO().consultarPorCodigo(grupo);
		
	}
	
	/*public ArrayList<TblGrupo> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerGrupoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblGrupo> consultarPorFiltro(TblGrupo grupo, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerGrupoDAO().consultarPorFiltro(grupo);
		
	}
	
	public ArrayList<TblGrupo> consultarPorEstudianteGrupo(TblEstudianteXGrupo estudianteXGrupo, HibernateDAOFactory daoFactory) throws Exception {
		return daoFactory.obtenerGrupoDAO().consultarPorEstudianteGrupo(estudianteXGrupo);

}

}
