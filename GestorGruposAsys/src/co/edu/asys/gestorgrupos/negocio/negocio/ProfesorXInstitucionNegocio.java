package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.ProfesorXInstitucionHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblProfesorXInstitucion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class ProfesorXInstitucionNegocio {
	
	public void crear(TblProfesorXInstitucion profesorXInstitucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProfesorXInstitucionHibernateDAO profesorXInstitucionDAO = daoFactory.obtenerProfesorXInstitucionDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_CREACION_NEGOCIO");
				
				if(profesorXInstitucionDAO.consultarPorFiltro(profesorXInstitucion).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerProfesorXInstitucionDAO().crear(profesorXInstitucion);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROFESOR_X_INSTITUCION_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblProfesorXInstitucion profesorXInstitucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProfesorXInstitucionHibernateDAO profesorXInstitucionDAO = daoFactory.obtenerProfesorXInstitucionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(profesorXInstitucionDAO.consultarPorCodigo(profesorXInstitucion) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerProfesorXInstitucionDAO().actualizarConMerge(profesorXInstitucion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblProfesorXInstitucion profesorXInstitucion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProfesorXInstitucionHibernateDAO profesorXInstitucionDAO = daoFactory.obtenerProfesorXInstitucionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ELIMINACION_NEGOCIO");
			
			if(profesorXInstitucionDAO.consultarPorFiltro(profesorXInstitucion).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerProfesorXInstitucionDAO().inhabilitarConMerge(profesorXInstitucion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblProfesorXInstitucion consultarPorCodigo(TblProfesorXInstitucion profesorXInstitucion, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerProfesorXInstitucionDAO().consultarPorCodigo(profesorXInstitucion);
		
	}
	
	/*public ArrayList<TblProfesorXInstitucion> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerProfesorXInstitucionDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblProfesorXInstitucion> consultarPorFiltro(TblProfesorXInstitucion profesorXInstitucion, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerProfesorXInstitucionDAO().consultarPorFiltro(profesorXInstitucion);
		
	}

}
