package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.MateriaXPensumHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateriaXPensum;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class MateriaXPensumNegocio {
	
	public void crear(TblMateriaXPensum materiaXPensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaXPensumHibernateDAO materiaXPensumDAO = daoFactory.obtenerMateriaXPensumDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_CREACION_NEGOCIO");
				
				if(materiaXPensumDAO.consultarPorFiltro(materiaXPensum).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerMateriaXPensumDAO().crear(materiaXPensum);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_X_PENSUM_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblMateriaXPensum materiaXPensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaXPensumHibernateDAO materiaXPensumDAO = daoFactory.obtenerMateriaXPensumDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(materiaXPensumDAO.consultarPorCodigo(materiaXPensum) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerMateriaXPensumDAO().actualizarConMerge(materiaXPensum);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblMateriaXPensum materiaXPensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaXPensumHibernateDAO materiaXPensumDAO = daoFactory.obtenerMateriaXPensumDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ELIMINACION_NEGOCIO");
			
			if(materiaXPensumDAO.consultarPorFiltro(materiaXPensum).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerMateriaXPensumDAO().inhabilitarConMerge(materiaXPensum);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_X_PENSUM_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblMateriaXPensum consultarPorCodigo(TblMateriaXPensum materiaXPensum, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerMateriaXPensumDAO().consultarPorCodigo(materiaXPensum);
		
	}
	
	/*public ArrayList<TblMateriaXPensum> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerMateriaXPensumDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblMateriaXPensum> consultarPorFiltro(TblMateriaXPensum materiaXPensum, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerMateriaXPensumDAO().consultarPorFiltro(materiaXPensum);
		
	}
	
	public ArrayList<TblMateriaXPensum> consultarPorGrupo(TblGrupo grupo, HibernateDAOFactory daoFactory) throws Exception {
	    return daoFactory.obtenerMateriaXPensumDAO().consultarPorGrupo(grupo);
	  }

}
