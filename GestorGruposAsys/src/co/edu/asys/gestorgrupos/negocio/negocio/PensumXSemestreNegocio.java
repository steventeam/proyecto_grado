package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PensumXSemestreHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensumXSemestre;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PensumXSemestreNegocio {
	
	public void crear(TblPensumXSemestre pensumXSemestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumXSemestreHibernateDAO pensumXSemestreDAO = daoFactory.obtenerPensumXSemestreDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_CREACION_NEGOCIO");
				
				if(pensumXSemestreDAO.consultarPorFiltro(pensumXSemestre).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPensumXSemestreDAO().crear(pensumXSemestre);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_X_SEMESTRE_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblPensumXSemestre pensumXSemestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumXSemestreHibernateDAO pensumXSemestreDAO = daoFactory.obtenerPensumXSemestreDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(pensumXSemestreDAO.consultarPorCodigo(pensumXSemestre) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPensumXSemestreDAO().actualizarConMerge(pensumXSemestre);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblPensumXSemestre pensumXSemestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumXSemestreHibernateDAO pensumXSemestreDAO = daoFactory.obtenerPensumXSemestreDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ELIMINACION_NEGOCIO");
			
			if(pensumXSemestreDAO.consultarPorFiltro(pensumXSemestre).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPensumXSemestreDAO().inhabilitarConMerge(pensumXSemestre);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_X_SEMESTRE_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblPensumXSemestre consultarPorCodigo(TblPensumXSemestre pensumXSemestre, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPensumXSemestreDAO().consultarPorCodigo(pensumXSemestre);
		
	}
	
	/*public ArrayList<TblPensumXSemestre> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPensumXSemestreDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblPensumXSemestre> consultarPorFiltro(TblPensumXSemestre pensumXSemestre, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPensumXSemestreDAO().consultarPorFiltro(pensumXSemestre);
		
	}

}
