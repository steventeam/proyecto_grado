package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.SemestreHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSemestre;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class SemestreNegocio {
	
	public void crear(TblSemestre semestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SemestreHibernateDAO semestreDAO = daoFactory.obtenerSemestreDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_CREACION_NEGOCIO", semestre.getNvNombre());
				
				if(semestreDAO.consultarPorFiltro(semestre).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION_YA_EXISTE", semestre.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerSemestreDAO().crear(semestre);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_CREACION_NEGOCIO", semestre.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SEMESTRE_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblSemestre semestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SemestreHibernateDAO semestreDAO = daoFactory.obtenerSemestreDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_ACTUALIZACION_NEGOCIO", semestre.getNvNombre());
			
			if(semestreDAO.consultarPorCodigo(semestre) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION_NO_EXISTE", semestre.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerSemestreDAO().actualizarConMerge(semestre);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_ACTUALIZACION_NEGOCIO", semestre.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SEMESTRE_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblSemestre semestre, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		SemestreHibernateDAO semestreDAO = daoFactory.obtenerSemestreDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_ANTES_ELIMINACION_NEGOCIO", semestre.getNvNombre());
			
			if(semestreDAO.consultarPorFiltro(semestre).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION_NO_EXISTE", semestre.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerSemestreDAO().inhabilitarConMerge(semestre);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SEMESTRE_DESPUES_ELIMINACION_NEGOCIO", semestre.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_SEMESTRE_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	public TblSemestre consultarPorCodigo(TblSemestre semestre, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerSemestreDAO().consultarPorCodigo(semestre);
		
	}
	
	/*public ArrayList<TblSemestre> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerSemestreDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblSemestre> consultarPorFiltro(TblSemestre semestre, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerSemestreDAO().consultarPorFiltro(semestre);
		
	}

}
