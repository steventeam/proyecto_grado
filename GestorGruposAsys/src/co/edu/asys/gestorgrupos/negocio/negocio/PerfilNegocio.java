package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PerfilHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegPerfil;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PerfilNegocio {
	
	public void crear(SegPerfil perfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PerfilHibernateDAO perfilDAO = daoFactory.obtenerPerfilDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_CREACION_NEGOCIO", perfil.getNvNombre());
				
				if(perfilDAO.consultarPorFiltro(perfil).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION_YA_EXISTE", perfil.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPerfilDAO().crear(perfil);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_CREACION_NEGOCIO", perfil.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERFIL_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegPerfil perfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PerfilHibernateDAO perfilDAO = daoFactory.obtenerPerfilDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_ACTUALIZACION_NEGOCIO", perfil.getNvNombre());
			
			if(perfilDAO.consultarPorCodigo(perfil) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION_NO_EXISTE", perfil.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPerfilDAO().actualizarConMerge(perfil);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_ACTUALIZACION_NEGOCIO", perfil.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERFIL_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegPerfil perfil, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PerfilHibernateDAO perfilDAO = daoFactory.obtenerPerfilDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_ANTES_ELIMINACION_NEGOCIO", perfil.getNvNombre());
			
			if(perfilDAO.consultarPorFiltro(perfil).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION_NO_EXISTE", perfil.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPerfilDAO().inhabilitarConMerge(perfil);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERFIL_DESPUES_ELIMINACION_NEGOCIO", perfil.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERFIL_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegPerfil consultarPorCodigo(SegPerfil perfil, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPerfilDAO().consultarPorCodigo(perfil);
		
	}
	
	/*public ArrayList<SegPerfil> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPerfilDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegPerfil> consultarPorFiltro(SegPerfil perfil, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPerfilDAO().consultarPorFiltro(perfil);
		
	}

}
