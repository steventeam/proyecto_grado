package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.MateriaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateria;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class MateriaNegocio {
	
	public void crear(TblMateria materia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaHibernateDAO materiaDAO = daoFactory.obtenerMateriaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_CREACION_NEGOCIO", materia.getNvNombre());
				
				if(materiaDAO.consultarPorFiltro(materia).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION_YA_EXISTE", materia.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerMateriaDAO().crear(materia);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_CREACION_NEGOCIO", materia.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblMateria materia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaHibernateDAO materiaDAO = daoFactory.obtenerMateriaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_ACTUALIZACION_NEGOCIO", materia.getNvNombre());
			
			if(materiaDAO.consultarPorCodigo(materia) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION_NO_EXISTE", materia.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerMateriaDAO().actualizarConMerge(materia);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_ACTUALIZACION_NEGOCIO", materia.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblMateria materia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		MateriaHibernateDAO materiaDAO = daoFactory.obtenerMateriaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_ANTES_ELIMINACION_NEGOCIO", materia.getNvNombre());
			
			if(materiaDAO.consultarPorFiltro(materia) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION_NO_EXISTE", materia.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerMateriaDAO().inhabilitarConMerge(materia);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_DESPUES_ELIMINACION_NEGOCIO", materia.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_MATERIA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblMateria consultarPorCodigo(TblMateria materia, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerMateriaDAO().consultarPorCodigo(materia);
		
	}
	
	/*public ArrayList<TblMateria> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerMateriaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblMateria> consultarPorFiltro(TblMateria materia, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerMateriaDAO().consultarPorFiltro(materia);
		
	}

}
