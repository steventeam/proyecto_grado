package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PeriodoAcademicoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPeriodoAcademico;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PeriodoAcademicoNegocio {
	
	public void crear(TblPeriodoAcademico periodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PeriodoAcademicoHibernateDAO periodoAcademicoDAO = daoFactory.obtenerPeriodoAcademicoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_CREACION_NEGOCIO");
				
				if(periodoAcademicoDAO.consultarPorFiltro(periodoAcademico).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPeriodoAcademicoDAO().crear(periodoAcademico);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERIODO_ACADEMICO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblPeriodoAcademico periodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PeriodoAcademicoHibernateDAO periodoAcademicoDAO = daoFactory.obtenerPeriodoAcademicoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(periodoAcademicoDAO.consultarPorCodigo(periodoAcademico) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPeriodoAcademicoDAO().actualizarConMerge(periodoAcademico);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblPeriodoAcademico periodoAcademico, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PeriodoAcademicoHibernateDAO periodoAcademicoDAO = daoFactory.obtenerPeriodoAcademicoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ELIMINACION_NEGOCIO");
			
			if(periodoAcademicoDAO.consultarPorFiltro(periodoAcademico).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPeriodoAcademicoDAO().inhabilitarConMerge(periodoAcademico);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PERIODO_ACADEMICO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblPeriodoAcademico consultarPorCodigo(TblPeriodoAcademico periodoAcademico, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPeriodoAcademicoDAO().consultarPorCodigo(periodoAcademico);
		
	}
	
	/*public ArrayList<TblPeriodoAcademico> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPeriodoAcademicoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblPeriodoAcademico> consultarPorFiltro(TblPeriodoAcademico periodoAcademico, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPeriodoAcademicoDAO().consultarPorFiltro(periodoAcademico);
		
	}

}
