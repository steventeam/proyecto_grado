package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.GeneroHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;

@Service
@Scope("prototype")
public class GeneroNegocio {
	
	public void crear(TblGenero genero, HibernateDAOFactory daoFactory) throws Exception {
		GeneroHibernateDAO generoDAO = daoFactory.obtenerGeneroDAO();	
		
		if(generoDAO.consultarPorFiltro(genero).size() > 0) {
			throw new Exception("El g�nero que quiere registrar ya existe.");
		}
		daoFactory.obtenerGeneroDAO().crear(genero);
	}
	
	public void actualizar(TblGenero genero, HibernateDAOFactory daoFactory) throws Exception {
		GeneroHibernateDAO generoDAO = daoFactory.obtenerGeneroDAO();	
		
		if(generoDAO.consultarPorCodigo(genero) == null) {
			throw new Exception("El g�nero que quiere actualizar no existe.");
		}
		daoFactory.obtenerGeneroDAO().actualizarConMerge(genero);
	}
	
	//@transactional
	public void inhabilitar(TblGenero genero, HibernateDAOFactory daoFactory) throws Exception {
		GeneroHibernateDAO generoDAO = daoFactory.obtenerGeneroDAO();	
		
		if(generoDAO.consultarPorFiltro(genero).isEmpty()) {
			throw new Exception("El g�nero que quiere inhabilitar no existe.");
		}
		daoFactory.obtenerGeneroDAO().inhabilitarConMerge(genero);
	}
	
	public TblGenero consultarPorCodigo(TblGenero genero, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerGeneroDAO().consultarPorCodigo(genero);
		
	}
	
	/*public ArrayList<TblGenero> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerGeneroDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblGenero> consultarPorFiltro(TblGenero genero, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerGeneroDAO().consultarPorFiltro(genero);
		
	}

}
