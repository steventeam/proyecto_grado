package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.TipoNotaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoNota;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoNotaNegocio {
	
	public void crear(TblTipoNota tipoNota, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoNotaHibernateDAO tipoNotaDAO = daoFactory.obtenerTipoNotaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_CREACION_NEGOCIO", tipoNota.getNvNombre());
				
				if(tipoNotaDAO.consultarPorFiltro(tipoNota).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_CREACION_YA_EXISTE", tipoNota.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerTipoNotaDAO().crear(tipoNota);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_CREACION_NEGOCIO", tipoNota.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_NOTA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblTipoNota tipoNota, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoNotaHibernateDAO tipoNotaDAO = daoFactory.obtenerTipoNotaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_ACTUALIZACION_NEGOCIO", tipoNota.getNvNombre());
			
			if(tipoNotaDAO.consultarPorCodigo(tipoNota) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ACTUALIZACION_NO_EXISTE", tipoNota.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoNotaDAO().actualizarConMerge(tipoNota);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_ACTUALIZACION_NEGOCIO", tipoNota.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_NOTA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblTipoNota tipoNota, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		TipoNotaHibernateDAO tipoNotaDAO = daoFactory.obtenerTipoNotaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_ANTES_ELIMINACION_NEGOCIO", tipoNota.getNvNombre());
			
			if(tipoNotaDAO.consultarPorFiltro(tipoNota).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ELIMINACION_NO_EXISTE", tipoNota.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerTipoNotaDAO().inhabilitarConMerge(tipoNota);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_NOTA_DESPUES_ELIMINACION_NEGOCIO", tipoNota.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_NOTA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_NOTA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblTipoNota consultarPorCodigo(TblTipoNota tipoNota, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerTipoNotaDAO().consultarPorCodigo(tipoNota);
		
	}
	
	/*public ArrayList<TblTipoNota> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoNotaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblTipoNota> consultarPorFiltro(TblTipoNota tipoNota, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerTipoNotaDAO().consultarPorFiltro(tipoNota);
		
	}

}
