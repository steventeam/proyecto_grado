package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PensumHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensum;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PensumNegocio {
	
	public void crear(TblPensum pensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumHibernateDAO pensumDAO = daoFactory.obtenerPensumDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_CREACION_NEGOCIO", pensum.getNvNombre());
				
				if(pensumDAO.consultarPorFiltro(pensum).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION_YA_EXISTE", pensum.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPensumDAO().crear(pensum);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_CREACION_NEGOCIO", pensum.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblPensum pensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumHibernateDAO pensumDAO = daoFactory.obtenerPensumDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_ACTUALIZACION_NEGOCIO", pensum.getNvNombre());
			
			if(pensumDAO.consultarPorCodigo(pensum) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION_NO_EXISTE", pensum.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPensumDAO().actualizarConMerge(pensum);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_ACTUALIZACION_NEGOCIO", pensum.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblPensum pensum, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PensumHibernateDAO pensumDAO = daoFactory.obtenerPensumDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_ANTES_ELIMINACION_NEGOCIO", pensum.getNvNombre());
			
			if(pensumDAO.consultarPorFiltro(pensum).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION_NO_EXISTE", pensum.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPensumDAO().inhabilitarConMerge(pensum);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_DESPUES_ELIMINACION_NEGOCIO", pensum.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PENSUM_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblPensum consultarPorCodigo(TblPensum pensum, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPensumDAO().consultarPorCodigo(pensum);
		
	}
	
	/*public ArrayList<TblPensum> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPensumDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblPensum> consultarPorFiltro(TblPensum pensum, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPensumDAO().consultarPorFiltro(pensum);
		
	}

}
