package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.NotaXGrupoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXGrupo;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class NotaXGrupoNegocio {
	
	public void crear(TblNotaXGrupo notaXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXGrupoHibernateDAO notaXGrupoDAO = daoFactory.obtenerNotaXGrupoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_CREACION_NEGOCIO");
				
				if(notaXGrupoDAO.consultarPorFiltro(notaXGrupo).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerNotaXGrupoDAO().crear(notaXGrupo);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_GRUPO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblNotaXGrupo notaXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXGrupoHibernateDAO notaXGrupoDAO = daoFactory.obtenerNotaXGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(notaXGrupoDAO.consultarPorCodigo(notaXGrupo) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerNotaXGrupoDAO().actualizarConMerge(notaXGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_GRUPO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblNotaXGrupo notaXGrupo, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		NotaXGrupoHibernateDAO notaXGrupoDAO = daoFactory.obtenerNotaXGrupoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_ANTES_ELIMINACION_NEGOCIO");
			
			if(notaXGrupoDAO.consultarPorFiltro(notaXGrupo).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerNotaXGrupoDAO().inhabilitarConMerge(notaXGrupo);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_GRUPO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_NOTA_X_GRUPO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblNotaXGrupo consultarPorCodigo(TblNotaXGrupo notaXGrupo, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerNotaXGrupoDAO().consultarPorCodigo(notaXGrupo);
		
	}
	
	/*public ArrayList<TblNotaXGrupo> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerNotaXGrupoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblNotaXGrupo> consultarPorFiltro(TblNotaXGrupo notaXGrupo, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerNotaXGrupoDAO().consultarPorFiltro(notaXGrupo);
		
	}

}
