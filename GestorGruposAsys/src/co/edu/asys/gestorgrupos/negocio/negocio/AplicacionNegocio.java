package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.AplicacionHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class AplicacionNegocio {
	
	public void crear(SegAplicacion aplicacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AplicacionHibernateDAO aplicacionDAO = daoFactory.obtenerAplicacionDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_CREACION_NEGOCIO", aplicacion.getNvNombre());
				
				if(aplicacionDAO.consultarPorFiltro(aplicacion).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION_YA_EXISTE", aplicacion.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerAplicacionDAO().crear(aplicacion);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CREACION_NEGOCIO", aplicacion.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_APLICACION_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegAplicacion aplicacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AplicacionHibernateDAO aplicacionDAO = daoFactory.obtenerAplicacionDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_NEGOCIO", aplicacion.getNvNombre());
			
			if(aplicacionDAO.consultarPorCodigo(aplicacion) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION_NO_EXISTE", aplicacion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerAplicacionDAO().actualizarConMerge(aplicacion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_NEGOCIO", aplicacion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_APLICACION_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegAplicacion aplicacion, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AplicacionHibernateDAO aplicacionDAO = daoFactory.obtenerAplicacionDAO();	
		
		
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ELIMINACION_NEGOCIO", aplicacion.getNvNombre());
			
			if(aplicacionDAO.consultarPorFiltro(aplicacion).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION_NO_EXISTE", aplicacion.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerAplicacionDAO().inhabilitarConMerge(aplicacion);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ELIMINACION_NEGOCIO", aplicacion.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_APLICACION_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegAplicacion consultarPorCodigo(SegAplicacion aplicacion, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerAplicacionDAO().consultarPorCodigo(aplicacion);
		
	}
	
	/*public ArrayList<SegAplicacion> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerAplicacionDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegAplicacion> consultarPorFiltro(SegAplicacion aplicacion, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerAplicacionDAO().consultarPorFiltro(aplicacion);
		
	}

}
