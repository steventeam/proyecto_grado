package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.ProgramaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class ProgramaNegocio {
	
	public void crear(TblPrograma programa, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProgramaHibernateDAO programaDAO = daoFactory.obtenerProgramaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_CREACION_NEGOCIO", programa.getNvNombre());
				
				if(programaDAO.consultarPorFiltro(programa).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION_YA_EXISTE", programa.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerProgramaDAO().crear(programa);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CREACION_NEGOCIO", programa.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROGRAMA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblPrograma programa, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProgramaHibernateDAO programaDAO = daoFactory.obtenerProgramaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ACTUALIZACION_NEGOCIO", programa.getNvNombre());
			
			if(programaDAO.consultarPorCodigo(programa) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION_NO_EXISTE", programa.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerProgramaDAO().actualizarConMerge(programa);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ACTUALIZACION_NEGOCIO", programa.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROGRAMA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblPrograma programa, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		ProgramaHibernateDAO programaDAO = daoFactory.obtenerProgramaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ELIMINACION_NEGOCIO", programa.getNvNombre());
			
			if(programaDAO.consultarPorFiltro(programa).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION_NO_EXISTE", programa.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerProgramaDAO().inhabilitarConMerge(programa);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ELIMINACION_NEGOCIO", programa.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PROGRAMA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblPrograma consultarPorCodigo(TblPrograma programa, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerProgramaDAO().consultarPorCodigo(programa);
		
	}
	
	/*public ArrayList<TblPrograma> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerProgramaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblPrograma> consultarPorFiltro(TblPrograma programa, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerProgramaDAO().consultarPorFiltro(programa);
		
	}

}
