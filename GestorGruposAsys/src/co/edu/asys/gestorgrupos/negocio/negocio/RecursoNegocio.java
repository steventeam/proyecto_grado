package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.RecursoHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecurso;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class RecursoNegocio {
	
	public void crear(SegRecurso recurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoHibernateDAO recursoDAO = daoFactory.obtenerRecursoDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_CREACION_NEGOCIO", recurso.getNvNombre());
				
				if(recursoDAO.consultarPorFiltro(recurso).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION_YA_EXISTE", recurso.getNvNombre());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerRecursoDAO().crear(recurso);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_CREACION_NEGOCIO", recurso.getNvNombre());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegRecurso recurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoHibernateDAO recursoDAO = daoFactory.obtenerRecursoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_ACTUALIZACION_NEGOCIO", recurso.getNvNombre());
			
			if(recursoDAO.consultarPorCodigo(recurso) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION_NO_EXISTE", recurso.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerRecursoDAO().actualizarConMerge(recurso);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_ACTUALIZACION_NEGOCIO", recurso.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegRecurso recurso, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		RecursoHibernateDAO recursoDAO = daoFactory.obtenerRecursoDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_ANTES_ELIMINACION_NEGOCIO", recurso.getNvNombre());
			
			if(recursoDAO.consultarPorFiltro(recurso).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION_NO_EXISTE", recurso.getNvNombre());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerRecursoDAO().inhabilitarConMerge(recurso);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_RECURSO_DESPUES_ELIMINACION_NEGOCIO", recurso.getNvNombre());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_RECURSO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public void habilitar(SegRecurso recurso, HibernateDAOFactory daoFactory) throws Exception {
		RecursoHibernateDAO recursoDAO = daoFactory.obtenerRecursoDAO();	
		
				
		if(recursoDAO.consultarPorCodigo(recurso) == null) {
			throw new Exception("No es posible habilitar el recurso.");
		}
		daoFactory.obtenerRecursoDAO().habilitar(recurso);
	}
	
	public SegRecurso consultarPorCodigo(SegRecurso recurso, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerRecursoDAO().consultarPorCodigo(recurso);
		
	}
	
	/*public ArrayList<SegRecurso> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerRecursoDAO().consultarTodo();
		
	}*/
	
	public ArrayList<SegRecurso> consultarPorFiltro(SegRecurso recurso, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerRecursoDAO().consultarPorFiltro(recurso);
		
	}

}
