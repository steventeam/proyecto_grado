package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.PreguntaUsuarioHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegPreguntaUsuario;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PreguntaUsuarioNegocio {
	
	public void crear(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaUsuarioHibernateDAO preguntaUsuarioDAO = daoFactory.obtenerPreguntaUsuarioDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_CREACION_NEGOCIO");
				
				if(preguntaUsuarioDAO.consultarPorFiltro(preguntaUsuario).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerPreguntaUsuarioDAO().crear(preguntaUsuario);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_USUARIO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaUsuarioHibernateDAO preguntaUsuarioDAO = daoFactory.obtenerPreguntaUsuarioDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(preguntaUsuarioDAO.consultarPorCodigo(preguntaUsuario) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPreguntaUsuarioDAO().actualizarConMerge(preguntaUsuario);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		PreguntaUsuarioHibernateDAO preguntaUsuarioDAO = daoFactory.obtenerPreguntaUsuarioDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ELIMINACION_NEGOCIO");
			
			if(preguntaUsuarioDAO.consultarPorFiltro(preguntaUsuario).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerPreguntaUsuarioDAO().inhabilitarConMerge(preguntaUsuario);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_PREGUNTA_USUARIO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public SegPreguntaUsuario consultarPorCodigo(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerPreguntaUsuarioDAO().consultarPorCodigo(preguntaUsuario);
		
	}
	
	/*public ArrayList<SegPreguntaUsuario> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPreguntaUsuarioDAO().consultarTodo();
		
	}*/
	
	public void recuperar(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws Exception {
		PreguntaUsuarioHibernateDAO preguntaUsuarioDAO = daoFactory.obtenerPreguntaUsuarioDAO();

		if (preguntaUsuarioDAO.consultarPorFiltro(preguntaUsuario).size() == 0) {
			throw new Exception("Datos Invalidos");
		}

		daoFactory.obtenerPreguntaUsuarioDAO().recuperar(preguntaUsuario);
	}
	
	public ArrayList<SegPreguntaUsuario> consultarPorFiltro(SegPreguntaUsuario preguntaUsuario, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerPreguntaUsuarioDAO().consultarPorFiltro(preguntaUsuario);
		
	}

}
