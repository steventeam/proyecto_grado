package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.UsuarioHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class UsuarioNegocio {
	
	
	
	
	public void crear(SegUsuario usuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_CREACION_NEGOCIO", usuario.getNvNombreUsuario());
				
				if(usuarioDAO.consultarPorFiltro(usuario).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION_YA_EXISTE", usuario.getNvNombreUsuario());
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerUsuarioDAO().crear(usuario);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CREACION_NEGOCIO", usuario.getNvNombreUsuario());
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_USUARIO_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(SegUsuario usuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ACTUALIZACION_NEGOCIO", usuario.getNvNombreUsuario());
			
			if(usuarioDAO.consultarPorCodigo(usuario) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION_NO_EXISTE", usuario.getNvNombreUsuario());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerUsuarioDAO().actualizarConMerge(usuario);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ACTUALIZACION_NEGOCIO", usuario.getNvNombreUsuario());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_USUARIO_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(SegUsuario usuario, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ELIMINACION_NEGOCIO", usuario.getNvNombreUsuario());
			
			if(usuarioDAO.consultarPorFiltro(usuario).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION_NO_EXISTE", usuario.getNvNombreUsuario());
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerUsuarioDAO().inhabilitarConMerge(usuario);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ELIMINACION_NEGOCIO", usuario.getNvNombreUsuario());
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_USUARIO_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	
	
	public void actualizar2(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();

		if (usuarioDAO.consultarPorFiltro(usuario) == null) {
			throw new Exception("El usuario que quiere actualizar NO existe");
		}

		daoFactory.obtenerUsuarioDAO().actualizar2(usuario);
	}
	
	public void habilitar(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();	
		
				
		if(usuarioDAO.consultarPorCodigo(usuario) == null) {
			throw new Exception("No es posible habilitar el usuario.");
		}
		daoFactory.obtenerUsuarioDAO().habilitar(usuario);
	}
	
	public void enviarClave(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		UsuarioHibernateDAO usuarioDAO = daoFactory.obtenerUsuarioDAO();	
		
				
		if(usuarioDAO.consultarPorCorreo(usuario) == null) {
			throw new Exception("No es posible enviar la clave.");
		}
		daoFactory.obtenerUsuarioDAO().enviarClave(usuario);
	}
	
	public ArrayList<SegUsuario> consultarPorFiltro(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerUsuarioDAO().consultarPorFiltro(usuario);
		
	}
	
	public ArrayList<SegUsuario> consultarPorNumeroId(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerUsuarioDAO().consultarPorNumeroId(usuario);
		
	}
	
	public ArrayList<SegUsuario> consultarPorCorreo(SegUsuario usuario, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerUsuarioDAO().consultarPorCorreo(usuario);
		
	}

}
