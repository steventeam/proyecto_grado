package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.AsistenciaHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblAsistencia;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class AsistenciaNegocio {
	
	public void crear(TblAsistencia asistencia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AsistenciaHibernateDAO asistenciaDAO = daoFactory.obtenerAsistenciaDAO();	
		
		
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_CREACION_NEGOCIO");
				
				if(asistenciaDAO.consultarPorFiltro(asistencia).size() > 0) {
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION_YA_EXISTE");
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				}
					daoFactory.obtenerAsistenciaDAO().crear(asistencia);
					UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CREACION_NEGOCIO");
			
				}catch (ExcepcionAplicacionGrupos excepcion){
					throw excepcion;
			
				}catch (Exception exception){
				
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ASISTENCIA_CREACION");
					
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION");
					String mensajeTecnico = exception.getMessage(); 
					
					UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
					
				
				}
			}
		
	
	public void actualizar(TblAsistencia asistencia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AsistenciaHibernateDAO asistenciaDAO = daoFactory.obtenerAsistenciaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ACTUALIZACION_NEGOCIO");
			
			if(asistenciaDAO.consultarPorCodigo(asistencia) == null) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerAsistenciaDAO().actualizarConMerge(asistencia);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ACTUALIZACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ASISTENCIA_ACTUALIZACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
		
	}
	
	//@transactional
	public void inhabilitar(TblAsistencia asistencia, HibernateDAOFactory daoFactory) throws ExcepcionAplicacionGrupos {
		AsistenciaHibernateDAO asistenciaDAO = daoFactory.obtenerAsistenciaDAO();	
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ELIMINACION_NEGOCIO");
			
			if(asistenciaDAO.consultarPorFiltro(asistencia).isEmpty()) {
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION_NO_EXISTE");
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, null, null, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
			}
				daoFactory.obtenerAsistenciaDAO().inhabilitarConMerge(asistencia);
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ELIMINACION_NEGOCIO");
		
			}catch (ExcepcionAplicacionGrupos excepcion){
				throw excepcion;
		
			}catch (Exception exception){
			
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_ASISTENCIA_ELIMINACION");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION");
				String mensajeTecnico = exception.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, exception, LugarExcepcionEnum.NEGOCIO, false, this.getClass());
				
			
			}
	}
	
	public TblAsistencia consultarPorCodigo(TblAsistencia asistencia, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerAsistenciaDAO().consultarPorCodigo(asistencia);
		
	}
	
	/*public ArrayList<TblAsistencia> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerAsistenciaDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblAsistencia> consultarPorFiltro(TblAsistencia asistencia, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerAsistenciaDAO().consultarPorFiltro(asistencia);
		
	}

}
