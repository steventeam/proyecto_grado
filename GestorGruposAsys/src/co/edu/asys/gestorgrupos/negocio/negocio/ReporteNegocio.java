package co.edu.asys.gestorgrupos.negocio.negocio;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.DAO.ReporteHibernateDAO;
import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblReporte;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;

@Service
@Scope("prototype")
public class ReporteNegocio {
	
	public void crear(TblReporte reporte, HibernateDAOFactory daoFactory) throws Exception {
		ReporteHibernateDAO reporteDAO = daoFactory.obtenerReporteDAO();	
		
		
		try{
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_CREACION_NEGOCIO", reporte.getNvDescripcion());
			
			if(reporteDAO.consultarPorFiltro(reporte).size() > 0) {
				throw new Exception("La reporte que quiere registrar ya existe.");
			}
			daoFactory.obtenerReporteDAO().crear(reporte);
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CREACION_NEGOCIO", reporte.getNvDescripcion());
			}catch (Exception exception){
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, exception, "ERROR_TIPO_IDENTIFICACION_CREACION");
			}
		
		
	}
	
	public void actualizar(TblReporte reporte, HibernateDAOFactory daoFactory) throws Exception {
		ReporteHibernateDAO reporteDAO = daoFactory.obtenerReporteDAO();	
		
		if(reporteDAO.consultarPorCodigo(reporte) == null) {
			throw new Exception("La reporte que quiere actualizar no existe.");
		}
		daoFactory.obtenerReporteDAO().actualizarConMerge(reporte);
	}
	
	//@transactional
	public void inhabilitar(TblReporte reporte, HibernateDAOFactory daoFactory) throws Exception {
		ReporteHibernateDAO reporteDAO = daoFactory.obtenerReporteDAO();	
		
		if(reporteDAO.consultarPorFiltro(reporte).isEmpty()) {
			throw new Exception("La reporte que quiere inhabilitar no existe.");
		}
		daoFactory.obtenerReporteDAO().inhabilitarConMerge(reporte);
	}
	
	public TblReporte consultarPorCodigo(TblReporte reporte, HibernateDAOFactory daoFactory) throws Exception {
				return daoFactory.obtenerReporteDAO().consultarPorCodigo(reporte);
		
	}
	
	/*public ArrayList<TblReporte> consultarTodo(HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerReporteDAO().consultarTodo();
		
	}*/
	
	public ArrayList<TblReporte> consultarPorFiltro(TblReporte reporte, HibernateDAOFactory daoFactory) throws Exception {
		
		return daoFactory.obtenerReporteDAO().consultarPorFiltro(reporte);
		
	}

}
