package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.negocio.negocio.UsuarioNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class UsuarioFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private UsuarioNegocio usuarioNegocio;
	
	public void crear(SegUsuario usuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_CREACION_FACHADA");
			usuarioNegocio.crear(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(SegUsuario usuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ACTUALIZACION_FACHADA");
			usuarioNegocio.actualizar(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar2(SegUsuario usuario) throws Exception {
		try {
			usuarioNegocio.actualizar2(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception exception) {
			daoFactory.cancelarTransaccion();
			throw exception;
		} finally {
			daoFactory.cerrarConexion();
		}

	}
	
	public void habilitar(SegUsuario usuario) throws Exception {
		try {
			usuarioNegocio.habilitar(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception exception) {
			daoFactory.cancelarTransaccion();
			throw exception;
		} finally {
			daoFactory.cerrarConexion();
		}

	}
	
	public void enviarClave(SegUsuario usuario) throws Exception {
		try {
			usuarioNegocio.enviarClave(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception exception) {
			daoFactory.cancelarTransaccion();
			throw exception;
		} finally {
			daoFactory.cerrarConexion();
		}

	}
	
	public void inhabilitar(SegUsuario usuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_ANTES_ELIMINACION_FACHADA");
			usuarioNegocio.inhabilitar(usuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
		
	public ArrayList<SegUsuario> consultarPorFiltro(
			SegUsuario usuario) throws Exception {
		try {
		
			return usuarioNegocio.consultarPorFiltro(usuario, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<SegUsuario> consultarPorNumeroId(SegUsuario usuario) throws Exception {
		try {
		
			return usuarioNegocio.consultarPorNumeroId(usuario, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<SegUsuario> consultarPorCorreo(SegUsuario usuario) throws Exception {
		try {
		
			return usuarioNegocio.consultarPorCorreo(usuario, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
}
