package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensumXSemestre;
import co.edu.asys.gestorgrupos.negocio.negocio.PensumXSemestreNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PensumXSemestreFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private PensumXSemestreNegocio pensumXSemestreNegocio;
	
	public void crear(TblPensumXSemestre pensumXSemestre) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_CREACION_FACHADA");
			pensumXSemestreNegocio.crear(pensumXSemestre, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblPensumXSemestre pensumXSemestre) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ACTUALIZACION_FACHADA");
			pensumXSemestreNegocio.actualizar(pensumXSemestre, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblPensumXSemestre pensumXSemestre) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_ANTES_ELIMINACION_FACHADA");
			pensumXSemestreNegocio.inhabilitar(pensumXSemestre, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_X_SEMESTRE_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PENSUM_X_SEMESTRE_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblPensumXSemestre> consultarPorFiltro(
			TblPensumXSemestre pensumXSemestre) throws Exception {
		try {
		
			return pensumXSemestreNegocio.consultarPorFiltro(pensumXSemestre, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblPensumXSemestre> consultarPorCodigo(TblPensumXSemestre pensumXSemestre) throws Exception {
		try {
		
			return pensumXSemestreNegocio.consultarPorFiltro(pensumXSemestre, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
