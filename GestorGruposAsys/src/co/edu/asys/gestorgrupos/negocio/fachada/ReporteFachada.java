package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblReporte;
import co.edu.asys.gestorgrupos.negocio.negocio.ReporteNegocio;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;

@Service
@Scope("prototype")
public class ReporteFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private ReporteNegocio reporteNegocio;
	
	public void crear(TblReporte reporte) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_CREACION_FACHADA", reporte.getNvDescripcion());
			reporteNegocio.crear(reporte, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CREACION_FACHADA", reporte.getNvDescripcion());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ROLLBACK");
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblReporte reporte) throws Exception {
		try {
			reporteNegocio.actualizar(reporte, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public void inhabilitar(TblReporte reporte) throws Exception {
		try {
			reporteNegocio.inhabilitar(reporte, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblReporte> consultarPorFiltro(
			TblReporte reporte) throws Exception {
		try {
		
			return reporteNegocio.consultarPorFiltro(reporte, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblReporte> consultarPorCodigo(TblReporte reporte) throws Exception {
		try {
		
			return reporteNegocio.consultarPorFiltro(reporte, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
