package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblAsistencia;
import co.edu.asys.gestorgrupos.negocio.negocio.AsistenciaNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class AsistenciaFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private AsistenciaNegocio asistenciaNegocio;
	
	public void crear(TblAsistencia asistencia) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_CREACION_FACHADA");
			asistenciaNegocio.crear(asistencia, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ASISTENCIA_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblAsistencia asistencia) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ACTUALIZACION_FACHADA");
			asistenciaNegocio.actualizar(asistencia, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ASISTENCIA_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblAsistencia asistencia) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_ANTES_ELIMINACION_FACHADA");
			asistenciaNegocio.inhabilitar(asistencia, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ASISTENCIA_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ASISTENCIA_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblAsistencia> consultarPorFiltro(
			TblAsistencia asistencia) throws Exception {
		try {
		
			return asistenciaNegocio.consultarPorFiltro(asistencia, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblAsistencia> consultarPorCodigo(TblAsistencia asistencia) throws Exception {
		try {
		
			return asistenciaNegocio.consultarPorFiltro(asistencia, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
