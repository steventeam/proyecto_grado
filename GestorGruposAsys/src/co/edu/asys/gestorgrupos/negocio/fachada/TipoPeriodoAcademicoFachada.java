package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPeriodoAcademico;
import co.edu.asys.gestorgrupos.negocio.negocio.TipoPeriodoAcademicoNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoPeriodoAcademicoFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private TipoPeriodoAcademicoNegocio tipoPeriodoAcademicoNegocio;
	
	public void crear(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_CREACION_FACHADA");
			tipoPeriodoAcademicoNegocio.crear(tipoPeriodoAcademico, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
			
			try {
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_FACHADA");
				tipoPeriodoAcademicoNegocio.actualizar(tipoPeriodoAcademico, daoFactory);
				daoFactory.confirmarTransaccion();
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_FACHADA");
			} catch (Exception excepcion) {
				daoFactory.cancelarTransaccion();
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
				
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
				String mensajeTecnico = excepcion.getMessage(); 
				
				UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
				throw excepcion;
			} finally {
				daoFactory.cerrarConexion();
				UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
			}
	}
	
	public void inhabilitar(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_ANTES_ELIMINACION_FACHADA");
			tipoPeriodoAcademicoNegocio.inhabilitar(tipoPeriodoAcademico, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblTipoPeriodoAcademico> consultarPorFiltro(
			TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		try {
		
			return tipoPeriodoAcademicoNegocio.consultarPorFiltro(tipoPeriodoAcademico, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblTipoPeriodoAcademico> consultarPorCodigo(TblTipoPeriodoAcademico tipoPeriodoAcademico) throws Exception {
		try {
		
			return tipoPeriodoAcademicoNegocio.consultarPorFiltro(tipoPeriodoAcademico, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
