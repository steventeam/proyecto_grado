package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPeriodoAcademico;
import co.edu.asys.gestorgrupos.negocio.negocio.PeriodoAcademicoNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PeriodoAcademicoFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private PeriodoAcademicoNegocio periodoAcademicoNegocio;
	
	public void crear(TblPeriodoAcademico periodoAcademico) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_CREACION_FACHADA");
			periodoAcademicoNegocio.crear(periodoAcademico, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblPeriodoAcademico periodoAcademico) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ACTUALIZACION_FACHADA");
			periodoAcademicoNegocio.actualizar(periodoAcademico, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblPeriodoAcademico periodoAcademico) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_ANTES_ELIMINACION_FACHADA");
			periodoAcademicoNegocio.inhabilitar(periodoAcademico, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PERIODO_ACADEMICO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PERIODO_ACADEMICO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblPeriodoAcademico> consultarPorFiltro(
			TblPeriodoAcademico periodoAcademico) throws Exception {
		try {
		
			return periodoAcademicoNegocio.consultarPorFiltro(periodoAcademico, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblPeriodoAcademico> consultarPorCodigo(TblPeriodoAcademico periodoAcademico) throws Exception {
		try {
		
			return periodoAcademicoNegocio.consultarPorFiltro(periodoAcademico, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
