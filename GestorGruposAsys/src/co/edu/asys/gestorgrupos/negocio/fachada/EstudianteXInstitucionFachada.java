package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXInstitucion;
import co.edu.asys.gestorgrupos.negocio.negocio.EstudianteXInstitucionNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class EstudianteXInstitucionFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private EstudianteXInstitucionNegocio estudianteXInstitucionNegocio;
	
	public void crear(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_CREACION_FACHADA");
			estudianteXInstitucionNegocio.crear(estudianteXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_ACTUALIZACION_FACHADA");
			estudianteXInstitucionNegocio.actualizar(estudianteXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_ANTES_ELIMINACION_FACHADA");
			estudianteXInstitucionNegocio.inhabilitar(estudianteXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblEstudianteXInstitucion> consultarPorFiltro(
			TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		try {
		
			return estudianteXInstitucionNegocio.consultarPorFiltro(estudianteXInstitucion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblEstudianteXInstitucion> consultarPorCodigo(TblEstudianteXInstitucion estudianteXInstitucion) throws Exception {
		try {
		
			return estudianteXInstitucionNegocio.consultarPorFiltro(estudianteXInstitucion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
