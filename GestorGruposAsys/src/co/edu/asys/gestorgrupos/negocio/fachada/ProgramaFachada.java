package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.negocio.negocio.ProgramaNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class ProgramaFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private ProgramaNegocio programaNegocio;
	
	public void crear(TblPrograma programa) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_CREACION_FACHADA", programa.getNvNombre());
			programaNegocio.crear(programa, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CREACION_FACHADA", programa.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROGRAMA_DESPUES_ROLLBACK");

			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
						
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblPrograma programa) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ACTUALIZACION_FACHADA", programa.getNvNombre());
			programaNegocio.actualizar(programa, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ACTUALIZACION_FACHADA", programa.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROGRAMA_DESPUES_ROLLBACK");

			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblPrograma programa) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_ANTES_ELIMINACION_FACHADA", programa.getNvNombre());
			programaNegocio.inhabilitar(programa, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_ELIMINACION_FACHADA", programa.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROGRAMA_DESPUES_ROLLBACK");

			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROGRAMA_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblPrograma> consultarPorFiltro(
			TblPrograma programa) throws Exception {
		try {
		
			return programaNegocio.consultarPorFiltro(programa, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblPrograma> consultarPorCodigo(TblPrograma programa) throws Exception {
		try {
		
			return programaNegocio.consultarPorFiltro(programa, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
