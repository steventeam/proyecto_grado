package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.negocio.negocio.TipoIdentificacionNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class TipoIdentificacionFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private TipoIdentificacionNegocio tipoIdentificacionNegocio;
	
	public void crear(SegTipoIdentificacion tipoIdentificacion) throws ExcepcionAplicacionGrupos {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_CREACION_FACHADA", tipoIdentificacion.getNvNombre());
			tipoIdentificacionNegocio.crear(tipoIdentificacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CREACION_FACHADA", tipoIdentificacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ACTUALIZACION_FACHADA", tipoIdentificacion.getNvNombre());
			tipoIdentificacionNegocio.actualizar(tipoIdentificacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ACTUALIZACION_FACHADA", tipoIdentificacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void inhabilitar(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_ANTES_ELIMINACION_FACHADA", tipoIdentificacion.getNvNombre());
			tipoIdentificacionNegocio.inhabilitar(tipoIdentificacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ELIMINACION_FACHADA", tipoIdentificacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_TIPO_IDENTIFICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_TIPO_IDENTIFICACION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<SegTipoIdentificacion> consultarPorFiltro(
			SegTipoIdentificacion tipoIdentificacion) throws Exception {
		try {
		
			return tipoIdentificacionNegocio.consultarPorFiltro(tipoIdentificacion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<SegTipoIdentificacion> consultarPorCodigo(SegTipoIdentificacion tipoIdentificacion) throws Exception {
		try {
		
			return tipoIdentificacionNegocio.consultarPorFiltro(tipoIdentificacion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
