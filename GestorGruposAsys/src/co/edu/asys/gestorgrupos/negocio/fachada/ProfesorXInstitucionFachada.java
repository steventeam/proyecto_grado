package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblProfesorXInstitucion;
import co.edu.asys.gestorgrupos.negocio.negocio.ProfesorXInstitucionNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class ProfesorXInstitucionFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private ProfesorXInstitucionNegocio profesorXInstitucionNegocio;
	
	public void crear(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_CREACION_FACHADA");
			profesorXInstitucionNegocio.crear(profesorXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ACTUALIZACION_FACHADA");
			profesorXInstitucionNegocio.actualizar(profesorXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_ANTES_ELIMINACION_FACHADA");
			profesorXInstitucionNegocio.inhabilitar(profesorXInstitucion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PROFESOR_X_INSTITUCION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblProfesorXInstitucion> consultarPorFiltro(
			TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		try {
		
			return profesorXInstitucionNegocio.consultarPorFiltro(profesorXInstitucion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblProfesorXInstitucion> consultarPorCodigo(TblProfesorXInstitucion profesorXInstitucion) throws Exception {
		try {
		
			return profesorXInstitucionNegocio.consultarPorFiltro(profesorXInstitucion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
