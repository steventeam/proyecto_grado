package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateriaXPensum;
import co.edu.asys.gestorgrupos.negocio.negocio.MateriaXPensumNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class MateriaXPensumFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private MateriaXPensumNegocio materiaXPensumNegocio;
	
	public void crear(TblMateriaXPensum materiaXPensum) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_CREACION_FACHADA");
			materiaXPensumNegocio.crear(materiaXPensum, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_MATERIA_X_PENSUM_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblMateriaXPensum materiaXPensum) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ACTUALIZACION_FACHADA");
			materiaXPensumNegocio.actualizar(materiaXPensum, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_MATERIA_X_PENSUM_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblMateriaXPensum materiaXPensum) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_ANTES_ELIMINACION_FACHADA");
			materiaXPensumNegocio.inhabilitar(materiaXPensum, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_MATERIA_X_PENSUM_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_MATERIA_X_PENSUM_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblMateriaXPensum> consultarPorFiltro(
			TblMateriaXPensum materiaXPensum) throws Exception {
		try {
		
			return materiaXPensumNegocio.consultarPorFiltro(materiaXPensum, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblMateriaXPensum> consultarPorCodigo(TblMateriaXPensum materiaXPensum) throws Exception {
		try {
		
			return materiaXPensumNegocio.consultarPorFiltro(materiaXPensum, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblMateriaXPensum> consultarPorGrupo(TblGrupo grupo) throws Exception {
		try {
		
			return materiaXPensumNegocio.consultarPorGrupo(grupo, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
}
