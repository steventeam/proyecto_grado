package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSesion;
import co.edu.asys.gestorgrupos.negocio.negocio.SesionNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class SesionFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private SesionNegocio sesionNegocio;
	
	public void crear(TblSesion session1) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_CREACION_FACHADA");
			sesionNegocio.crear(session1, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_SESION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblSesion session1) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ACTUALIZACION_FACHADA");
			sesionNegocio.actualizar(session1, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_SESION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblSesion session1) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_ANTES_ELIMINACION_FACHADA");
			sesionNegocio.inhabilitar(session1, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_SESION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SESION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_SESION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblSesion> consultarPorFiltro(
			TblSesion session1) throws Exception {
		try {
		
			return sesionNegocio.consultarPorFiltro(session1, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblSesion> consultarPorCodigo(TblSesion session1) throws Exception {
		try {
		
			return sesionNegocio.consultarPorFiltro(session1, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
