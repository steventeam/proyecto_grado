package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegPreguntaUsuario;
import co.edu.asys.gestorgrupos.negocio.negocio.PreguntaUsuarioNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class PreguntaUsuarioFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private PreguntaUsuarioNegocio preguntaUsuarioNegocio;
	
	public void crear(SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_CREACION_FACHADA");
			preguntaUsuarioNegocio.crear(preguntaUsuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PREGUNTA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ACTUALIZACION_FACHADA");
			preguntaUsuarioNegocio.actualizar(preguntaUsuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PREGUNTA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_ANTES_ELIMINACION_FACHADA");
			preguntaUsuarioNegocio.inhabilitar(preguntaUsuario, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_PREGUNTA_USUARIO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_PREGUNTA_USUARIO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<SegPreguntaUsuario> consultarPorFiltro(
			SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
		
			return preguntaUsuarioNegocio.consultarPorFiltro(preguntaUsuario, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<SegPreguntaUsuario> consultarPorCodigo(SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
		
			return preguntaUsuarioNegocio.consultarPorFiltro(preguntaUsuario, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public void recuperar(SegPreguntaUsuario preguntaUsuario) throws Exception {
		try {
			preguntaUsuarioNegocio.recuperar(preguntaUsuario, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception exception) {
			daoFactory.cancelarTransaccion();
			throw exception;
		} finally {
			daoFactory.cerrarConexion();
		}

	}
	
	
}
