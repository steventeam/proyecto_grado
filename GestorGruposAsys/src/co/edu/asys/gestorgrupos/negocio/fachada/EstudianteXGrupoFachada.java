package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.negocio.negocio.EstudianteXGrupoNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class EstudianteXGrupoFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private EstudianteXGrupoNegocio estudianteXGrupoNegocio;
	
	public void crear(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_CREACION_FACHADA");
			estudianteXGrupoNegocio.crear(estudianteXGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ACTUALIZACION_FACHADA");
			estudianteXGrupoNegocio.crear(estudianteXGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_ANTES_ELIMINACION_FACHADA");
			estudianteXGrupoNegocio.crear(estudianteXGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_ESTUDIANTE_X_GRUPO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblEstudianteXGrupo> consultarPorFiltro(
			TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		try {
		
			return estudianteXGrupoNegocio.consultarPorFiltro(estudianteXGrupo, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblEstudianteXGrupo> consultarPorCodigo(TblEstudianteXGrupo estudianteXGrupo) throws Exception {
		try {
		
			return estudianteXGrupoNegocio.consultarPorFiltro(estudianteXGrupo, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
		
}
