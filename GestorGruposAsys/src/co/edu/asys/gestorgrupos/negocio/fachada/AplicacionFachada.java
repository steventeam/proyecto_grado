package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.negocio.negocio.AplicacionNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class AplicacionFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private AplicacionNegocio aplicacionNegocio;
	
	public void crear(SegAplicacion aplicacion) throws Exception {

		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_CREACION_FACHADA", aplicacion.getNvNombre());
			aplicacionNegocio.crear(aplicacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CREACION_FACHADA", aplicacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_APLICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void actualizar(SegAplicacion aplicacion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ACTUALIZACION_FACHADA", aplicacion.getNvNombre());
			aplicacionNegocio.actualizar(aplicacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ACTUALIZACION_FACHADA", aplicacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_APLICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void inhabilitar(SegAplicacion aplicacion) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_ANTES_ELIMINACION_FACHADA", aplicacion.getNvNombre());
			aplicacionNegocio.inhabilitar(aplicacion, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_ELIMINACION_FACHADA", aplicacion.getNvNombre());
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_APLICACION_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_APLICACION_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<SegAplicacion> consultarPorFiltro(
			SegAplicacion aplicacion) throws Exception {
		try {
		
			return aplicacionNegocio.consultarPorFiltro(aplicacion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<SegAplicacion> consultarPorCodigo(SegAplicacion aplicacion) throws Exception {
		try {
		
			return aplicacionNegocio.consultarPorFiltro(aplicacion, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
