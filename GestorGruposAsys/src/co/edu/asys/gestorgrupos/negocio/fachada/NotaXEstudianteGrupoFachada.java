package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXEstudianteGrupo;
import co.edu.asys.gestorgrupos.negocio.negocio.NotaXEstudianteGrupoNegocio;
import co.edu.asys.gestorgrupos.transversal.enums.LugarExcepcionEnum;
import co.edu.asys.gestorgrupos.transversal.excepcion.UtilExcepcion;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@Service
@Scope("prototype")
public class NotaXEstudianteGrupoFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private NotaXEstudianteGrupoNegocio notaXEstudianteGrupoNegocio;
	
	public void crear(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
				
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_CREACION_FACHADA");
			notaXEstudianteGrupoNegocio.crear(notaXEstudianteGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CREACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CERRAR_CONEXION");
		}
		
	}
	
	public void actualizar(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ACTUALIZACION_FACHADA");
			notaXEstudianteGrupoNegocio.actualizar(notaXEstudianteGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ACTUALIZACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public void inhabilitar(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		try {
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_ANTES_ELIMINACION_FACHADA");
			notaXEstudianteGrupoNegocio.inhabilitar(notaXEstudianteGrupo, daoFactory);
			daoFactory.confirmarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ELIMINACION_FACHADA");
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_ROLLBACK");
			
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
			String mensajeTecnico = excepcion.getMessage(); 
			
			UtilExcepcion.controlarExcepcion(mensajeUsuario, mensajeTecnico, excepcion, LugarExcepcionEnum.FACHADA, false, this.getClass());
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
			UtilLogging.escribirLog(this.getClass(), Level.TRACE, null, "TRAZA_NOTA_X_ESTUDIANTE_GRUPO_DESPUES_CERRAR_CONEXION");
		}
	}
	
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorFiltro(
			TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		try {
		
			return notaXEstudianteGrupoNegocio.consultarPorFiltro(notaXEstudianteGrupo, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblNotaXEstudianteGrupo> consultarPorNumeroId(TblNotaXEstudianteGrupo notaXEstudianteGrupo) throws Exception {
		try {
		
			return notaXEstudianteGrupoNegocio.consultarPorNumeroId(notaXEstudianteGrupo, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
