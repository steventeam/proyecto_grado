package co.edu.asys.gestorgrupos.negocio.fachada;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.edu.asys.gestorgrupos.datos.daofactory.HibernateDAOFactory;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;
import co.edu.asys.gestorgrupos.negocio.negocio.GeneroNegocio;

@Service
@Scope("prototype")
public class GeneroFachada {
	
	@Autowired
	private HibernateDAOFactory daoFactory;
	
	@Autowired
	private GeneroNegocio generoNegocio;
	
	public void crear(TblGenero genero) throws Exception {
		try {
			generoNegocio.crear(genero, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public void actualizar(TblGenero genero) throws Exception {
		try {
			generoNegocio.actualizar(genero, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public void inhabilitar(TblGenero genero) throws Exception {
		try {
			generoNegocio.inhabilitar(genero, daoFactory);
			daoFactory.confirmarTransaccion();
		} catch (Exception excepcion) {
			daoFactory.cancelarTransaccion();
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblGenero> consultarPorFiltro(
			TblGenero genero) throws Exception {
		try {
		
			return generoNegocio.consultarPorFiltro(genero, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	public ArrayList<TblGenero> consultarPorCodigo(TblGenero genero) throws Exception {
		try {
		
			return generoNegocio.consultarPorFiltro(genero, daoFactory);
		} catch (Exception excepcion) {
			
			throw excepcion;
		} finally {
			daoFactory.cerrarConexion();
		}
	}
	
	
}
