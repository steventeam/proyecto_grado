package co.edu.asys.gestorgrupos.presentacion.controlador;


import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPerfil;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;
import co.edu.asys.gestorgrupos.negocio.fachada.UsuarioFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name = "usuarioConsultaBean")
@SessionScoped
public class UsuarioConsultaControlador {

	private SegUsuario usuario;
	private ArrayList<SegUsuario> listaUsuario;
	
	public ArrayList<SegUsuario> getListaUsuario() {
		return listaUsuario;
	}

	public SegUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(SegUsuario usuario) {
		this.usuario = usuario;
	}

	public UsuarioConsultaControlador() {
		usuario = new SegUsuario();
		usuario.setSegTipoIdentificacion(new SegTipoIdentificacion());
		usuario.setSegPerfil(new SegPerfil());
		usuario.setTblGenero(new TblGenero());
	}

	// Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {

		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}

	public void consultar() {
		
		
		try {
			if(usuario.getNvNumeroIdentificacion() != null && usuario.getNvNumeroIdentificacion() != "" && usuario.getNvClave() != null && usuario.getNvClave() != ""){
				UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
				listaUsuario = usuarioFachada.consultarPorNumeroId(usuario);
			}
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	public void onRowEdit(RowEditEvent event) {
		
        SegUsuario usuario = (SegUsuario) event.getObject();
        
        try {
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			
			usuarioFachada.actualizar(usuario);

			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_EXITO_ACTUALIZACION", usuario.getNvNombreUsuario()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		} finally {
			limpiar();
		}
    }
    
    public void onRowCancel(RowEditEvent event) {
    	FacesContext.getCurrentInstance();
    	
    }

	public void limpiar() {

		usuario = new SegUsuario();
		usuario.setSegTipoIdentificacion(new SegTipoIdentificacion());
		usuario.setSegPerfil(new SegPerfil());
		usuario.setTblGenero(new TblGenero());
	}
	
	
}
