package co.edu.asys.gestorgrupos.presentacion.controlador;

//import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXEstudianteGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblNotaXGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSesion;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoNota;
import co.edu.asys.gestorgrupos.negocio.fachada.NotaXEstudianteGrupoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="notaXEstudianteGrupoBean")
@SessionScoped
public class NotaXEstudianteGrupoControlador {
	
	private TblNotaXEstudianteGrupo notaXEstudianteGrupo;
	private ArrayList<TblNotaXEstudianteGrupo> listaNotaXEstudianteGrupo;
	public ArrayList<TblTipoNota> listTipoNota;
	private TblEstudianteXGrupo estudianteXGrupo;
	private ArrayList<TblGrupo> listaGrupo;

		
	public TblEstudianteXGrupo getEstudianteUsuario() {
		return estudianteXGrupo;
	}

	public ArrayList<TblNotaXEstudianteGrupo> getListaNotaXEstudianteGrupo() {
		return listaNotaXEstudianteGrupo;
	}
	
	public ArrayList<TblGrupo> getListaGrupo() {
		return listaGrupo;
	}
	
	
	//////////////////////
	private double acumuladorNota = 0;
	private double contarNota = 0;
	private double acumuladorPorcentaje = 0;
	
	public double getAcumuladorNotaFinal() {
        double aux = acumuladorNota ;
        acumuladorNota = 0;
        return aux;
    }
	
	public double getContarNota() {
        double aux2 = contarNota;
        contarNota = 0;
        return aux2;
    }	
	
	public double getAcumuladorPorcentaje() {
		double auxPor = acumuladorPorcentaje;
		acumuladorPorcentaje = 0;
		return auxPor;
	}
	
	public void acumuladorNotasFinales(double valor)	{
		acumuladorNota += valor;
    }
	
	public void acumuladorPorcentajes(double porcentual){
	
			try {
				
				if(acumuladorPorcentaje <= 100){
					acumuladorPorcentaje += porcentual;
				}
				
			} catch (Exception excepcion) {
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION", mensajeUsuario));
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
			}
    }
	
	public void contarNotas(double valor)
    {
		contarNota = valor + 1;
    }
	
	///////////////////////////////////////////////////
	
	public ArrayList<TblNotaXEstudianteGrupo> getListaNotaXEstudianteGrupo2() {
		
		ArrayList<TblNotaXEstudianteGrupo> listaDef2 = new ArrayList<>();
		for(TblNotaXEstudianteGrupo lista : listaNotaXEstudianteGrupo){	
			listaDef2.add(lista);
			if(listaDef2.contains(notaXEstudianteGrupo.getTblNotaXGrupo().getTblTipoNota().getInCodigo())){
				listaDef2.add(lista);
			}
		}
		
		return listaDef2;
	}
	
	public ArrayList<TblTipoNota> getListTipoNota() {
		return listTipoNota;
	}

	public TblNotaXEstudianteGrupo getNotaXEstudianteGrupo() {
		return notaXEstudianteGrupo;
	}

	public void setNotaXEstudianteGrupo(TblNotaXEstudianteGrupo notaXEstudianteGrupo) {
		this.notaXEstudianteGrupo = notaXEstudianteGrupo;
	}

	public NotaXEstudianteGrupoControlador() {
		notaXEstudianteGrupo = new TblNotaXEstudianteGrupo();
		notaXEstudianteGrupo.setInCodigo(213999999);
		notaXEstudianteGrupo.setTblSesion(new TblSesion());
		notaXEstudianteGrupo.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		notaXEstudianteGrupo.setTblNotaXGrupo(new TblNotaXGrupo());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(notaXEstudianteGrupo != null){
				
				NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
				notaXEstudianteGrupoFachada.crear(notaXEstudianteGrupo);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
			notaXEstudianteGrupoFachada.actualizar(notaXEstudianteGrupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
			notaXEstudianteGrupoFachada.inhabilitar(notaXEstudianteGrupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_ESTUDIANTE_GRUPO_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
			listaNotaXEstudianteGrupo = notaXEstudianteGrupoFachada.consultarPorFiltro(notaXEstudianteGrupo);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
			listaNotaXEstudianteGrupo = notaXEstudianteGrupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		notaXEstudianteGrupo = new TblNotaXEstudianteGrupo();
		notaXEstudianteGrupo.setInCodigo(213999999);
		notaXEstudianteGrupo.setTblSesion(new TblSesion());
		notaXEstudianteGrupo.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		notaXEstudianteGrupo.setTblNotaXGrupo(new TblNotaXGrupo());
	}
	
//	private Map<Tabla, List<Campo>> tablaCampos = new HashMap<Tabla, List<Campo>>();
	
	//Calcular nota final
	
	public ArrayList<TblNotaXEstudianteGrupo> getListaNotaXEstudianteGrupoFinal(){
		
		ArrayList<TblNotaXEstudianteGrupo> listaNotas = new ArrayList<>();
		
		for (TblNotaXEstudianteGrupo lista: listaNotaXEstudianteGrupo){
			
			double acumPor = 0, acumNota = 0;
			
//			acumPor += lista.getTblNotaXGrupo().getInPorcentaje();
			
			if(acumPor<=100){
				acumNota += lista.getNuNotaPorcentual();
			}
			
			if(acumNota>= 3){
				System.out.println("Gan�");
			}
		}
		
		return listaNotas;
		
	}

}