package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPerfil;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecurso;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecursoPerfil;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;
import co.edu.asys.gestorgrupos.negocio.fachada.UsuarioFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;
import co.edu.asys.transversal.util.RecursoDTO;
import co.edu.asys.transversal.util.SesionDTO;
//import co.edu.asys.transversal.util.TipoRecursoDTO;
import co.edu.asys.transversal.util.UsuarioDTO;
import co.edu.asys.transversal.util.UtilSesion;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioControlador implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String lista;
	private SegUsuario usuario;
	private ArrayList<SegUsuario> listaUsuario;
//	private ArrayList<SegUsuario> listaUsuario2;
//	private String destino = "C:\\Users\\steven\\git\\proyecto_grado_asys\\Fotos\\";
	private boolean renderIframeColumn;
	
	public boolean isRenderIframeColumn() {
		return renderIframeColumn;
	}

	public void setRenderIframeColumn(boolean renderIframeColumn) {
		this.renderIframeColumn = renderIframeColumn;
	}

	public ArrayList<SegUsuario> getListaUsuario() {
		return listaUsuario;
	}

	public SegUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(SegUsuario usuario) {
		this.usuario = usuario;
	}

	public UsuarioControlador() {
		usuario = new SegUsuario();
		usuario.setSegTipoIdentificacion(new SegTipoIdentificacion());
		usuario.setSegPerfil(new SegPerfil());
		usuario.setTblGenero(new TblGenero());
		consultarTodos();
	}

	// Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {

		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	public void crear() {
		
		try {
			
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			usuarioFachada.crear(usuario);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_EXITO_CREACION", usuario.getNvNombreUsuario()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			usuarioFachada.actualizar(usuario);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_EXITO_ACTUALIZACION", usuario.getNvNombreUsuario()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar2() {
		
		if (usuario.getNvNombreUsuario().isEmpty() || usuario.getNvNombreUsuario() == null) {
	
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION", mensajeUsuario));
			
		}else {
	
			try {
			
					UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			
//					String encript = DigestUtils.sha1Hex(usuario.getNvClave());
//					usuario.setNvClave(encript);
//					
					usuarioFachada.actualizar2(usuario);
			
					FacesContext.getCurrentInstance().getExternalContext().redirect("/GestorGruposAsys/faces/paginas/publico/login.xhtml");
					
					
				} catch (ExcepcionAplicacionGrupos excepcion) {
					gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION"));
					excepcion.getMensajeUsuario();
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
							
				} catch (Exception excepcion) {
					//Obtener los mensajes desde el cat�logo de mensajes
					String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
					gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION", mensajeUsuario));
					UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
				} finally {
					consultarTodos();
					limpiar();
			}
		}
	}
	
	public void habilitar() {
		
		try {
		
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);

			usuarioFachada.habilitar(usuario);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "�xito habilitando usuario", "Se ha habilitado exitosamente el usuario.");
					
			} catch (Exception exception) {
			
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error en la habilitaci�n del usuario", "Se ha presentado un error tratando de habilitar el usuario. Por favor intente de nuevo y si el problema persiste contacte al administrador de la aplicaci�n");
				exception.printStackTrace();
			} finally {
				consultarTodos();
				limpiar();
			}
	}
		
	public void inhabilitar() {
		
		try {
			
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			usuarioFachada.inhabilitar(usuario);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_EXITO_ELIMINACION", usuario.getNvNombreUsuario()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void consultar() {
		
		
		try {
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			listaUsuario = usuarioFachada.consultarPorFiltro(usuario);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
		
//	public void upload(FileUploadEvent event) {
//		try {
//			copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
//			FacesMessage message = new FacesMessage("El archivo se ha cargado con �xito!");
//			FacesContext.getCurrentInstance().addMessage(null, message);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	public void copyFile(String fileName, InputStream in) {
//		try {
//			OutputStream out = new FileOutputStream(new File(destino + fileName));
//			int read = 0;
//			byte[] bytes = new byte[1024];
//			while ((read = in.read(bytes)) != -1) {
//				out.write(bytes, 0, read);
//			}
//			in.close();
//			out.flush();
//			out.close();
//			System.out.println("El archivo se ha creado con �xito!");
//
//			String ruta1 = destino + fileName;
//
//			new File(ruta1);
//
//		} catch (IOException e) {
//			System.out.println(e.getMessage());
//		}
//	}
//
//	private String rutaImagen;
//
//	public String getRutaImagen() throws FileNotFoundException {
//		prepararImagen();
//		return rutaImagen;
//	}
//
//	public void setRutaImagen(String rutaImagen) {
//		this.rutaImagen = rutaImagen;
//	}
//
//	private StreamedContent graphicImage;
//
//	public void prepararImagen() throws FileNotFoundException {
//
//		if (usuario.getNvFoto() != null) {
//						
//			String parteRutaUno = "../../imagenes/";
//			String parteRutaDos = ".jpg";
//			String espacio = " ";
//			
//			rutaImagen = parteRutaUno + usuario.getNvPrimerNombre()+ espacio + usuario.getNvPrimerApellido() + parteRutaDos;
//			
//			System.out.println(rutaImagen);
//		}
//	}
//
//	public StreamedContent getGraphicImage() throws FileNotFoundException {
//		prepararImagen();
//		return graphicImage;
//	}
//
//	public void setGraphicImage(StreamedContent graphicImage) {
//		this.graphicImage = graphicImage;
//	}
	
	public void onRowEdit(RowEditEvent event) {
		
        SegUsuario usuario = (SegUsuario) event.getObject();
        
        try {
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			
			usuarioFachada.actualizar(usuario);

			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_USUARIO_EXITO_ACTUALIZACION", usuario.getNvNombreUsuario()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		} finally {
			consultarTodos();
			limpiar();
		}
    }
    
    public void onRowCancel(RowEditEvent event) {
    	FacesContext.getCurrentInstance();
    	
    }
	
	public void consultarUsuario() {

			try {

				UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
				ArrayList<SegUsuario> listaUsuarios = usuarioFachada.consultarPorFiltro(usuario);

				if (listaUsuarios.isEmpty()) {

					gestionarMensaje(FacesMessage.SEVERITY_WARN, "Error de login", "Datos inv�lidos");

				} else {
//					gestionarMensaje(FacesMessage.SEVERITY_INFO, "Bienvenido", usuario.getNvNombreUsuario());
//					FacesContext.getCurrentInstance().getExternalContext().redirect("/GestorGruposAsys/faces/paginas/publico/login.xhtml");

					 FacesContext.getCurrentInstance().getExternalContext().redirect("/GestorGruposAsys/faces/paginas/publico/recuperarClave/respuestaUsuario.xhtml");
				}

			} catch (Exception excepcion) {

				excepcion.printStackTrace();

			}

		}

	public void limpiar() {

		usuario = new SegUsuario();
		usuario.setSegTipoIdentificacion(new SegTipoIdentificacion());
		usuario.setSegPerfil(new SegPerfil());
		usuario.setTblGenero(new TblGenero());

	}
	
	public void logout() throws IOException {
	      HttpSession session = UtilSesion.getSession();
	      session.invalidate();
	      FacesContext.getCurrentInstance().getExternalContext().redirect("/GestorGruposAsys/faces/paginas/publico/login.xhtml");
	   }
	
	
	
	//////////////------------------LOGIN--------------------------------------////////////////////////////////
	
	/**
	 * @return
	 * @throws Exception
	 * En cualquiera de los login debes guardar la sesion, creo que es usuario, en el crear obtienes esa sesi�n y seteas el valor que necesitas 
	 * en la clase del usuario que vas a mandar a guardar y as� env�as ese dto, ya que como lo tienes no tienes forma de enviar eso
	 */

	public String login() throws Exception {
		// TODO: VALIDAR DATOS
		// 1. Formato de los datos
		// 2. Obligatoriedad
		// 3. Tipo de datos
		// 4. Longitudes m�nima y m�xima
		// 5. Cross scripting y SQL Injection
		
		String paginaRetorno = null;
		
		
		UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
		ArrayList<SegUsuario> listaUsuarios = usuarioFachada.consultarPorFiltro(usuario);
				
			if (listaUsuarios == null || listaUsuarios.isEmpty()) {
			// No existe el usuario
				
			paginaRetorno = "/paginas/publico/login.xhtml?faces-redirect=true";
				
			
			HttpServletRequest peticion = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			UtilSesion.obtenerUtilSesion().cerrarSesion(peticion);
			
			} else {
				
				SegUsuario usuarioConsultado = listaUsuarios.get(0);

			// Mapa de recursos donde almacenaremos todos los recursos que puede
			// ver
			// el usuario actual
				
			HashMap<String, RecursoDTO> mapaRecursos = new HashMap<String, RecursoDTO>();

			// Obtenemos todos los perfiles por usuario que tiene el usuario
			Set<SegUsuario> setPerfilesUsuario = usuarioConsultado.getSegPerfil().getSegUsuarios();

			// Iteramos todos los perfiles que tiene asignados el usuario
			for (SegUsuario segPerfilUsuario : setPerfilesUsuario) {
				
				// Obtenemos los datos del perfil que estamos iterando
				// actualmente
				SegPerfil perfil = segPerfilUsuario.getSegPerfil();

				// Obtenemos todos los recursos que tiene asignados el perfil
				// actual
				Set<SegRecursoPerfil> setRecursosPerfil = perfil.getSegRecursoPerfils();

				// Iteramos todos los recursos que tiene el perfil asignado
				for (SegRecursoPerfil segRecursoPerfil : setRecursosPerfil) {
					
					
					if(usuarioConsultado.getDtFechaEliminacion() == null){
						SegRecurso recurso = segRecursoPerfil.getSegRecurso();
	//					RecursoDTO recursoCreado = new RecursoDTO(recurso.getInCodigo(), null, (recurso.getSegTipoRecursoByInCodigoTipoRecurso() == null) ? null : new TipoRecursoDTO(recurso.getSegRecurso().getInCodigo(), recurso.getSegRecurso().getNvNombre()), recurso.getNvNombre(), recurso.getNvUrl(), recurso.getInOrden());
						if(recurso.getDtFechaEliminacion() == null){
							mapaRecursos.put(recurso.getNvUrl(), null);
						}
					}
				}

			}

					UsuarioDTO usuario1 = new UsuarioDTO(usuarioConsultado.getInCodigo(), usuarioConsultado.getNvPrimerNombre(), usuarioConsultado.getNvSegundoNombre(), usuarioConsultado.getNvPrimerApellido(), usuarioConsultado.getNvSegundoApellido(), usuarioConsultado.getNvCorreoElectronico(), usuarioConsultado.getNvNombreUsuario());
		
					// Creaci�n de la sesi�n del usuario
					SesionDTO sesionUsuario = new SesionDTO(usuario1, mapaRecursos);
					
					HttpSession session = UtilSesion.getSession();
		            session.setAttribute("nvNombreUsuario", usuarioConsultado.getNvNombreUsuario());
		
					HttpServletRequest peticion = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
					UtilSesion.obtenerUtilSesion().crearSesion(sesionUsuario, peticion);
					
						
		
					paginaRetorno = "/paginas/protegido/inicio.xhtml?faces-redirect=true";
		}
			
		return paginaRetorno;
		
	}

		
	private void consultarTodos() {
		
		try {
			UsuarioFachada estudianteFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			listaUsuario = estudianteFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	

	
	public String getLista() {
		return lista;
	}

	public void setLista(String lista) {
		this.lista = lista;
	}

	
	
	public ArrayList<SegUsuario> getListaUsuario2() {
		ArrayList<SegUsuario> listaDef = new ArrayList<>();
		for(SegUsuario lista : listaUsuario){	
			if(lista.getDtFechaEliminacion() == null){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
	
	public ArrayList<SegUsuario> getListaUsuario3() {
		ArrayList<SegUsuario> listaDef2 = new ArrayList<>();
		for(SegUsuario lista : listaUsuario){	
			if(lista.getDtFechaEliminacion() != null){
				listaDef2.add(lista);
			}
		}
		return listaDef2;
		
	}
	
	public ArrayList<SegUsuario> getListaUsuarioPerfilEstudiante() {
		ArrayList<SegUsuario> listaDef = new ArrayList<>();
		for(SegUsuario lista : listaUsuario){	
			if(lista.getSegPerfil().getNvNombre().equals("Estudiante") || lista.getSegPerfil().getNvNombre().equals("estudiante")){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
	
	public ArrayList<SegUsuario> getListaUsuarioPerfilProfesor() {
		ArrayList<SegUsuario> listaDef = new ArrayList<>();
		for(SegUsuario lista : listaUsuario){	
			if(lista.getSegPerfil().getNvNombre().equals("Profesor") || lista.getSegPerfil().getNvNombre().equals("profesor")){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
	
}
