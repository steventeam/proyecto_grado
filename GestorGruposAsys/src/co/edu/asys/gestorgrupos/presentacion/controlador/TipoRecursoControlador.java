package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoRecurso;
import co.edu.asys.gestorgrupos.negocio.fachada.TipoRecursoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="tipoRecursoBean")
@SessionScoped
public class TipoRecursoControlador {
	
	private SegTipoRecurso tipoRecurso;
	private ArrayList<SegTipoRecurso> listaTipoRecurso;
	
		
	public ArrayList<SegTipoRecurso> getListaTipoRecurso() {
		return listaTipoRecurso;
	}

	public SegTipoRecurso getTipoRecurso() {
		return tipoRecurso;
	}

	public void setTipoRecurso(SegTipoRecurso tipoRecurso) {
		this.tipoRecurso = tipoRecurso;
	}

	public TipoRecursoControlador() {
		tipoRecurso = new SegTipoRecurso();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(tipoRecurso != null){
				
				TipoRecursoFachada tipoRecursoFachada = LocalizadorBean.ObtenerBean(TipoRecursoFachada.class);
				tipoRecursoFachada.crear(tipoRecurso);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_EXITO_CREACION", tipoRecurso.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			TipoRecursoFachada tipoRecursoFachada = LocalizadorBean.ObtenerBean(TipoRecursoFachada.class);
			tipoRecursoFachada.actualizar(tipoRecurso);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_EXITO_ACTUALIZACION", tipoRecurso.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			TipoRecursoFachada tipoRecursoFachada = LocalizadorBean.ObtenerBean(TipoRecursoFachada.class);
			tipoRecursoFachada.inhabilitar(tipoRecurso);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_RECURSO_EXITO_ELIMINACION", tipoRecurso.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			TipoRecursoFachada tipoRecursoFachada = LocalizadorBean.ObtenerBean(TipoRecursoFachada.class);
			listaTipoRecurso = tipoRecursoFachada.consultarPorFiltro(tipoRecurso);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			TipoRecursoFachada tipoRecursoFachada = LocalizadorBean.ObtenerBean(TipoRecursoFachada.class);
			listaTipoRecurso = tipoRecursoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_RECURSO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		//1. A pedal
		tipoRecurso.setInCodigo(null);
		tipoRecurso.setNvNombre(null);
		
		//2. Mejor
		tipoRecurso = new SegTipoRecurso();
	}
		
}