package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.negocio.fachada.AplicacionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="aplicacionBean")
@SessionScoped
public class AplicacionControlador {
	
	private SegAplicacion aplicacion;
	private ArrayList<SegAplicacion> listaAplicacion;
	
		
	public ArrayList<SegAplicacion> getListaAplicacion() {
		return listaAplicacion;
	}

	public SegAplicacion getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(SegAplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public AplicacionControlador() {
		aplicacion = new SegAplicacion();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	public void crear() {
		
		try {
			
			AplicacionFachada aplicacionFachada = LocalizadorBean.ObtenerBean(AplicacionFachada.class);
			aplicacionFachada.crear(aplicacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_EXITO_CREACION", aplicacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			AplicacionFachada aplicacionFachada = LocalizadorBean.ObtenerBean(AplicacionFachada.class);
			aplicacionFachada.actualizar(aplicacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_EXITO_ACTUALIZACION", aplicacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			AplicacionFachada aplicacionFachada = LocalizadorBean.ObtenerBean(AplicacionFachada.class);
			aplicacionFachada.inhabilitar(aplicacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_APLICACION_EXITO_ELIMINACION", aplicacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			AplicacionFachada aplicacionFachada = LocalizadorBean.ObtenerBean(AplicacionFachada.class);
			listaAplicacion = aplicacionFachada.consultarPorFiltro(aplicacion);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	public void limpiar(){
		
		aplicacion = new SegAplicacion();
	}
	
	private void consultarTodos() {
	
		try {
			AplicacionFachada aplicacionFachada = LocalizadorBean.ObtenerBean(AplicacionFachada.class);
			listaAplicacion = aplicacionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_APLICACION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
}