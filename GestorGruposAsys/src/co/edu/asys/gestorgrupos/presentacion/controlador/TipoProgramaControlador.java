package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblInstitucion;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPrograma;
import co.edu.asys.gestorgrupos.negocio.fachada.TipoProgramaFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="tipoProgramaBean")
@SessionScoped
public class TipoProgramaControlador {
	
	private TblTipoPrograma tipoPrograma;
	private ArrayList<TblTipoPrograma> listaTipoPrograma;
	
		
	public ArrayList<TblTipoPrograma> getListaTipoPrograma() {
		return listaTipoPrograma;
	}

	public TblTipoPrograma getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(TblTipoPrograma tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public TipoProgramaControlador() {
		tipoPrograma = new TblTipoPrograma();
		tipoPrograma.setTblInstitucion(new TblInstitucion());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(tipoPrograma != null){
				
				TipoProgramaFachada tipoProgramaFachada = LocalizadorBean.ObtenerBean(TipoProgramaFachada.class);
				tipoProgramaFachada.crear(tipoPrograma);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_EXITO_CREACION", tipoPrograma.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			TipoProgramaFachada tipoProgramaFachada = LocalizadorBean.ObtenerBean(TipoProgramaFachada.class);
			tipoProgramaFachada.actualizar(tipoPrograma);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_EXITO_ACTUALIZACION", tipoPrograma.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			TipoProgramaFachada tipoProgramaFachada = LocalizadorBean.ObtenerBean(TipoProgramaFachada.class);
			tipoProgramaFachada.inhabilitar(tipoPrograma);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PROGRAMA_EXITO_ELIMINACION", tipoPrograma.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			TipoProgramaFachada tipoProgramaFachada = LocalizadorBean.ObtenerBean(TipoProgramaFachada.class);
			listaTipoPrograma = tipoProgramaFachada.consultarPorFiltro(tipoPrograma);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			TipoProgramaFachada tipoProgramaFachada = LocalizadorBean.ObtenerBean(TipoProgramaFachada.class);
			listaTipoPrograma = tipoProgramaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PROGRAMA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		tipoPrograma = new TblTipoPrograma();
		tipoPrograma.setTblInstitucion(new TblInstitucion());
	}
		
}