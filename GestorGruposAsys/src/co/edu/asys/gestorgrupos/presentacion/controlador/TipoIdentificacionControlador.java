package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoIdentificacion;
import co.edu.asys.gestorgrupos.negocio.fachada.TipoIdentificacionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="tipoIdentificacionBean")
@SessionScoped
public class TipoIdentificacionControlador {
	
	private ArrayList<SegTipoIdentificacion> listaTiposIdentificacion;
	
	@ManagedProperty("#{tipoIdentificacion}")
	private SegTipoIdentificacion tipoIdentificacion;
	
	public ArrayList<SegTipoIdentificacion> getListaTiposIdentificacion() {
		return listaTiposIdentificacion;
	}

	public SegTipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(SegTipoIdentificacion tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public TipoIdentificacionControlador() {
		tipoIdentificacion = new SegTipoIdentificacion();
		
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			TipoIdentificacionFachada tipoIdentificacionFachada = LocalizadorBean.ObtenerBean(TipoIdentificacionFachada.class);
			tipoIdentificacionFachada.crear(tipoIdentificacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_CREACION", tipoIdentificacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			TipoIdentificacionFachada tipoIdentificacionFachada = LocalizadorBean.ObtenerBean(TipoIdentificacionFachada.class);
			tipoIdentificacionFachada.actualizar(tipoIdentificacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_ACTUALIZACION", tipoIdentificacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			TipoIdentificacionFachada tipoIdentificacionFachada = LocalizadorBean.ObtenerBean(TipoIdentificacionFachada.class);
			tipoIdentificacionFachada.inhabilitar(tipoIdentificacion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_ELIMINACION", tipoIdentificacion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			TipoIdentificacionFachada tipoIdentificacionFachada = LocalizadorBean.ObtenerBean(TipoIdentificacionFachada.class);
			listaTiposIdentificacion = tipoIdentificacionFachada.consultarPorFiltro(tipoIdentificacion);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	public void limpiar(){
		
		tipoIdentificacion = new SegTipoIdentificacion();
	}
	
	private void consultarTodos() {
	
		try {
			TipoIdentificacionFachada tipoIdentificacionFachada = LocalizadorBean.ObtenerBean(TipoIdentificacionFachada.class);
			listaTiposIdentificacion = tipoIdentificacionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_IDENTIFICACION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}

	private String lista;
	
	public String getLista() {
		return lista;
	}

	public void setLista(String lista) {
		this.lista = lista;
	}
	
	public ArrayList<SegTipoIdentificacion> getListaTiposIdentificacion2() {
		ArrayList<SegTipoIdentificacion> listaDef = new ArrayList<>();
		for(SegTipoIdentificacion lista : listaTiposIdentificacion){			
			if(lista.getDtFechaEliminacion() == (null)){
				listaDef.add(lista);
			}
		}
	
		return listaDef;
	}

}