package co.edu.asys.gestorgrupos.presentacion.controlador;

//import java.text.DecimalFormat;
import java.util.ArrayList;


import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.NotaXEstudianteGrupoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="notaXEstudianteGrupoConsultaBean")
@SessionScoped
public class NotaXEstudianteGrupoConsultaControlador {
	
	private TblNotaXEstudianteGrupo notaXEstudianteGrupo;
	private ArrayList<TblNotaXEstudianteGrupo> listaNotaXEstudianteGrupo;
	public ArrayList<TblTipoNota> listTipoNota;
	private TblEstudianteXGrupo estudianteXGrupo;
	private  SegUsuario usuario;

		
	public SegUsuario getUsuario() {
		return usuario;
	}

	public TblEstudianteXGrupo getEstudianteUsuario() {
		return estudianteXGrupo;
	}

	public ArrayList<TblNotaXEstudianteGrupo> getListaNotaXEstudianteGrupo() {
		return listaNotaXEstudianteGrupo;
	}
	
	
	//////////////////////
	private double acumuladorNota = 0;
	private double contarNota = 0;
	private double acumuladorPorcentaje = 0;
	
	public double getAcumuladorNotaFinal() {
        double aux = acumuladorNota ;
        acumuladorNota = 0;
        return aux;
    }
	
	public double getContarNota() {
        double aux2 = contarNota;
        contarNota = 0;
        return aux2;
    }	
	
	public double getAcumuladorPorcentaje() {
		double auxPor = acumuladorPorcentaje;
		acumuladorPorcentaje = 0;
		return auxPor;
	}
	
	public void acumuladorNotasFinales(double valor)	{
		acumuladorNota += valor;
    }
	
	public void acumuladorPorcentajes(double porcentual){
	
			try {
				
				if(acumuladorPorcentaje <= 100){
					acumuladorPorcentaje += porcentual;
				}
				
			} catch (Exception excepcion) {
				//Obtener los mensajes desde el cat�logo de mensajes
				String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION");
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CREACION", mensajeUsuario));
				UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
			}
    }
	
	public void contarNotas(double valor)
    {
		contarNota = valor + 1;
    }
	
	///////////////////////////////////////////////////
	
	public ArrayList<TblNotaXEstudianteGrupo> getListaNotaXEstudianteGrupo2() {
		
		ArrayList<TblNotaXEstudianteGrupo> listaDef2 = new ArrayList<>();
		for(TblNotaXEstudianteGrupo lista : listaNotaXEstudianteGrupo){	
			listaDef2.add(lista);
			if(listaDef2.contains(notaXEstudianteGrupo.getTblNotaXGrupo().getTblTipoNota().getInCodigo())){
				listaDef2.add(lista);
			}
		}
		
		return listaDef2;
	}
	
	public ArrayList<TblTipoNota> getListTipoNota() {
		return listTipoNota;
	}

	public TblNotaXEstudianteGrupo getNotaXEstudianteGrupo() {
		return notaXEstudianteGrupo;
	}

	public void setNotaXEstudianteGrupo(TblNotaXEstudianteGrupo notaXEstudianteGrupo) {
		this.notaXEstudianteGrupo = notaXEstudianteGrupo;
	}

	public NotaXEstudianteGrupoConsultaControlador() {
		notaXEstudianteGrupo = new TblNotaXEstudianteGrupo();
		notaXEstudianteGrupo.setTblSesion(new TblSesion());
		notaXEstudianteGrupo.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		notaXEstudianteGrupo.setTblNotaXGrupo(new TblNotaXGrupo());
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	public void consultar() {
		
		
		try {
			NotaXEstudianteGrupoFachada notaXEstudianteGrupoFachada = LocalizadorBean.ObtenerBean(NotaXEstudianteGrupoFachada.class);
			listaNotaXEstudianteGrupo = notaXEstudianteGrupoFachada.consultarPorNumeroId(notaXEstudianteGrupo);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_ESTUDIANTE_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
		
	public void limpiar(){
		
		notaXEstudianteGrupo = new TblNotaXEstudianteGrupo();
		notaXEstudianteGrupo.setTblSesion(new TblSesion());
		notaXEstudianteGrupo.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		notaXEstudianteGrupo.setTblNotaXGrupo(new TblNotaXGrupo());
	}
	     
}