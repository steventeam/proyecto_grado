package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPeriodoAcademico;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPeriodoAcademico;
import co.edu.asys.gestorgrupos.negocio.fachada.PeriodoAcademicoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="periodoAcademicoBean")
@SessionScoped
public class PeriodoAcademicoControlador {
	
	private TblPeriodoAcademico periodoAcademico;
	private ArrayList<TblPeriodoAcademico> listaPeriodoAcademico;
	
		
	public ArrayList<TblPeriodoAcademico> getListaPeriodoAcademico() {
		return listaPeriodoAcademico;
	}

	public TblPeriodoAcademico getPeriodoAcademico() {
		return periodoAcademico;
	}

	public void setPeriodoAcademico(TblPeriodoAcademico periodoAcademico) {
		this.periodoAcademico = periodoAcademico;
	}

	public PeriodoAcademicoControlador() {
		periodoAcademico = new TblPeriodoAcademico();
		periodoAcademico.setInCodigo(2145888888);
		periodoAcademico.setTblTipoPeriodoAcademico(new TblTipoPeriodoAcademico());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(periodoAcademico != null){
				
				PeriodoAcademicoFachada periodoAcademicoFachada = LocalizadorBean.ObtenerBean(PeriodoAcademicoFachada.class);
				periodoAcademicoFachada.crear(periodoAcademico);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			PeriodoAcademicoFachada periodoAcademicoFachada = LocalizadorBean.ObtenerBean(PeriodoAcademicoFachada.class);
			periodoAcademicoFachada.actualizar(periodoAcademico);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			PeriodoAcademicoFachada periodoAcademicoFachada = LocalizadorBean.ObtenerBean(PeriodoAcademicoFachada.class);
			periodoAcademicoFachada.inhabilitar(periodoAcademico);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERIODO_ACADEMICO_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			PeriodoAcademicoFachada periodoAcademicoFachada = LocalizadorBean.ObtenerBean(PeriodoAcademicoFachada.class);
			listaPeriodoAcademico = periodoAcademicoFachada.consultarPorFiltro(periodoAcademico);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			PeriodoAcademicoFachada periodoAcademicoFachada = LocalizadorBean.ObtenerBean(PeriodoAcademicoFachada.class);
			listaPeriodoAcademico = periodoAcademicoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PERIODO_ACADEMICO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
		
	public void limpiar(){
		
		periodoAcademico = new TblPeriodoAcademico();
		periodoAcademico.setInCodigo(2145888888);
		periodoAcademico.setTblTipoPeriodoAcademico(new TblTipoPeriodoAcademico());

	}
	
	public ArrayList<TblPeriodoAcademico> getListaPeriodoAcademico2(){
		ArrayList<TblPeriodoAcademico> listaDef = new ArrayList<>();
		
		for(TblPeriodoAcademico lista : listaPeriodoAcademico){
			if(lista.getDaFechaFin().after(new Date())){
				listaDef.add(lista);
			}
		}
		
		return listaDef;
		
	}
		
}