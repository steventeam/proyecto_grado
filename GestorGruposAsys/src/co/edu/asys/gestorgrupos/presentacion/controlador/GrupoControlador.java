package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.GrupoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="grupoBean")
@SessionScoped
public class GrupoControlador {
	
	private TblGrupo grupo;
	private ArrayList<TblGrupo> listaGrupo;
	private Map<String, String> dias = new LinkedHashMap<String, String>();
	
	public Map<String, String> getDias() {
		return dias;
	}

	public ArrayList<TblGrupo> getListaGrupo() {
		return listaGrupo;
	}

	public TblGrupo getGrupo() {
		return grupo;
	}

	public void setGrupo(TblGrupo grupo) {
		this.grupo = grupo;
	}

	public GrupoControlador() {
		grupo = new TblGrupo();
//		grupo.setInCodigo(21343245);
		grupo.setTblMateriaXPensum(new TblMateriaXPensum());
		grupo.setTblPeriodoAcademico(new TblPeriodoAcademico());
		grupo.setTblProfesorXInstitucion(new TblProfesorXInstitucion());
		consultarTodos();
	}
	
	@PostConstruct
    public void dia() {
        
        //dias
		dias = new LinkedHashMap<String, String>();
		dias.put("Lunes", "Lunes");
		dias.put("Martes", "Martes");
		dias.put("Mi�rcoles", "Mi�rcoles");
		dias.put("Jueves", "Jueves");
		dias.put("Viernes", "Viernes");
		dias.put("S�bado", "S�bado");
		dias.put("Domingo", "Domingo");
		
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(grupo !=null){
//				if(grupo.getTblMateriaXPensum().getTblMateria().getNvNombre() != "" && (grupo.getTblMateriaXPensum().getTblMateriaXPensum().getInCodigo() == 0 || grupo.getTblMateriaXPensum().getTblMateriaXPensum().getInCodigo() == null)){
//					System.out.println("Materia disponible");
				if(grupo.getInCantidadMinima() >= 5 && grupo.getInCantidadMaxima() <= 30){
					if(grupo.getTiHoraFin().after(grupo.getTiHoraInicio())){					
						GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
						grupoFachada.crear(grupo);
						gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_EXITO_CREACION"));
					} else {
						gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION"));
					}
				}
				
			}
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
			grupoFachada.actualizar(grupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
			grupoFachada.inhabilitar(grupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_GRUPO_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
			listaGrupo = grupoFachada.consultarPorFiltro(grupo);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
			listaGrupo = grupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		grupo = new TblGrupo();
		grupo.setInCodigo(2147483646);
		grupo.setTblMateriaXPensum(new TblMateriaXPensum());
		grupo.setTblPeriodoAcademico(new TblPeriodoAcademico());
		grupo.setTblProfesorXInstitucion(new TblProfesorXInstitucion());
	}
	
}