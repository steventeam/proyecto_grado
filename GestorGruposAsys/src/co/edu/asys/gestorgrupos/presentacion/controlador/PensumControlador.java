package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensum;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.negocio.fachada.PensumFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="pensumBean")
@SessionScoped
public class PensumControlador {
	
	private TblPensum pensum;
	private ArrayList<TblPensum> listaPensum;
	
		
	public ArrayList<TblPensum> getListaPensum() {
		return listaPensum;
	}

	public TblPensum getPensum() {
		return pensum;
	}

	public void setPensum(TblPensum pensum) {
		this.pensum = pensum;
	}

	public PensumControlador() {
		pensum = new TblPensum();
		pensum.setTblPrograma(new TblPrograma());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(pensum != null){
				
				PensumFachada pensumFachada = LocalizadorBean.ObtenerBean(PensumFachada.class);
				pensumFachada.crear(pensum);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_EXITO_CREACION", pensum.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			PensumFachada pensumFachada = LocalizadorBean.ObtenerBean(PensumFachada.class);
			pensumFachada.actualizar(pensum);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_EXITO_ACTUALIZACION", pensum.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			PensumFachada pensumFachada = LocalizadorBean.ObtenerBean(PensumFachada.class);
			pensumFachada.inhabilitar(pensum);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PENSUM_EXITO_ELIMINACION", pensum.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			PensumFachada pensumFachada = LocalizadorBean.ObtenerBean(PensumFachada.class);
			listaPensum = pensumFachada.consultarPorFiltro(pensum);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			PensumFachada pensumFachada = LocalizadorBean.ObtenerBean(PensumFachada.class);
			listaPensum = pensumFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PENSUM_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		pensum = new TblPensum();
		pensum.setTblPrograma(new TblPrograma());
	}
	
}