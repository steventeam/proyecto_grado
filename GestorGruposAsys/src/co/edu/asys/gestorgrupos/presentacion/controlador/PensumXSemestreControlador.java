package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensum;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblPensumXSemestre;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSemestre;
import co.edu.asys.gestorgrupos.negocio.fachada.PensumXSemestreFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="pensumXSemestreBean")
@SessionScoped
public class PensumXSemestreControlador {
	
	private TblPensumXSemestre pensumXSemestre;
	private ArrayList<TblPensumXSemestre> listaPensumXSemestre;
	
		
	public ArrayList<TblPensumXSemestre> getListaPensumXSemestre() {
		return listaPensumXSemestre;
	}

	public TblPensumXSemestre getPensumXSemestre() {
		return pensumXSemestre;
	}

	public void setPensumXSemestre(TblPensumXSemestre pensumXSemestre) {
		this.pensumXSemestre = pensumXSemestre;
	}

	public PensumXSemestreControlador() {
		pensumXSemestre = new TblPensumXSemestre();
		pensumXSemestre.setTblPensum(new TblPensum());
		pensumXSemestre.setTblSemestre(new TblSemestre());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		try {
			
			if (pensumXSemestre != null) {
			// Lo que hay que validar
			// 1. Obligatoriedad
			// 2. Formato
			// 3. Tipo del dato
			// 4. Longitud
			
			PensumXSemestreFachada pensumXSemestreFachada = LocalizadorBean.ObtenerBean(PensumXSemestreFachada.class);
			
			pensumXSemestreFachada.crear(pensumXSemestre);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_CREACION"));
			
			Logger.getLogger("miLogger").trace("Haciendo trace");
			Logger.getLogger("miLogger").debug("Haciendo debug");
			Logger.getLogger("miLogger").info("Haciendo info");
			Logger.getLogger("miLogger").warn("Haciendo warn");
			Logger.getLogger("miLogger").error("Haciendo error");
			Logger.getLogger("miLogger").fatal("Haciendo fatal");
			}
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error creando tipo de identificación", "Se ha presentado un error tratando de registrar la información de nuevo tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
			
		}
		finally {
			consultarTodos();
			limpiar();
		}
		
	}
	
	public void actualizar() {
			try {
				PensumXSemestreFachada pensumXSemestreFachada = LocalizadorBean.ObtenerBean(PensumXSemestreFachada.class);
				pensumXSemestreFachada.actualizar(pensumXSemestre);
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_ACTUALIZACION", pensumXSemestre.getInCodigo()));
				
				Logger.getLogger("miLogger").trace("Haciendo trace");
				Logger.getLogger("miLogger").debug("Haciendo debug");
				Logger.getLogger("miLogger").info("Haciendo info");
				Logger.getLogger("miLogger").warn("Haciendo warn");
				Logger.getLogger("miLogger").error("Haciendo error");
				Logger.getLogger("miLogger").fatal("Haciendo fatal");
				
			} catch (Exception excepcion) {
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error actualizando tipo de identificación", "Se ha presentado un error tratando de actualizar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
				excepcion.printStackTrace();
			}
			finally {
				consultarTodos();
				limpiar();
			}
		}
	
	public void inhabilitar() {
		try {
			PensumXSemestreFachada pensumXSemestreFachada = LocalizadorBean.ObtenerBean(PensumXSemestreFachada.class);
			pensumXSemestreFachada.inhabilitar(pensumXSemestre);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "Éxito eliminando tipo de identificación", "Se ha eliminado la información de manera exitosa");
			
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error eliminando tipo de identificación", "Se ha presentado un error tratando de dar de baja la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
			finally {
				consultarTodos();
				limpiar();
			}
	}
	
	public void consultar() {
		
		
		try {
			PensumXSemestreFachada pensumXSemestreFachada = LocalizadorBean.ObtenerBean(PensumXSemestreFachada.class);
			listaPensumXSemestre = pensumXSemestreFachada.consultarPorFiltro(pensumXSemestre);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
				
		
	}
	
	public void limpiar(){
		
		pensumXSemestre = new TblPensumXSemestre();
		pensumXSemestre.setTblPensum(new TblPensum());
		pensumXSemestre.setTblSemestre(new TblSemestre());
	}
	
	private void consultarTodos() {
	
		try {
			PensumXSemestreFachada pensumXSemestreFachada = LocalizadorBean.ObtenerBean(PensumXSemestreFachada.class);
			listaPensumXSemestre = pensumXSemestreFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
		
	}
	
}