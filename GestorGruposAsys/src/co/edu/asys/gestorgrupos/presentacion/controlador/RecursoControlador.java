package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegRecurso;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegTipoRecurso;
import co.edu.asys.gestorgrupos.negocio.fachada.RecursoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="recursoBean")
@SessionScoped
public class RecursoControlador {
	
	private SegRecurso recurso;
	private ArrayList<SegRecurso> listaRecurso;
	
		
	public ArrayList<SegRecurso> getListaRecurso() {
		return listaRecurso;
	}

	public SegRecurso getRecurso() {
		return recurso;
	}

	public void setRecurso(SegRecurso recurso) {
		this.recurso = recurso;
	}

	public RecursoControlador() {
		recurso = new SegRecurso();
		recurso.setSegAplicacion(new SegAplicacion());
		recurso.setSegTipoRecurso(new SegTipoRecurso());
		
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(recurso != null){
				
				RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
				recursoFachada.crear(recurso);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_EXITO_CREACION", recurso.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
			recursoFachada.actualizar(recurso);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_EXITO_ACTUALIZACION", recurso.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
			recursoFachada.inhabilitar(recurso);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_EXITO_ELIMINACION", recurso.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
			listaRecurso = recursoFachada.consultarPorFiltro(recurso);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
			listaRecurso = recursoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		recurso = new SegRecurso();
		recurso.setSegAplicacion(new SegAplicacion());
		recurso.setSegTipoRecurso(new SegTipoRecurso());
		
	}
	
	public void onRowEdit(RowEditEvent event) {
		
        SegRecurso recurso= (SegRecurso) event.getObject();
        
        try {
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);
			
			recursoFachada.actualizar(recurso);

			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_EXITO_ACTUALIZACION", recurso.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeRecurso = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_ACTUALIZACION", mensajeRecurso));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		} finally {
			consultarTodos();
			limpiar();
		}
    }
    
    public void onRowCancel(RowEditEvent event) {
    	FacesContext.getCurrentInstance();
    	
    }
    
    public void habilitar() {
		
		try {
		
			RecursoFachada recursoFachada = LocalizadorBean.ObtenerBean(RecursoFachada.class);

			recursoFachada.habilitar(recurso);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "�xito habilitando recurso", "Se ha habilitado exitosamente el recurso.");
					
			} catch (Exception exception) {
			
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error en la habilitaci�n del recurso", "Se ha presentado un error tratando de habilitar el recurso. Por favor intente de nuevo y si el problema persiste contacte al administrador de la aplicaci�n");
				exception.printStackTrace();
			} finally {
				consultarTodos();
				limpiar();
			}
	}
    
    public ArrayList<SegRecurso> getListaRecurso2() {
    	ArrayList<SegRecurso> listaDef = new ArrayList<>();
    	for(SegRecurso lista: listaRecurso){
    		if(lista.getDtFechaEliminacion() == null){
    			listaDef.add(lista);
    		}
    	}
    	
		return listaDef;
    	
    }
	
}