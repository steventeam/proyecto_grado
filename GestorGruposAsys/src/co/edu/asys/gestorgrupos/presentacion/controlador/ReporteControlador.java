package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblReporte;
import co.edu.asys.gestorgrupos.negocio.fachada.ReporteFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="reporteBean")
@SessionScoped
public class ReporteControlador {
	
	private TblReporte reporte;
	private ArrayList<TblReporte> listaReporte;
	
		
	public ArrayList<TblReporte> getListaReporte() {
		return listaReporte;
	}

	public TblReporte getReporte() {
		return reporte;
	}

	public void setReporte(TblReporte reporte) {
		this.reporte = reporte;
	}

	public ReporteControlador() {
		reporte = new TblReporte();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		try {
			
			if (reporte != null) {
			// Lo que hay que validar
			// 1. Obligatoriedad
			// 2. Formato
			// 3. Tipo del dato
			// 4. Longitud
			
			ReporteFachada reporteFachada = LocalizadorBean.ObtenerBean(ReporteFachada.class);
			
			reporteFachada.crear(reporte);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_CREACION", reporte.getNvDescripcion()));
			
			}
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error creando tipo de identificación", "Se ha presentado un error tratando de registrar la información de nuevo tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
			
		}
		finally {
			consultarTodos();
			limpiar();
		}
		
	}
	
	public void actualizar() {
			try {
				ReporteFachada reporteFachada = LocalizadorBean.ObtenerBean(ReporteFachada.class);
				reporteFachada.actualizar(reporte);
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_IDENTIFICACION_EXITO_ACTUALIZACION", reporte.getNvDescripcion(), reporte.getInCodigo()));
				
			} catch (Exception excepcion) {
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error actualizando tipo de identificación", "Se ha presentado un error tratando de actualizar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
				excepcion.printStackTrace();
			}
			finally {
				consultarTodos();
				limpiar();
			}
		}
	
	public void inhabilitar() {
		try {
			ReporteFachada reporteFachada = LocalizadorBean.ObtenerBean(ReporteFachada.class);
			reporteFachada.inhabilitar(reporte);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "Éxito eliminando tipo de identificación", "Se ha eliminado la información de manera exitosa");
			
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error eliminando tipo de identificación", "Se ha presentado un error tratando de dar de baja la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
			finally {
				consultarTodos();
				limpiar();
			}
	}
	
	public void consultar() {
		
		
		try {
			ReporteFachada reporteFachada = LocalizadorBean.ObtenerBean(ReporteFachada.class);
			listaReporte = reporteFachada.consultarPorFiltro(reporte);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
				
		
	}
	
	public void limpiar(){

		reporte = new TblReporte();
	}
	
	private void consultarTodos() {
	
		try {
			ReporteFachada reporteFachada = LocalizadorBean.ObtenerBean(ReporteFachada.class);
			listaReporte = reporteFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
		
	}
	
}