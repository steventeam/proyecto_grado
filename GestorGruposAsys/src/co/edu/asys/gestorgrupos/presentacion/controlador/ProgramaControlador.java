package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblPrograma;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPrograma;
import co.edu.asys.gestorgrupos.negocio.fachada.ProgramaFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="programaBean")
@SessionScoped
public class ProgramaControlador {
	
	private TblPrograma programa;
	private ArrayList<TblPrograma> listaPrograma;
	
		
	public ArrayList<TblPrograma> getListaPrograma() {
		return listaPrograma;
	}

	public TblPrograma getPrograma() {
		return programa;
	}

	public void setPrograma(TblPrograma programa) {
		this.programa = programa;
	}

	public ProgramaControlador() {
		programa = new TblPrograma();
		programa.setTblTipoPrograma(new TblTipoPrograma());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(programa != null){
				
				ProgramaFachada programaFachada = LocalizadorBean.ObtenerBean(ProgramaFachada.class);
				programaFachada.crear(programa);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_EXITO_CREACION", programa.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			ProgramaFachada programaFachada = LocalizadorBean.ObtenerBean(ProgramaFachada.class);
			programaFachada.actualizar(programa);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_EXITO_ACTUALIZACION", programa.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			ProgramaFachada programaFachada = LocalizadorBean.ObtenerBean(ProgramaFachada.class);
			programaFachada.inhabilitar(programa);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROGRAMA_EXITO_ELIMINACION", programa.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			ProgramaFachada programaFachada = LocalizadorBean.ObtenerBean(ProgramaFachada.class);
			listaPrograma = programaFachada.consultarPorFiltro(programa);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			ProgramaFachada programaFachada = LocalizadorBean.ObtenerBean(ProgramaFachada.class);
			listaPrograma = programaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PROGRAMA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}

	public void limpiar(){
		
		programa = new TblPrograma();
		programa.setTblTipoPrograma(new TblTipoPrograma());
	}
	
}