package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblGenero;
import co.edu.asys.gestorgrupos.negocio.fachada.GeneroFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="generoBean")
@SessionScoped
public class GeneroControlador {
	
	private TblGenero genero;
	private ArrayList<TblGenero> listaGenero;
	
		
	public ArrayList<TblGenero> getListaGenero() {
		return listaGenero;
	}

	public TblGenero getGenero() {
		return genero;
	}

	public void setGenero(TblGenero genero) {
		this.genero = genero;
	}

	public GeneroControlador() {
		genero = new TblGenero();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		try {
			
			if (genero != null) {
			
				GeneroFachada generoFachada = LocalizadorBean.ObtenerBean(GeneroFachada.class);
				generoFachada.crear(genero);
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_GENERO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_GENERO_EXITO_CREACION", genero.getNvNombre()));
			}
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error creando g�nero", "Se ha presentado un error tratando de registrar la informaci�n de nuevo g�nero. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicaci�n");
			excepcion.printStackTrace();
			
		}
		finally {
			consultarTodos();
			limpiar();
		}
		
	}
	
	public void actualizar() {
			try {
				GeneroFachada generoFachada = LocalizadorBean.ObtenerBean(GeneroFachada.class);
				generoFachada.actualizar(genero);
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_GENERO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_GENERO_EXITO_ACTUALIZACION", genero.getNvNombre(), genero.getInCodigo()));
				
			} catch (Exception excepcion) {
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error actualizando g�nero", "Se ha presentado un error tratando de actualizar la informaci�n de g�nero. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicaci�n");
				excepcion.printStackTrace();
			}
			finally {
				consultarTodos();
				limpiar();
			}
		}
	
	public void inhabilitar() {
		try {
			GeneroFachada generoFachada = LocalizadorBean.ObtenerBean(GeneroFachada.class);
			generoFachada.inhabilitar(genero);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "�xito eliminando g�nero", "Se ha eliminado la informaci�n de manera exitosa");
			
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error eliminando g�nero", "Se ha presentado un error tratando de dar de baja la informaci�n de g�nero. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicaci�n");
			excepcion.printStackTrace();
		}
			finally {
				consultarTodos();
				limpiar();
			}
	}
	
	public void consultar() {
		
		
		try {
			GeneroFachada generoFachada = LocalizadorBean.ObtenerBean(GeneroFachada.class);
			listaGenero = generoFachada.consultarPorFiltro(genero);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando g�nero", "Se ha presentado un error tratando de consultar la informaci�n de g�nero. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicaci�n");
			excepcion.printStackTrace();
		}
				
		
	}
	
	public void limpiar(){
		
		genero = new TblGenero();
	}
	
	private void consultarTodos() {
	
		try {
			GeneroFachada generoFachada = LocalizadorBean.ObtenerBean(GeneroFachada.class);
			listaGenero = generoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando g�nero", "Se ha presentado un error tratando de consultar la informaci�n de g�nero. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicaci�n");
			excepcion.printStackTrace();
		}
		
	}
	
}