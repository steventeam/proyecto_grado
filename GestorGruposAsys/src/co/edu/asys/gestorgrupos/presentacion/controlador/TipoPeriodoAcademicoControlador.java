package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoPeriodoAcademico;
import co.edu.asys.gestorgrupos.negocio.fachada.TipoPeriodoAcademicoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="tipoPeriodoAcademicoBean")
@SessionScoped
public class TipoPeriodoAcademicoControlador {
	
	private TblTipoPeriodoAcademico tipoPeriodoAcademico;
	private ArrayList<TblTipoPeriodoAcademico> listaTipoPeriodoAcademico;
	
		
	public ArrayList<TblTipoPeriodoAcademico> getListaTipoPeriodoAcademico() {
		return listaTipoPeriodoAcademico;
	}

	public TblTipoPeriodoAcademico getTipoPeriodoAcademico() {
		return tipoPeriodoAcademico;
	}

	public void setTipoPeriodoAcademico(TblTipoPeriodoAcademico tipoPeriodoAcademico) {
		this.tipoPeriodoAcademico = tipoPeriodoAcademico;
	}

	public TipoPeriodoAcademicoControlador() {
		tipoPeriodoAcademico = new TblTipoPeriodoAcademico();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(tipoPeriodoAcademico != null){
				
				TipoPeriodoAcademicoFachada tipoPeriodoAcademicoFachada = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoFachada.class);
				tipoPeriodoAcademicoFachada.crear(tipoPeriodoAcademico);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_EXITO_CREACION", tipoPeriodoAcademico.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			TipoPeriodoAcademicoFachada tipoPeriodoAcademicoFachada = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoFachada.class);
			tipoPeriodoAcademicoFachada.actualizar(tipoPeriodoAcademico);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_EXITO_ACTUALIZACION", tipoPeriodoAcademico.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			TipoPeriodoAcademicoFachada tipoPeriodoAcademicoFachada = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoFachada.class);
			tipoPeriodoAcademicoFachada.inhabilitar(tipoPeriodoAcademico);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_TIPO_PERIODO_ACADEMICO_EXITO_ELIMINACION", tipoPeriodoAcademico.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			TipoPeriodoAcademicoFachada tipoPeriodoAcademicoFachada = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoFachada.class);
			listaTipoPeriodoAcademico = tipoPeriodoAcademicoFachada.consultarPorFiltro(tipoPeriodoAcademico);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			TipoPeriodoAcademicoFachada tipoPeriodoAcademicoFachada = LocalizadorBean.ObtenerBean(TipoPeriodoAcademicoFachada.class);
			listaTipoPeriodoAcademico = tipoPeriodoAcademicoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_TIPO_PERIODO_ACADEMICO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		tipoPeriodoAcademico = new TblTipoPeriodoAcademico();
	}
		
}