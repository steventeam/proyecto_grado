package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblInstitucion;
import co.edu.asys.gestorgrupos.negocio.fachada.InstitucionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="institucionBean")
@SessionScoped
public class InstitucionControlador {
	
	private TblInstitucion institucion;
	private ArrayList<TblInstitucion> listaInstitucion;
	
		
	public ArrayList<TblInstitucion> getListaInstitucion() {
		return listaInstitucion;
	}

	public TblInstitucion getInstitucion() {
		return institucion;
	}

	public void setInstitucion(TblInstitucion institucion) {
		this.institucion = institucion;
	}

	public InstitucionControlador() {
		institucion = new TblInstitucion();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
public void crear() {
		
		try {
			
			if(institucion != null){
				InstitucionFachada institucionFachada = LocalizadorBean.ObtenerBean(InstitucionFachada.class);
				institucionFachada.crear(institucion);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_EXITO_CREACION", institucion.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			InstitucionFachada institucionFachada = LocalizadorBean.ObtenerBean(InstitucionFachada.class);
			institucionFachada.actualizar(institucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_EXITO_ACTUALIZACION", institucion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			InstitucionFachada institucionFachada = LocalizadorBean.ObtenerBean(InstitucionFachada.class);
			institucionFachada.inhabilitar(institucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_INSTITUCION_EXITO_ELIMINACION", institucion.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			InstitucionFachada institucionFachada = LocalizadorBean.ObtenerBean(InstitucionFachada.class);
			listaInstitucion = institucionFachada.consultarPorFiltro(institucion);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			InstitucionFachada institucionFachada = LocalizadorBean.ObtenerBean(InstitucionFachada.class);
			listaInstitucion = institucionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_INSTITUCION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		institucion = new TblInstitucion();
	}
		
}