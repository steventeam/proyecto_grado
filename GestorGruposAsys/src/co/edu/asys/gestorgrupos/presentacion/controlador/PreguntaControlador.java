package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegPregunta;
import co.edu.asys.gestorgrupos.negocio.fachada.PreguntaFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="preguntaBean")
@SessionScoped
public class PreguntaControlador {
	
	private SegPregunta pregunta;
	private ArrayList<SegPregunta> listaPregunta;
	
		
	public ArrayList<SegPregunta> getListaPregunta() {
		return listaPregunta;
	}

	public SegPregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(SegPregunta pregunta) {
		this.pregunta = pregunta;
	}

	public PreguntaControlador() {
		pregunta = new SegPregunta();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(pregunta != null){
				
				PreguntaFachada preguntaFachada = LocalizadorBean.ObtenerBean(PreguntaFachada.class);
				preguntaFachada.crear(pregunta);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			PreguntaFachada preguntaFachada = LocalizadorBean.ObtenerBean(PreguntaFachada.class);
			preguntaFachada.actualizar(pregunta);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			PreguntaFachada preguntaFachada = LocalizadorBean.ObtenerBean(PreguntaFachada.class);
			preguntaFachada.inhabilitar(pregunta);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			PreguntaFachada preguntaFachada = LocalizadorBean.ObtenerBean(PreguntaFachada.class);
			listaPregunta = preguntaFachada.consultarPorFiltro(pregunta);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			PreguntaFachada preguntaFachada = LocalizadorBean.ObtenerBean(PreguntaFachada.class);
			listaPregunta = preguntaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){

		pregunta = new SegPregunta();
	}
	
}