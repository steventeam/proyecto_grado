package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegAplicacion;
import co.edu.asys.gestorgrupos.entidad.seguridad.SegPerfil;
import co.edu.asys.gestorgrupos.negocio.fachada.PerfilFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="perfilBean")
@SessionScoped
public class PerfilControlador {
	
	private SegPerfil perfil;
	private ArrayList<SegPerfil> listaPerfil;
	
		
	public ArrayList<SegPerfil> getListaPerfil() {
		return listaPerfil;
	}

	public SegPerfil getPerfil() {
		return perfil;
	}

	public void setPerfil(SegPerfil perfil) {
		this.perfil = perfil;
	}

	public PerfilControlador() {
		perfil = new SegPerfil();
		perfil.setSegAplicacion(new SegAplicacion());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(perfil != null){
				
				PerfilFachada perfilFachada = LocalizadorBean.ObtenerBean(PerfilFachada.class);
				perfilFachada.crear(perfil);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_EXITO_CREACION", perfil.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			PerfilFachada perfilFachada = LocalizadorBean.ObtenerBean(PerfilFachada.class);
			perfilFachada.actualizar(perfil);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_EXITO_ACTUALIZACION", perfil.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			PerfilFachada perfilFachada = LocalizadorBean.ObtenerBean(PerfilFachada.class);
			perfilFachada.inhabilitar(perfil);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PERFIL_EXITO_ELIMINACION", perfil.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			PerfilFachada perfilFachada = LocalizadorBean.ObtenerBean(PerfilFachada.class);
			listaPerfil = perfilFachada.consultarPorFiltro(perfil);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			PerfilFachada perfilFachada = LocalizadorBean.ObtenerBean(PerfilFachada.class);
			listaPerfil = perfilFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PERFIL_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		perfil = new SegPerfil();
		perfil.setSegAplicacion(new SegAplicacion());
	}
	
}