package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateria;
import co.edu.asys.gestorgrupos.negocio.fachada.MateriaFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="materiaBean")
@SessionScoped
public class MateriaControlador {
	
	private TblMateria materia;
	private ArrayList<TblMateria> listaMateria;
	
		
	public ArrayList<TblMateria> getListaMateria() {
		return listaMateria;
	}

	public TblMateria getMateria() {
		return materia;
	}

	public void setMateria(TblMateria materia) {
		this.materia = materia;
	}

	public MateriaControlador() {
		materia = new TblMateria();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
public void crear() {
		
		try {
			
			if(materia != null){
			
				MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
				materiaFachada.crear(materia);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_EXITO_CREACION", materia.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
			materiaFachada.actualizar(materia);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_EXITO_ACTUALIZACION", materia.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
			materiaFachada.inhabilitar(materia);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_EXITO_ELIMINACION", materia.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
			listaMateria = materiaFachada.consultarPorFiltro(materia);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
			listaMateria = materiaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void actualizarFila(RowEditEvent event) {
		
        TblMateria materia = (TblMateria) event.getObject();
        
        try {
			MateriaFachada materiaFachada = LocalizadorBean.ObtenerBean(MateriaFachada.class);
			
			materiaFachada.actualizar(materia);

			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_EXITO_ACTUALIZACION", materia.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		} finally {
			consultarTodos();
			limpiar();
		}
    }
    
    public void onRowCancel(RowEditEvent event) {
    	FacesContext.getCurrentInstance();
    	
    }
	
	public ArrayList<TblMateria> getListaMateria2() {
		ArrayList<TblMateria> listaDef = new ArrayList<>();
		for(TblMateria lista : listaMateria){	
			if(lista.getInCodigo() != 0){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
	
	public void limpiar(){
	
		materia = new TblMateria();
	}
	
}