package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.NotaXGrupoFachada;
import co.edu.asys.gestorgrupos.negocio.fachada.UsuarioFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="notaXGrupoBean")
@SessionScoped
public class NotaXGrupoControlador {
	
	private TblNotaXGrupo notaXGrupo;
	private ArrayList<TblNotaXGrupo> listaNotaXGrupo;
	
	
	public ArrayList<TblNotaXGrupo> getListaNotaXGrupo() {
		return listaNotaXGrupo;
	}

	public TblNotaXGrupo getNotaXGrupo() {
		return notaXGrupo;
	}

	public void setNotaXGrupo(TblNotaXGrupo notaXGrupo) {
		this.notaXGrupo = notaXGrupo;
	}

	public NotaXGrupoControlador() {
		notaXGrupo = new TblNotaXGrupo();
		notaXGrupo.setInCodigo(2145888888);
		notaXGrupo.setTblGrupo(new TblGrupo());
		notaXGrupo.setTblTipoNota(new TblTipoNota());

		consultarTodos();
	}
	
	//////////////////////////
	private double acumuladorPorcentaje = 0;
	
	public double getAcumuladorPorcentaje() {
        double aux = acumuladorPorcentaje ;
        acumuladorPorcentaje = 0;
        return aux;
    }
	
	public void acumuladorPorcentajes(double valor)
    {
		acumuladorPorcentaje += valor;
    }	
	
	/////////////////////////
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	public void crear() {
		
		try {
			if (notaXGrupo != null) {
				if (notaXGrupo.getDaFechaRegistro().after(new Date())) {
					
					NotaXGrupoFachada notaXGrupoFachada = LocalizadorBean.ObtenerBean(NotaXGrupoFachada.class);
					notaXGrupoFachada.crear(notaXGrupo);
						
					gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_EXITO_CREACION"));
				}
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			NotaXGrupoFachada notaXGrupoFachada = LocalizadorBean.ObtenerBean(NotaXGrupoFachada.class);
			notaXGrupoFachada.actualizar(notaXGrupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			NotaXGrupoFachada notaXGrupoFachada = LocalizadorBean.ObtenerBean(NotaXGrupoFachada.class);
			notaXGrupoFachada.inhabilitar(notaXGrupo);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			NotaXGrupoFachada notaXGrupoFachada = LocalizadorBean.ObtenerBean(NotaXGrupoFachada.class);
			listaNotaXGrupo = notaXGrupoFachada.consultarPorFiltro(notaXGrupo);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			NotaXGrupoFachada notaXGrupoFachada = LocalizadorBean.ObtenerBean(NotaXGrupoFachada.class);
			listaNotaXGrupo = notaXGrupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}	
	public void limpiar(){
		
		notaXGrupo = new TblNotaXGrupo();
		notaXGrupo.setInCodigo(2145888888);
		notaXGrupo.setTblGrupo(new TblGrupo());
		notaXGrupo.setTblTipoNota(new TblTipoNota());

	}
	
	public void onRowEdit(RowEditEvent event) {
		
        SegUsuario usuario = (SegUsuario) event.getObject();
        
        try {
			UsuarioFachada usuarioFachada = LocalizadorBean.ObtenerBean(UsuarioFachada.class);
			
			usuarioFachada.actualizar(usuario);

			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_NOTA_X_GRUPO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_NOTA_X_GRUPO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		} finally {
			consultarTodos();
			limpiar();
		}
    }
    
    public void onRowCancel(RowEditEvent event) {
    	FacesContext.getCurrentInstance();
    	
    }
		
    public ArrayList<TblNotaXGrupo> getListaNotaXGrupo2() {
		ArrayList<TblNotaXGrupo> listaDef = new ArrayList<>();
		for(TblNotaXGrupo lista : listaNotaXGrupo){	
			if((lista.getDaFechaLimiteRegistro().after(new Date())) && (lista.getDaFechaLimiteRegistro().after(lista.getDaFechaRegistro()))){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
}