package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.primefaces.event.RowEditEvent;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.EstudianteXInstitucionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="estudianteXInstitucionBean")
public class EstudianteXInstitucionControlador {
	
	private TblEstudianteXInstitucion estudianteXInstitucion;
	private ArrayList<TblEstudianteXInstitucion> listaEstudianteXInstitucion;
	
		
	public ArrayList<TblEstudianteXInstitucion> getListaEstudianteXInstitucion() {
		return listaEstudianteXInstitucion;
	}

	public TblEstudianteXInstitucion getEstudianteXInstitucion() {
		return estudianteXInstitucion;
	}

	public void setEstudianteXInstitucion(TblEstudianteXInstitucion estudianteXInstitucion) {
		this.estudianteXInstitucion = estudianteXInstitucion;
	}

	public EstudianteXInstitucionControlador() {
		estudianteXInstitucion = new TblEstudianteXInstitucion();
		estudianteXInstitucion.setInCodigo(2133214124);
		estudianteXInstitucion.setTblInstitucion(new TblInstitucion());
		estudianteXInstitucion.setSegUsuario(new SegUsuario());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	public void crear() {
		
		try {
			if (estudianteXInstitucion != null) {
				EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
				estudianteXInstitucionFachada.crear(estudianteXInstitucion);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_EXITO_CREACION"));
			}	
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
			estudianteXInstitucionFachada.actualizar(estudianteXInstitucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizarFila(RowEditEvent editar){
		
		TblEstudianteXInstitucion estudianteXInstitucion = (TblEstudianteXInstitucion) editar.getObject();
		
		try {
			
			EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
			
			estudianteXInstitucionFachada.actualizar(estudianteXInstitucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
			estudianteXInstitucionFachada.inhabilitar(estudianteXInstitucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
			listaEstudianteXInstitucion = estudianteXInstitucionFachada.consultarPorFiltro(estudianteXInstitucion);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			EstudianteXInstitucionFachada estudianteXInstitucionFachada = LocalizadorBean.ObtenerBean(EstudianteXInstitucionFachada.class);
			listaEstudianteXInstitucion = estudianteXInstitucionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		//1. A pedal
		estudianteXInstitucion.setInCodigo(null);
		
		//2. Mejor
		estudianteXInstitucion = new TblEstudianteXInstitucion();
//		estudianteXInstitucion.setTblEstudianteXInstitucion(new TblEstudianteXInstitucion());
		estudianteXInstitucion.setSegUsuario(new SegUsuario());
		estudianteXInstitucion.setTblInstitucion(new TblInstitucion());
	}

//	@PostConstruct
//	public void init() {
//		if(estudianteXInstitucion.getSegUsuario().getSegPerfil().getNvNombre() == "estudiante" || estudianteXInstitucion.getSegUsuario().getSegPerfil().getNvNombre() == "Estudiante"){
//		    ArrayList<TblEstudianteXInstitucion> list = getListaEstudianteXInstitucion();
//		}
//	}
//	
	public ArrayList<TblEstudianteXInstitucion> getListaUsuario2() {
		ArrayList<TblEstudianteXInstitucion> listaDef = new ArrayList<>();
		for(TblEstudianteXInstitucion lista : listaEstudianteXInstitucion){	
			if(lista.getSegUsuario().getSegPerfil().getNvNombre() == "estudiante" || lista.getSegUsuario().getSegPerfil().getNvNombre() == "Estudiante"){
				listaDef.add(lista);
			}
		}
		return listaDef;
		
	}
	
}