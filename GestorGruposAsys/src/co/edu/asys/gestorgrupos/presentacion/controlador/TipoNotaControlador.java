package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblTipoNota;
import co.edu.asys.gestorgrupos.negocio.fachada.TipoNotaFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="tipoNotaBean")
@SessionScoped
public class TipoNotaControlador {
	
	private TblTipoNota tipoNota;
	private ArrayList<TblTipoNota> listaTipoNota;
	
		
	public ArrayList<TblTipoNota> getListaTipoNota() {
		return listaTipoNota;
	}

	public TblTipoNota getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(TblTipoNota tipoNota) {
		this.tipoNota = tipoNota;
	}

	public TipoNotaControlador() {
		tipoNota = new TblTipoNota();
		consultarTodos();
	}
	
	public int getSizeColumn() {
		
		return listaTipoNota.size();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(tipoNota != null){
				
				TipoNotaFachada tipoNotaFachada = LocalizadorBean.ObtenerBean(TipoNotaFachada.class);
				tipoNotaFachada.crear(tipoNota);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_CREACION", tipoNota.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			TipoNotaFachada tipoNotaFachada = LocalizadorBean.ObtenerBean(TipoNotaFachada.class);
			tipoNotaFachada.actualizar(tipoNota);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_ACTUALIZACION", tipoNota.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			TipoNotaFachada tipoNotaFachada = LocalizadorBean.ObtenerBean(TipoNotaFachada.class);
			tipoNotaFachada.inhabilitar(tipoNota);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_ELIMINACION", tipoNota.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			TipoNotaFachada tipoNotaFachada = LocalizadorBean.ObtenerBean(TipoNotaFachada.class);
			listaTipoNota = tipoNotaFachada.consultarPorFiltro(tipoNota);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			TipoNotaFachada tipoNotaFachada = LocalizadorBean.ObtenerBean(TipoNotaFachada.class);
			listaTipoNota = tipoNotaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		//1. A pedal
		tipoNota.setInCodigo(null);
		tipoNota.setNvNombre(null);
		
		//2. Mejor
		tipoNota = new TblTipoNota();
	}
		
}