package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.SegUsuario;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblInstitucion;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblProfesorXInstitucion;
import co.edu.asys.gestorgrupos.negocio.fachada.ProfesorXInstitucionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="profesorXInstitucionBean")
@SessionScoped
public class ProfesorXInstitucionControlador {
	
	private TblProfesorXInstitucion profesorXInstitucion;
	private ArrayList<TblProfesorXInstitucion> listaProfesorXInstitucion;
	
		
	public ArrayList<TblProfesorXInstitucion> getListaProfesorXInstitucion() {
		return listaProfesorXInstitucion;
	}

	public TblProfesorXInstitucion getProfesorXInstitucion() {
		return profesorXInstitucion;
	}

	public void setProfesorXInstitucion(TblProfesorXInstitucion profesorXInstitucion) {
		this.profesorXInstitucion = profesorXInstitucion;
	}

	public ProfesorXInstitucionControlador() {
		profesorXInstitucion = new TblProfesorXInstitucion();
		profesorXInstitucion.setTblInstitucion(new TblInstitucion());
		profesorXInstitucion.setSegUsuario(new SegUsuario());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(profesorXInstitucion != null){
				
				ProfesorXInstitucionFachada profesorXInstitucionFachada = LocalizadorBean.ObtenerBean(ProfesorXInstitucionFachada.class);
				profesorXInstitucionFachada.crear(profesorXInstitucion);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			ProfesorXInstitucionFachada profesorXInstitucionFachada = LocalizadorBean.ObtenerBean(ProfesorXInstitucionFachada.class);
			profesorXInstitucionFachada.actualizar(profesorXInstitucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			ProfesorXInstitucionFachada profesorXInstitucionFachada = LocalizadorBean.ObtenerBean(ProfesorXInstitucionFachada.class);
			profesorXInstitucionFachada.inhabilitar(profesorXInstitucion);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PROFESOR_X_INSTITUCION_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			ProfesorXInstitucionFachada profesorXInstitucionFachada = LocalizadorBean.ObtenerBean(ProfesorXInstitucionFachada.class);
			listaProfesorXInstitucion = profesorXInstitucionFachada.consultarPorFiltro(profesorXInstitucion);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			ProfesorXInstitucionFachada profesorXInstitucionFachada = LocalizadorBean.ObtenerBean(ProfesorXInstitucionFachada.class);
			listaProfesorXInstitucion = profesorXInstitucionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PROFESOR_X_INSTITUCION_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}

	public void limpiar(){
		
		profesorXInstitucion = new TblProfesorXInstitucion();
		profesorXInstitucion.setTblInstitucion(new TblInstitucion());
		profesorXInstitucion.setSegUsuario(new SegUsuario());
	}
		
}