package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.EstudianteXGrupoFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name = "estudianteXGrupoBean")
public class EstudianteXGrupoControlador {

	private TblEstudianteXGrupo estudianteXGrupo;
	private ArrayList<TblEstudianteXGrupo> listaEstudianteXGrupo;

	public ArrayList<TblEstudianteXGrupo> getListaEstudianteXGrupo() {
		return listaEstudianteXGrupo;
	}

	public TblEstudianteXGrupo getEstudianteXGrupo() {
		return estudianteXGrupo;
	}

	public void setEstudianteXGrupo(TblEstudianteXGrupo estudianteXGrupo) {
		this.estudianteXGrupo = estudianteXGrupo;
	}

	public EstudianteXGrupoControlador() {
		estudianteXGrupo = new TblEstudianteXGrupo();
		estudianteXGrupo.setTblEstudianteXInstitucion(new TblEstudianteXInstitucion());
		estudianteXGrupo.setTblGrupo(new TblGrupo());
		consultarTodos();
	}

	// Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {

		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}

	public void crear() {

		try {
			if (estudianteXGrupo != null) {
					EstudianteXGrupoFachada estudianteXGrupoFachada = LocalizadorBean
							.ObtenerBean(EstudianteXGrupoFachada.class);
					estudianteXGrupoFachada.crear(estudianteXGrupo);
	
					gestionarMensaje(FacesMessage.SEVERITY_INFO,
							CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_TITULO_EXITO_CREACION"),
							CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_CREACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_CREACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void actualizar() {

		try {

			EstudianteXGrupoFachada estudianteXGrupoFachada = LocalizadorBean
					.ObtenerBean(EstudianteXGrupoFachada.class);
			estudianteXGrupoFachada.actualizar(estudianteXGrupo);

			gestionarMensaje(FacesMessage.SEVERITY_INFO,
					CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_TITULO_EXITO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_EXITO_ACTUALIZACION"));

		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void inhabilitar() {

		try {

			EstudianteXGrupoFachada estudianteXGrupoFachada = LocalizadorBean
					.ObtenerBean(EstudianteXGrupoFachada.class);
			estudianteXGrupoFachada.inhabilitar(estudianteXGrupo);

			gestionarMensaje(FacesMessage.SEVERITY_INFO,
					CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_TITULO_EXITO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("INFORMACION_ESTUDIANTE_X_GRUPO_EXITO_ELIMINACION"));

		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void consultar() {

		try {
			EstudianteXGrupoFachada estudianteXGrupoFachada = LocalizadorBean
					.ObtenerBean(EstudianteXGrupoFachada.class);
			listaEstudianteXGrupo = estudianteXGrupoFachada.consultarPorFiltro(estudianteXGrupo);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_CONSULTA"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}

	}

	private void consultarTodos() {

		try {
			EstudianteXGrupoFachada estudianteXGrupoFachada = LocalizadorBean
					.ObtenerBean(EstudianteXGrupoFachada.class);
			listaEstudianteXGrupo = estudianteXGrupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_TITULO_CONSULTA"),
					CatalogoMensajes.obtenerMensaje("ERROR_ESTUDIANTE_X_GRUPO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}

	}

	public void limpiar() {

		estudianteXGrupo = new TblEstudianteXGrupo();
		estudianteXGrupo.setTblEstudianteXInstitucion(new TblEstudianteXInstitucion());
		estudianteXGrupo.setTblGrupo(new TblGrupo());
	}

}