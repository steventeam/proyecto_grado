package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.RecursoPerfilFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="recursoPerfilBean")
@SessionScoped
public class RecursoPerfilControlador {
	
	private SegRecursoPerfil recursoPerfil;
	private ArrayList<SegRecursoPerfil> listaRecursoPerfil;
	
		
	public ArrayList<SegRecursoPerfil> getListaRecursoPerfil() {
		return listaRecursoPerfil;
	}

	public SegRecursoPerfil getRecursoPerfil() {
		return recursoPerfil;
	}

	public void setRecursoPerfil(SegRecursoPerfil recursoPerfil) {
		this.recursoPerfil = recursoPerfil;
	}

	public RecursoPerfilControlador() {
		recursoPerfil = new SegRecursoPerfil();
		recursoPerfil.setInCodigo(2100000000);
		recursoPerfil.setSegRecurso(new SegRecurso());
		recursoPerfil.setSegPerfil(new SegPerfil());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(recursoPerfil != null){
				
				RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
				recursoPerfilFachada.crear(recursoPerfil);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			recursoPerfilFachada.actualizar(recursoPerfil);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			recursoPerfilFachada.inhabilitar(recursoPerfil);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_RECURSO_PERFIL_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			listaRecursoPerfil = recursoPerfilFachada.consultarPorFiltro(recursoPerfil);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			listaRecursoPerfil = recursoPerfilFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_RECURSO_PERFIL_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		recursoPerfil = new SegRecursoPerfil();
		recursoPerfil.setInCodigo(2100000000);
		recursoPerfil.setSegPerfil(new SegPerfil());
		recursoPerfil.setSegRecurso(new SegRecurso());
	}
		
}