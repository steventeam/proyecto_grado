package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.MateriaXPensumFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name = "materiaXPensumBean")
public class MateriaXPensumControlador {

	private TblMateriaXPensum materiaXPensum;
	private ArrayList<TblMateriaXPensum> listaMateriaXPensum;

	public ArrayList<TblMateriaXPensum> getListaMateriaXPensum() {
		return listaMateriaXPensum;
	}

	public TblMateriaXPensum getMateriaXPensum() {
		return materiaXPensum;
	}

	public void setMateriaXPensum(TblMateriaXPensum materiaXPensum) {
		this.materiaXPensum = materiaXPensum;
	}

	public MateriaXPensumControlador() {
		materiaXPensum = new TblMateriaXPensum();
		materiaXPensum.setTblMateria(new TblMateria());
		materiaXPensum.setTblPensumXSemestre(new TblPensumXSemestre());
		materiaXPensum.setTblMateriaXPensum(new TblMateriaXPensum());

		consultarTodos();
	}

	// Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {

		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}

	public void crear() {

		try {

			if (materiaXPensum != null) {
				

				MateriaXPensumFachada materiaXPensumFachada = LocalizadorBean.ObtenerBean(MateriaXPensumFachada.class);
				materiaXPensumFachada.crear(materiaXPensum);

				gestionarMensaje(FacesMessage.SEVERITY_INFO,
						CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_TITULO_EXITO_CREACION"),
						CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_CREACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_CREACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void actualizar() {

		try {

			MateriaXPensumFachada materiaXPensumFachada = LocalizadorBean.ObtenerBean(MateriaXPensumFachada.class);
			materiaXPensumFachada.actualizar(materiaXPensum);

			gestionarMensaje(FacesMessage.SEVERITY_INFO,
					CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_TITULO_EXITO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_EXITO_ACTUALIZACION"));

		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_ACTUALIZACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void inhabilitar() {

		try {

			MateriaXPensumFachada materiaXPensumFachada = LocalizadorBean.ObtenerBean(MateriaXPensumFachada.class);
			materiaXPensumFachada.inhabilitar(materiaXPensum);

			gestionarMensaje(FacesMessage.SEVERITY_INFO,
					CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_TITULO_EXITO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("INFORMACION_MATERIA_X_PENSUM_EXITO_ELIMINACION"));

		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} catch (Exception excepcion) {
			// Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_ELIMINACION"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());

		} finally {
			consultarTodos();
			limpiar();
		}
	}

	public void consultar() {

		try {
			MateriaXPensumFachada materiaXPensumFachada = LocalizadorBean.ObtenerBean(MateriaXPensumFachada.class);
			listaMateriaXPensum = materiaXPensumFachada.consultarPorFiltro(materiaXPensum);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_CONSULTA"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}

	}

	private void consultarTodos() {

		try {
			MateriaXPensumFachada materiaXPensumFachada = LocalizadorBean.ObtenerBean(MateriaXPensumFachada.class);
			listaMateriaXPensum = materiaXPensumFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR,
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_TITULO_CONSULTA"),
					CatalogoMensajes.obtenerMensaje("ERROR_MATERIA_X_PENSUM_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}

	}

	public ArrayList<TblMateriaXPensum> getListaMateriaXPensum2() {
		ArrayList<TblMateriaXPensum> listaDef = new ArrayList<>();
		for (TblMateriaXPensum lista : listaMateriaXPensum) {
			if (lista.getInCodigo() != 0) {
				listaDef.add(lista);
			}
		}
		return listaDef;

	}

	public void limpiar() {

		materiaXPensum = new TblMateriaXPensum();
		materiaXPensum.setTblMateria(new TblMateria());
		materiaXPensum.setTblPensumXSemestre(new TblPensumXSemestre());
		materiaXPensum.setTblMateriaXPensum(new TblMateriaXPensum());

	}

}