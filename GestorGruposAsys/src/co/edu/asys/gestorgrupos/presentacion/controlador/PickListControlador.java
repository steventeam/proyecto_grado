package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.RecursoPerfilFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;

@ManagedBean(name="recursoPerfilPickListBean")
@SessionScoped
public class PickListControlador {
	
	private SegRecursoPerfil recursoPerfil;
	private ArrayList<SegRecursoPerfil> listaRecursoPerfil;
	
		
	public ArrayList<SegRecursoPerfil> getListaRecursoPerfil() {
		return listaRecursoPerfil;
	}

	public SegRecursoPerfil getRecursoPerfil() {
		return recursoPerfil;
	}

	public void setRecursoPerfil(SegRecursoPerfil recursoPerfil) {
		this.recursoPerfil = recursoPerfil;
	}

	public PickListControlador() {
		recursoPerfil = new SegRecursoPerfil();
		recursoPerfil.setSegRecurso(new SegRecurso());
		recursoPerfil.setSegPerfil(new SegPerfil());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		try {
			
			if (recursoPerfil != null) {
			// Lo que hay que validar
			// 1. Obligatoriedad
			// 2. Formato
			// 3. Tipo del dato
			// 4. Longitud
			
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			
			recursoPerfilFachada.crear(recursoPerfil);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "Éxito creando tipo de identificación", "Se ha registrado la información de manera exitosa");
			}
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error creando tipo de identificación", "Se ha presentado un error tratando de registrar la información de nuevo tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
			
		}
		finally {
			consultarTodos();
			limpiar();
		}
		
	}
	
	public void actualizar() {
			try {
				RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
				recursoPerfilFachada.actualizar(recursoPerfil);
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, "Éxito actualizando tipo de identificación", "Se ha actualizando la información de manera exitosa");
				
			} catch (Exception excepcion) {
				gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error actualizando tipo de identificación", "Se ha presentado un error tratando de actualizar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
				excepcion.printStackTrace();
			}
			finally {
				consultarTodos();
				limpiar();
			}
		}
	
	public void inhabilitar() {
		try {
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			recursoPerfilFachada.inhabilitar(recursoPerfil);
			
			gestionarMensaje(FacesMessage.SEVERITY_INFO, "Éxito eliminando tipo de identificación", "Se ha eliminado la información de manera exitosa");
			
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error eliminando tipo de identificación", "Se ha presentado un error tratando de dar de baja la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
			finally {
				consultarTodos();
				limpiar();
			}
	}
	
	public void consultar() {
		
		
		try {
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			listaRecursoPerfil = recursoPerfilFachada.consultarPorFiltro(recursoPerfil);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
				
		
	}
	
	public void limpiar(){
		//1. A pedal
		recursoPerfil.setInCodigo(null);
		
		//2. Mejor
		recursoPerfil = new SegRecursoPerfil();
	}
	
	private void consultarTodos() {
	
		try {
			RecursoPerfilFachada recursoPerfilFachada = LocalizadorBean.ObtenerBean(RecursoPerfilFachada.class);
			listaRecursoPerfil = recursoPerfilFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error consultando tipo de identificación", "Se ha presentado un error tratando de consultar la información de tipo de identificación. Por favor intente de nuevo y si el problema persiste contacte el administrador de la aplicación");
			excepcion.printStackTrace();
		}
		
	}
	
}