package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblAsistencia;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblEstudianteXGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblGrupo;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblMateriaXPensum;
import co.edu.asys.gestorgrupos.entidad.seguridad.TblSesion;
import co.edu.asys.gestorgrupos.negocio.fachada.AsistenciaFachada;
import co.edu.asys.gestorgrupos.negocio.fachada.EstudianteXGrupoFachada;
import co.edu.asys.gestorgrupos.negocio.fachada.GrupoFachada;
import co.edu.asys.gestorgrupos.negocio.fachada.SesionFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="asistenciaBean")
public class AsistenciaControlador {
	
	private TblAsistencia asistencia;
	private ArrayList<TblAsistencia> listaAsistencia;
	
	private Integer sesionSelCode;
	private ArrayList<TblSesion> listaSesiones;
	private TblSesion sesionSel;
	
	private Integer grupoSelCode;
	private ArrayList<TblGrupo> listaGrupos;
	private TblGrupo grupoSel;
	
	private ArrayList<TblEstudianteXGrupo> listaEstudiantes;
	
	private Integer materiaSelCode;
	private TblMateriaXPensum materiaSel;
	
	
	public void onMateriaChange(){
		consultarGrupos();
		ArrayList<TblGrupo> lisFilter = new ArrayList<>();
		for (TblGrupo grupo: this.listaGrupos) {
			if (grupo.getTblMateriaXPensum().getInCodigo().equals(materiaSelCode)){
				lisFilter.add(grupo);
			}
		}
		this.listaGrupos = lisFilter;
	}
	
	public void onGrupoChange(){
		consultarSesiones();
		ArrayList<TblSesion> lisFilter = new ArrayList<>();
		for (TblSesion sesion: this.listaSesiones) {
			if (sesion.getTblGrupo().getInCodigo().equals(grupoSelCode)){
				lisFilter.add(sesion);
			}
		}
		this.listaSesiones = lisFilter;
	}
	
	public void onGrupoChange2(){
		consultarEstudiantes();
		ArrayList<TblEstudianteXGrupo> lisFilter = new ArrayList<>();
		for (TblEstudianteXGrupo estudianteGrupo: this.listaEstudiantes) {
			if (estudianteGrupo.getTblGrupo().getInCodigo().equals(grupoSelCode)){
				lisFilter.add(estudianteGrupo);
			}
		}
		this.listaEstudiantes = lisFilter;
	}

	public ArrayList<TblEstudianteXGrupo> getListaEstudiantes() {
		return listaEstudiantes;
	}

	public Integer getGrupoSelCode() {
		return grupoSelCode;
	}


	public void setGrupoSelCode(Integer grupoSelCode) {
		this.grupoSelCode = grupoSelCode;
	}


	public TblGrupo getGrupoSel() {
		return grupoSel;
	}


	public void setGrupoSel(TblGrupo grupoSel) {
		this.grupoSel = grupoSel;
	}


	public ArrayList<TblGrupo> getListaGrupos() {
		return listaGrupos;
	}


	public Integer getSesionSelCode() {
		return sesionSelCode;
	}

	public void setSesionSelCode(Integer sesionSelCode) {
		this.sesionSelCode = sesionSelCode;
	}

	public TblSesion getSesionSel() {
		return sesionSel;
	}

	public void setSesionSel(TblSesion sesionSel) {
		this.sesionSel = sesionSel;
	}

	public Integer getMateriaSelCode() {
		return materiaSelCode;
	}

	public void setMateriaSelCode(Integer materiaSelCode) {
		this.materiaSelCode = materiaSelCode;
	}

	public TblMateriaXPensum getMateriaSel() {
		return materiaSel;
	}



	public void setMateriaSel(TblMateriaXPensum materiaSel) {
		this.materiaSel = materiaSel;
	}



	public ArrayList<TblSesion> getListaSesiones() {
		return listaSesiones;
	}



	public ArrayList<TblAsistencia> getListaAsistencia() {
		return listaAsistencia;
	}

	public TblAsistencia getAsistencia() {
		return asistencia;
	}

	public void setAsistencia(TblAsistencia asistencia) {
		this.asistencia = asistencia;
	}

	public AsistenciaControlador() {
		asistencia = new TblAsistencia();
		asistencia.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		asistencia.setTblSesion(new TblSesion());
		materiaSel = new TblMateriaXPensum();
		grupoSel = new TblGrupo();
		consultarTodos();
		consultarSesiones();
		consultarGrupos();
		consultarEstudiantes();
		
//		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//		String tokenReceived = params.get("tk");
//		System.out.println("TOKEN RECIBIDO -------------- " + tokenReceived);
//		
//		Token t = new Token();
//		
//		//asistenciaFachada.consultarPorCodigo(asistencia.getInCodigo());
//		t = Token.consultarPorCodigo(tokenReceived);
//		
//		if (t.isUsed()){
//			System.out.println("el token ya fue usado, inicie el proceso nuevamente");
//		}else if (t.getDateExpired().compareTo(new Date()) > 0){
//			System.out.println("el token expiro");
//		}else{
//			//se puede hacer la modificación
//		}
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			if (asistencia != null) {
				
				AsistenciaFachada asistenciaFachada = LocalizadorBean.ObtenerBean(AsistenciaFachada.class);
				asistenciaFachada.crear(asistencia);
					
//				asistenciaFachada.consultarPorCodigo(asistencia.getInCodigo());
				
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			AsistenciaFachada asistenciaFachada = LocalizadorBean.ObtenerBean(AsistenciaFachada.class);
			asistenciaFachada.actualizar(asistencia);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			AsistenciaFachada asistenciaFachada = LocalizadorBean.ObtenerBean(AsistenciaFachada.class);
			asistenciaFachada.inhabilitar(asistencia);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_ASISTENCIA_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			AsistenciaFachada asistenciaFachada = LocalizadorBean.ObtenerBean(AsistenciaFachada.class);
			listaAsistencia = asistenciaFachada.consultarPorFiltro(asistencia);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	public void limpiar(){
		
		asistencia = new TblAsistencia();
		asistencia.setTblEstudianteXGrupo(new TblEstudianteXGrupo());
		asistencia.setTblSesion(new TblSesion());
	}
	
	private void consultarTodos() {
	
		try {
			AsistenciaFachada asistenciaFachada = LocalizadorBean.ObtenerBean(AsistenciaFachada.class);
			listaAsistencia = asistenciaFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	private void consultarSesiones() {
		
		try {
			SesionFachada sesionFachada = LocalizadorBean.ObtenerBean(SesionFachada.class);
			listaSesiones = sesionFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	private void consultarEstudiantes() {
		
		try {
			EstudianteXGrupoFachada estudianteGrupoFachada = LocalizadorBean.ObtenerBean(EstudianteXGrupoFachada.class);
			listaEstudiantes = estudianteGrupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	private void consultarGrupos() {
		
		try {
			GrupoFachada grupoFachada = LocalizadorBean.ObtenerBean(GrupoFachada.class);
			listaGrupos = grupoFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_ASISTENCIA_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
//    public ArrayList<TblAsistencia> getListaAsistencia2() {
//		ArrayList<TblAsistencia> listaDef = new ArrayList<>();
//		for(TblAsistencia lista : listaAsistencia){	
//			if((lista.getDtFechaLimiteIngreso().after(new Date())) && (lista.getDtFechaLimiteIngreso().after(lista.getDtFechaIngreso()))){
//				listaDef.add(lista);
//			}
//		}
//		return listaDef;
//		
//	}
}
