package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.TblSemestre;
import co.edu.asys.gestorgrupos.negocio.fachada.SemestreFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="semestreBean")
@SessionScoped
public class SemestreControlador {
	
	private TblSemestre semestre;
	private ArrayList<TblSemestre> listaSemestre;
	
		
	public ArrayList<TblSemestre> getListaSemestre() {
		return listaSemestre;
	}

	public TblSemestre getSemestre() {
		return semestre;
	}

	public void setSemestre(TblSemestre semestre) {
		this.semestre = semestre;
	}

	public SemestreControlador() {
		semestre = new TblSemestre();
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(semestre != null){
				
				SemestreFachada semestreFachada = LocalizadorBean.ObtenerBean(SemestreFachada.class);
				semestreFachada.crear(semestre);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_CREACION", semestre.getNvNombre()));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			SemestreFachada semestreFachada = LocalizadorBean.ObtenerBean(SemestreFachada.class);
			semestreFachada.actualizar(semestre);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_ACTUALIZACION", semestre.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			SemestreFachada semestreFachada = LocalizadorBean.ObtenerBean(SemestreFachada.class);
			semestreFachada.inhabilitar(semestre);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_SEMESTRE_EXITO_ELIMINACION", semestre.getNvNombre()));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el cat�logo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			SemestreFachada semestreFachada = LocalizadorBean.ObtenerBean(SemestreFachada.class);
			listaSemestre = semestreFachada.consultarPorFiltro(semestre);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			SemestreFachada semestreFachada = LocalizadorBean.ObtenerBean(SemestreFachada.class);
			listaSemestre = semestreFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_SEMESTRE_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void limpiar(){
		
		semestre = new TblSemestre();
	}
		
}