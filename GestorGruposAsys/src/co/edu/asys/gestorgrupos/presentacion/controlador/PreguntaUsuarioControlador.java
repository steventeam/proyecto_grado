package co.edu.asys.gestorgrupos.presentacion.controlador;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Level;

import co.edu.asys.gestorgrupos.entidad.seguridad.*;
import co.edu.asys.gestorgrupos.negocio.fachada.PreguntaUsuarioFachada;
import co.edu.asys.gestorgrupos.transversal.aop.LocalizadorBean;
import co.edu.asys.gestorgrupos.transversal.excepcion.ExcepcionAplicacionGrupos;
import co.edu.asys.gestorgrupos.transversal.logging.UtilLogging;
import co.edu.asys.gestorgrupos.transversal.mensajes.CatalogoMensajes;

@ManagedBean(name="preguntaUsuarioBean")
@SessionScoped
public class PreguntaUsuarioControlador {
	
	private SegPreguntaUsuario preguntaUsuario;
	private ArrayList<SegPreguntaUsuario> listaPreguntaUsuario;
	
		
	public ArrayList<SegPreguntaUsuario> getListaPreguntaUsuario() {
		return listaPreguntaUsuario;
	}

	public SegPreguntaUsuario getPreguntaUsuario() {
		return preguntaUsuario;
	}

	public void setPreguntaUsuario(SegPreguntaUsuario preguntaUsuario) {
		this.preguntaUsuario = preguntaUsuario;
	}

	public PreguntaUsuarioControlador() {
		preguntaUsuario = new SegPreguntaUsuario();
		preguntaUsuario.setSegPregunta(new SegPregunta());
		preguntaUsuario.setSegUsuario(new SegUsuario());
		consultarTodos();
	}
	
	//Agrega mensaje a faces
	private void gestionarMensaje(Severity severidad, String titulo, String contenido) {
		
		FacesMessage mensaje = new FacesMessage(severidad, titulo, contenido);
		FacesContext.getCurrentInstance().addMessage(null, mensaje);
	}
	
	
	public void crear() {
		
		try {
			
			if(preguntaUsuario != null){
				
				PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
				preguntaUsuarioFachada.crear(preguntaUsuario);
					
				gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_TITULO_EXITO_CREACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_EXITO_CREACION"));
			}
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_CREACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CREACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void actualizar() {

		try {
			
			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
			preguntaUsuarioFachada.actualizar(preguntaUsuario);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_TITULO_EXITO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_EXITO_ACTUALIZACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_ACTUALIZACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ACTUALIZACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void inhabilitar() {
		
		try {
			
			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
			preguntaUsuarioFachada.inhabilitar(preguntaUsuario);
				
			gestionarMensaje(FacesMessage.SEVERITY_INFO, CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_TITULO_EXITO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("INFORMACION_PREGUNTA_USUARIO_EXITO_ELIMINACION"));
			
		} catch (ExcepcionAplicacionGrupos excepcion) {
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION"));
			excepcion.getMensajeUsuario();
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
					
		} catch (Exception excepcion) {
			//Obtener los mensajes desde el catálogo de mensajes
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_ELIMINACION"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_ELIMINACION", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
				
		} finally {
			consultarTodos();
			limpiar();
		}
	}
	
	public void consultar() {
		
		
		try {
			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
			listaPreguntaUsuario = preguntaUsuarioFachada.consultarPorFiltro(preguntaUsuario);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
				
		
	}
	
	private void consultarTodos() {
	
		try {
			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
			listaPreguntaUsuario = preguntaUsuarioFachada.consultarPorFiltro(null);
		} catch (Exception excepcion) {
			String mensajeUsuario = CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CONSULTA");
			gestionarMensaje(FacesMessage.SEVERITY_ERROR, CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_TITULO_CONSULTA"), CatalogoMensajes.obtenerMensaje("ERROR_PREGUNTA_USUARIO_CONSULTA", mensajeUsuario));
			UtilLogging.escribirLog(this.getClass(), Level.ERROR, excepcion.getCause());
		}
		
	}
	
	public void consultarUsuario() {

		try {

			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);
			ArrayList<SegPreguntaUsuario> listaPreguntaUsuario = preguntaUsuarioFachada.consultarPorFiltro(preguntaUsuario);

			if (listaPreguntaUsuario.isEmpty()) {

				gestionarMensaje(FacesMessage.SEVERITY_WARN, "Error", "Información incorrecta");

			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/GestorGruposAsys/faces/paginas/publico/recuperarClave/recuperarClave.xhtml");

				// FacesContext.getCurrentInstance().getExternalContext().redirect("paginas/seguridad/usuario.xhtml");
			}

		} catch (Exception excepcion) {

			excepcion.printStackTrace();

		}

	}
	
	public void limpiar(){
		//1. A pedal
		preguntaUsuario.setInCodigo(null);
		
		//2. Mejor
		preguntaUsuario = new SegPreguntaUsuario();
		preguntaUsuario.setSegPregunta(new SegPregunta());
		preguntaUsuario.setSegUsuario(new SegUsuario());
	}
		
	public void recuperar() {
		
		try {

			PreguntaUsuarioFachada preguntaUsuarioFachada = LocalizadorBean.ObtenerBean(PreguntaUsuarioFachada.class);

			preguntaUsuarioFachada.recuperar(preguntaUsuario);
			
		} catch (Exception exception) {

			gestionarMensaje(FacesMessage.SEVERITY_ERROR, "Error en la validación de datos", "Los datos ingresados no son correctos, por favor intentelo nuevamente");
			exception.printStackTrace();
			limpiar();
		} finally {
			consultarTodos();

		}
	}

}